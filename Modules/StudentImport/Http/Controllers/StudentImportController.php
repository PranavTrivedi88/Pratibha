<?php

namespace Modules\StudentImport\Http\Controllers;

use App\City;
use App\Course;
use App\CourseStudentSchool;
use App\Parents;
use App\Student;
use App\User;
use Encore\Admin\Auth\Database\Role;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Encore\Admin\Facades\Admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Nwidart\Modules\Facades\Module;
use Encore\Admin\Layout\Content;

class StudentImportController extends Controller
{


    public function __construct()
    {
        Admin::script([
            "jQuery.getScript('" . Module::asset('studentimport:js/xls.core.min.js') . "');",
            "jQuery.getScript('" . Module::asset('studentimport:js/xlsx.core.min.js') . "');",
            "jQuery.getScript('" . Module::asset('studentimport:js/import.min.js') . "');",
            "jQuery.getScript('" . Module::asset('studentimport:js/import.js') . "');",
        ]);

        Admin::css([
            Module::asset('studentimport:css/student-module.css')
        ]);
    }

    public function index()
    {
        $download = \request()->get('download');
        if ($download) {
            return response()->download(public_path('master/student_import.xls'));
        }

        return Admin::content(function (Content $content) {
                $content->header('Students Import');
                $content->body(view('studentimport::student'));
            });
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('studentimport::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            list($errors, $success) = $this->mapValuesAndValidateForStudent(json_decode($request->data), $request->school);
        } catch (\Exception $exception) {
            $errors = [
                'title' => "There are errors in your sheet",
                'message' => $exception->getMessage()
            ];
            return response()->json($errors,500);
        }

        if (count($errors) > 0) {
            return response()->json([$errors, $success],500);
        } else {
            return response()->json($success,200);
        }
    }

    public function mapValuesAndValidateForStudent($items, $school){
        $counter = 0;
        $errorsArray = [];
        $successRow = [];

        //dd($items);
        foreach ($items as $row) {
            $errors = [];
            $errorCounter = 0;

            //Let's take parent first.
            $parentHasError = false;
            $parent = [];
            $parent['father_name'] = $row->father_name ?? '';
            $parent['father_mobile'] = $row->father_mobile ?? '';
            $parent['father_profession'] = $row->father_profession ?? '';
            $parent['mother_name'] = $row->mother_name_o ?? '';
            $parent['mother_mobile'] = $row->mother_mobile ?? '';
            $parent['mother_profession'] = $row->mother_profession ?? '';
            $parent['address'] = $row->address ?? '';
            $parent['city'] = isset($row->city) ? $this->findOrCreateCity($row->city) : 1;

            //Map student information to array.
            $student = [];

            $student['full_name'] = $row->full_name ?? '';
            $student['primary_mobile'] = $row->primary_mobile ?? '';
            $student['second_mobile'] = $row->other_mobile ?? '';
            $student['birthdate'] = $row->birthdate;//!empty($row->birthdate) ? date('Y-m-d',strtotime($row->birthdate)): '';
            $student['address'] = $row->address ?? '';
            $student['city'] = isset($row->city) ? $this->findOrCreateCity($row->city) : 1;
            $student['student_blood_group'] = $row->blood_group ?? '';
            $student['activities'] = $row->activities ?? '';
            $student['school'] = $school;


            $parentValidation = Validator::make($parent, [
                'father_mobile' => 'nullable|digits:10',
                'father_profession' => 'nullable|string',
                'mother_mobile' => 'nullable|digits:10',
                'mother_profession' => 'nullable|string',
                'city' => 'numeric|nullable',
            ],[
                'city.number' => 'The city field is invalid.'
            ]);

            //Validate student records.
            $studentValidation = Validator::make($student, [
                'full_name' => 'required',
                'birthdate' => 'nullable|date_format:"d-m-Y"',
                'primary_mobile' => 'required|digits:10|unique:users,username',
                'second_mobile' => 'nullable|digits:10',
                'activities' => 'required'
            ],[
                'birthdate.date_format' => 'The birth date is not a valid date. The format is dd-mm-yyyy.'
            ]);

            //dd($row->birthdate, date('Y-m-d',strtotime("15-12-1991")));
            $student['birthdate'] = date('Y-m-d',strtotime($row->birthdate));

            if ($parentValidation->fails()) {

                $parentErrors = $this->createErrorArray($parentValidation);

                $errors[$errorCounter] = [
                    'm' => "Student record at " . ordinal($errorCounter) . " row has " . str_plural('error', $errorCounter),
                    'errors' => $parentErrors
                ];
                $parentHasError = true;
            }

            if ($studentValidation->fails()){

                $studentErrors = $this->createErrorArray($studentValidation);

                if ($parentHasError) {
                    $errors[$errorCounter]['errors'] = array_merge($errors[$errorCounter]['errors'], $studentErrors);
                } else {
                    $errors[$errorCounter] = [
                        'm' => "Student record at " . ordinal($errorCounter) . " row has " . str_plural('error', $errorCounter),
                        'errors' => $studentErrors
                    ];
                }
            }

            if(!(count($errors) > 0)){

                DB::beginTransaction();
                $parent = array_filter($parent);
                if(count($parent) > 0) {
                    $addParent = $this->saveParent($parent);
                    if ($addParent['status'] == false) {
                        DB::rollback();
                        $errors[$errorCounter]['m'] = "Record at row $errorCounter has error";
                        $errors[$errorCounter]['e'][] = $addParent['error'];
                    }
                }

                //$student = array_filter($student);
                if(count($student) > 0){
                    $saved = $this->saveStudent($student, $addParent['parent']);
                    if ($saved['status'] == false) {
                        DB::rollback();
                        $errors[$errorCounter]['m'] = "Record at row $errorCounter has error";
                        $errors[$errorCounter]['e'][] = $saved['error'];
                        $errorsArray[$counter] = $errors;
                    }else{
                        DB::commit();
                        $successRow[$counter] = ['Successfully saved.'];
                    }
                }

            } else {
                $errorsArray[$counter] = $errors;
            }

            $counter++;
        }

        return [
            //dd($errorsArray, $successRow),
            $errorsArray, $successRow
        ];
    }

    private function saveParent($row)
    {
        if(isset($row['father_mobile'])){
            $parent = Parents::where(['father_mobile' => $row['father_mobile']]);

            if ($parent->count()) {
                return [
                    "status" => true,
                    "parent" => $parent->first()
                ];
            }
        }

        $parent = new Parents();
        try {

            $parent->father_name = isset($row['father_name']) ? $row['father_name'] : "";
            $parent->father_mobile = isset($row['father_mobile']) ? $row['father_mobile'] : "";
            $parent->father_profession = isset($row['father_profession']) ? $row['father_profession'] : "";
            $parent->mother_name = isset($row['mother_name']) ? $row['mother_name'] : "";
            $parent->mother_mobile = isset($row['mother_mobile']) ? $row['mother_mobile'] : "";
            $parent->mother_profession = isset($row['mother_profession']) ? $row['mother_profession'] : "";
            $parent->address = isset($row['address']) ? $row['address'] : "";
            $parent->city_id = isset($row['city']) ? $row['city'] : "";

            $parent->save();

            //$parent->roles()->save(Role::where("slug", "parent")->first());

        } catch (\Exception $exception) {
            return [
                "status" => false,
                "error" => $exception->getMessage()
            ];
        }

        return [
            "status" => true,
            "parent" => $parent
        ];
    }

    private function saveStudent($row, $parent)
    {
        try {

            $user = new User();
            $user->name = $row['full_name'];
            $user->mobile = $row['primary_mobile'];
            $user->address = $row['address'];
            $user->city_id = $row['city'];
            $user->password = bcrypt(config('app.default_password'));
            $user->username = $row['primary_mobile'];
            $user->save();

            $user->roles()->save(Role::where('slug', 'student')->first());

            Student::create([
                "student_category" => 1,
                'second_contact_no' => $row['second_mobile'],
                'parent_id' => $parent ? $parent->id : 0,
                'student_blood_group' => $row['student_blood_group'],
                'user_id' => $user->id,
                'bdate' => $row['birthdate'],
                'school_id' => $row['school']
            ]);

            $courses = explode(",", $row['activities']);

            foreach ($courses as $course){
                $course_info = Course::where("name", $course)->first();
                if($course_info) {
                    CourseStudentSchool::create([
                        "course_id" => $course_info->id,
                        "user_id" => $user->id
                    ]);
                }
            }

            $user->roles()->save(Role::where("slug", "student")->first());

        } catch (QueryException $exception) {
            //dd($exception->getMessage());
            return [
                "status" => false,
                "error" => $exception->getMessage()
            ];
        }

        return [
            "status" => true,
            "error" => $user
        ];
    }

    private function findOrCreateCity($name)
    {
        if (empty($name)) {
            return 1;
        }
        $found = City::where('name', $name);
        if ($found->count() == 0) {
            $city = City::create([
                'name' => $name
            ]);
            return $city->id;
        } else {
            return $found->first()->id;
        }
    }

    function createErrorArray($validation)
    {
        $key = $validation->errors()->keys();
        $errors = [];
        foreach ($validation->errors()->all() as $index => $value)
        {
            $errors[$key[$index]] = $validation->errors()->first($key[$index]);
        }
        return $errors;
    }


    public function show()
    {
        return view('studentimport::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('studentimport::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
