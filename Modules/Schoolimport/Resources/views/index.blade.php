<div class="modal-content">
    <form id="schoolImport" enctype="multipart/form-data" method="post">
        {{ csrf_field() }}
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-1">
                    <p><b>Export School Data:</b></p>
                    <table class="table table-striped no-header table-bordered">
                        <tbody>
                        <tr>
                            <td width="20%">
                                <h5><b>Select Financial Year:</b></h5>
                            </td>
                            <td width="80%">
                                <select id="financialYear" name="financialYear" class="form-control">
                                    <option value="2016">2016-2017</option>
                                    <option value="2017">2017-2018</option>
                                    <option value="2018" selected>2018-2019</option>
                                    <option value="2019">2019-2020</option>
                                    <option value="2020">2020-2021</option>
                                </select>
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2" class="text-center">
                                <button type="submit" class="btn btn-success" id="download">Download School Excel File</button>
                            </td>

                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row" id="sheetContainer" style="display:none;">
                <div class="col-sm-12">
                    <hr>
                    <div class="alert alert-danger alert-dismissable" style="display:none;">
                        <a href="#" class="close" onclick="hideErrorList()">&times;</a>
                        <strong>Error:</strong><br> <span id="errorList"></span>
                    </div>
                </div>
                <div class="col-sm-12" style="width:auto;max-width: 100%;">
                    <table id="sheetPreview" class="table table-bordered table-condensed"
                           style="width:100%;display: block;overflow-x: auto;white-space: nowrap;">
                    </table>
                </div>
            </div>
        </div>
    </form>


    <form id="schoolUpload" enctype="multipart/form-data" method="post">
        {{ csrf_field() }}
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-1">
                    <p><b>Import School Data:</b></p>
                            <div class="alert alert-danger" id="error">
                            </div>
                    <table class="table table-striped no-header table-bordered">
                        <tbody>
                        <tr>
                            <td width="20%">
                                <h5><b>Upload School Excel:</b></h5>
                            </td>
                            <td width="80%">
                                <div class="progress" id="progress">
                                    <div id="progressbar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="" aria-valuemax="100" style="width:0%">
                                    </div>
                                </div>
                                <input type="file" name="schoolFile" id="schoolFile" accept=".xlsx">
                            </td>
                        </tr>
                        <tr id="successMessage">
                            <td colspan="2">
                                <div class="alert alert-success">
                                    School Data Successful Imported.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center">
                                <button type="submit" class="btn btn-success" id="uploadButton">Upload</button>
                            </td>

                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row" id="sheetContainer" style="display:none;">
                <div class="col-sm-12">
                    <hr>
                    <div class="alert alert-danger alert-dismissable" style="display:none;">
                        <a href="#" class="close" onclick="hideErrorList()">&times;</a>
                        <strong>Error:</strong><br> <span id="errorList"></span>
                    </div>
                </div>
                <div class="col-sm-12" style="width:auto;max-width: 100%;">
                    <table id="sheetPreview" class="table table-bordered table-condensed"
                           style="width:100%;display: block;overflow-x: auto;white-space: nowrap;">
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>