<?php

namespace Modules\StudentImportTemp\Http\Controllers;

use App\AppUser;
use App\Batch;
use App\City;
use App\Course;
use App\CourseFeeType;
use App\CourseStudent;
use App\CourseStudentSchool;
use App\Fee;
use App\FeeDetail;
use App\Parents;
use App\PendingFee;
use App\Student;
use App\User;
use Carbon\Carbon;
use Encore\Admin\Auth\Database\Role;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Encore\Admin\Facades\Admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Nwidart\Modules\Facades\Module;
use Encore\Admin\Layout\Content;

class StudentImportTempController extends Controller
{
    public function __construct()
    {
//        Admin::script([
//            "jQuery.getScript('" . Module::asset('studentimporttemp:js/xls.core.min.js') . "');",
//            "jQuery.getScript('" . Module::asset('studentimporttemp:js/xlsx.core.min.js') . "');",
//            "jQuery.getScript('" . Module::asset('studentimporttemp:js/import.min.js') . "');",
//            "jQuery.getScript('" . Module::asset('studentimporttemp:js/import.js') . "');",
//        ]);

        Admin::script("
            $(document).ready(function(){
                jQuery.getScript('" . Module::asset('studentimporttemp:js/xls.core.min.js') . "');
                jQuery.getScript('" . Module::asset('studentimporttemp:js/xlsx.core.min.js') . "');
                jQuery.getScript('" . Module::asset('studentimporttemp:js/import.min.js') . "');
                jQuery.getScript('" . Module::asset('studentimporttemp:js/import.js') . "');
            
            });
        ");

        Admin::css([
            Module::asset('studentimporttemp:css/student-module.css')
        ]);
    }

    public function index()
    {
        $download = request()->get('download');
        if ($download) {
            return response()->download(public_path('master/student_import.xls'));
        }

        return Admin::content(function (Content $content) {
            $content->header('Students Import');
            $content->body(view('studentimporttemp::index'));
        });
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('studentimporttemp::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            //dd($request->data);
            list($errors, $success) = $this->mapValuesAndValidateForStudent($request->data, $request->school);
        } catch (\Exception $exception) {
            $errors = [
                'title' => "There are errors in your sheet",
                'message' => $exception->getMessage()
            ];
            return response()->json($errors, 500);
        }

        if (count($errors) > 0) {
            return response()->json([$errors, $success], 500);
        } else {
            return response()->json($success, 200);
        }
    }

    public function mapValuesAndValidateForStudent($items, $school)
    {
        $counter = 0;
        $errorsArray = [];
        $successRow = [];

        dd($items[0]);
        foreach ($items as $row) {
            $errors = [];
            $errorCounter = 0;

            //Let's take parent first.
            $parentHasError = false;
            $parent = [];
            $parent['father_name'] = $row->father_name ?? '';
            $parent['father_mobile'] = $row->father_mobile ?? '';
            $parent['father_profession'] = $row->father_profession ?? '';
            $parent['mother_name'] = $row->mother_name ?? '';
            $parent['mother_mobile'] = $row->mother_mobile ?? '';
            $parent['mother_profession'] = $row->mother_profession ?? '';
            $parent['address'] = $row->address ?? '';
            $parent['city'] = isset($row->city) ? $this->findOrCreateCity($row->city) : 1;

            //Map student information to array.
            $student = [];

            $student['full_name'] = $row->name ?? '';
            $student['primary_mobile'] = $row->primary_mobile ?? '';
            $student['secondary_mobile'] = $row->secondary_mobile ?? '';
            $student['birth_date'] = $row->birth_date;//!empty($row->birthdate) ? date('Y-m-d',strtotime($row->birthdate)): '';
            $student['address'] = $row->address ?? '';
            $student['city'] = $parent['city'];
            $student['blood_group'] = $row->blood_group ?? '';
            $student['gender'] = $row->gender ?? '';
            $student['activities'] = $row->activities ?? '';
            $student['batches'] = $row->batches ?? '';
            $student['fee_type'] = $row->fee_type ?? '';
            $student['fees_taken'] = $row->fees_taken ?? '';


            $parentValidation = Validator::make($parent, [
                'father_mobile' => 'nullable|digits:10',
                'father_profession' => 'nullable|string',
                'mother_mobile' => 'nullable|digits:10',
                'mother_profession' => 'nullable|string',
                'city' => 'numeric|nullable',
            ], [
                'city.number' => 'The city field is invalid.'
            ]);

            //Validate student records.
            $studentValidation = Validator::make($student, [
                'full_name' => 'required',
                'birth_date' => 'nullable|date_format:"d-m-Y"',
                'primary_mobile' => 'required|digits:10|unique:users,username',
                'secondary_mobile' => 'nullable|digits:10',
                'activities' => 'required',
                'fee_type' => 'required',
                'fees_taken' => 'required'

            ], [
                'birth_date.date_format' => 'The birth date is not a valid date. The format is dd-mm-yyyy.'
            ]);

            $student['birth_date'] = date('Y-m-d', strtotime($row->birth_date));

            if ($parentValidation->fails()) {

                $parentErrors = $this->createErrorArray($parentValidation);

                $errors[$errorCounter] = [
                    'm' => "Student record at " . ordinal($errorCounter) . " row has " . str_plural('error', $errorCounter),
                    'errors' => $parentErrors
                ];
                $parentHasError = true;
            }

            if ($studentValidation->fails()) {

                $studentErrors = $this->createErrorArray($studentValidation);

                if ($parentHasError) {
                    $errors[$errorCounter]['errors'] = array_merge($errors[$errorCounter]['errors'], $studentErrors);
                } else {
                    $errors[$errorCounter] = [
                        'm' => "Student record at " . ordinal($errorCounter) . " row has " . str_plural('error', $errorCounter),
                        'errors' => $studentErrors
                    ];
                }
            }

            if (!(count($errors) > 0)) {

                DB::beginTransaction();
                $parent = array_filter($parent);
                if (count($parent) > 0) {
                    $addParent = $this->saveParent($parent);
                    if ($addParent['status'] == false) {
                        DB::rollback();
                        $errors[$errorCounter]['m'] = "Record at row $errorCounter has error";
                        $errors[$errorCounter]['e'][] = $addParent['error'];
                    }
                }

                //$student = array_filter($student);
                if (count($student) > 0) {
                    $saved = $this->saveStudent($student, $addParent['parent']);
                    if ($saved['status'] == false) {
                        DB::rollback();
                        $errors[$errorCounter]['m'] = "Record at row $errorCounter has error";
                        $errors[$errorCounter]['e'][] = $saved['error'];
                        $errorsArray[$counter] = $errors;
                    } else {
                        DB::commit();
                        $successRow[$counter] = ['Successfully saved.'];
                    }
                }

            } else {
                $errorsArray[$counter] = $errors;
            }

            $counter++;
        }

        return [
            //dd($errorsArray, $successRow),
            $errorsArray, $successRow
        ];
    }

    private function saveParent($row)
    {
        if (isset($row['father_mobile'])) {
            $parent = Parents::where(['father_mobile' => $row['father_mobile']]);

            if ($parent->count()) {
                return [
                    "status" => true,
                    "parent" => $parent->first()
                ];
            }
        }

        $parent = new Parents();
        try {

            $parent->father_name = isset($row['father_name']) ? $row['father_name'] : "";
            $parent->father_mobile = isset($row['father_mobile']) ? $row['father_mobile'] : "";
            $parent->father_profession = isset($row['father_profession']) ? $row['father_profession'] : "";
            $parent->mother_name = isset($row['mother_name']) ? $row['mother_name'] : "";
            $parent->mother_mobile = isset($row['mother_mobile']) ? $row['mother_mobile'] : "";
            $parent->mother_profession = isset($row['mother_profession']) ? $row['mother_profession'] : "";
            $parent->address = isset($row['address']) ? $row['address'] : "";
            $parent->city_id = isset($row['city']) ? $row['city'] : "";

            $parent->save();

            //$parent->roles()->save(Role::where("slug", "parent")->first());

        } catch (\Exception $exception) {
            return [
                "status" => false,
                "error" => $exception->getMessage()
            ];
        }

        return [
            "status" => true,
            "parent" => $parent
        ];
    }

    private function saveStudent($row, $parent)
    {
        try {

            $user = new AppUser();
            $user->name = $row['full_name'];
            $user->mobile = $row['primary_mobile'];
            $user->address = $row['address'];
            $user->city_id = $row['city'];
            $user->password = bcrypt(config('app.default_password'));
            $user->username = $row['primary_mobile'];
            $user->save();

            $user->roles()->save(Role::where('slug', 'student')->first());

            Student::create([
                "student_category" => 0,
                'second_contact_no' => $row['secondary_mobile'],
                'parent_id' => $parent ? $parent->id : 0,
                'student_blood_group' => $row['blood_group'],
                'user_id' => $user->id,
                'bdate' => $row['birth_date'],
                'school_id' => 0
            ]);

            $courses = explode(",", $row['activities']);
            $batches = explode(",", $row['batches']);
            $fee_types = explode(",", $row['fee_type']);
            $fees_taken = explode(",", $row['fees_taken']);

            $i = 0;
            foreach ($courses as $course) {
                $course_info = Course::where("name", $course)->first();

                if ($course_info) {
                    $batch = 2;
                    if ($batches) {
                        $batch_info = Batch::where("name", $batches[$i])->where("course_id", $course_info->id)->first();
                        $batch = $batch_info ? $batch_info->id : 2;
                    }

                    $fee_type = $this->getFeeType($fee_types[$i]);

                    CourseStudent::create([
                        "course_id" => $course_info->id,
                        "student_id" => $user->id,
                        "start_date" => "2018-04-01",
                        "batch_id" => $batch,
                        "fee_type" => $fee_type
                    ]);

                    $fee_amount = CourseFeeType:: where([
                        "course_id" => $course_info->id,
                        "fee_type_id" => $fee_type
                    ])->value("fees");

                    if ($fees_taken[$i] >= 3) {

                        $fees = 3;
                        $total_fee = $fee_amount * 3;
                        $advance = getAdvanceAmount($fee_type, $fees_taken[$i] - 3);
                        $fee_status = 2;

                    } else {

                        $fees = $fees_taken[$i];
                        $total_fee = $fee_amount * $fees_taken[$i];
                        $advance = 0;
                        $fee_status = $fees ? 2 : 1;

                        for ($k = 0; $k < 3 - $fees; $k++) {
                            PendingFee::create([
                               "user_id" => $user->id,
                               "course_id" => $course_info->id,
                                "month" => 6 - $k,
                                "year" => 2018,
                                "fees" => $fee_amount,
                                "fee_type" => $fee_type
                            ]);
                        }
                    }

                    $fee_info = Fee::create([
                        "user_id" => $user->id,
                        "payment_date" => date("Y-m-d H:i:s"),
                        "net_amount" => $total_fee,
                        "lump_sum_amount" => 0,
                        "payment_mode" => 0
                    ]);

                    for ($j = 0; $j < $fees; $j++) {
                        FeeDetail::create([
                            "fee_id" => $fee_info->id,
                            "course_id" => $course_info->id,
                            "discount" => 0,
                            "paid_amount" => $fee_amount
                        ]);
                    }

                    $user->advance_amount = $advance;
                    $user->fee_status = $fee_status;
                    $user->save();
                }
                $i++;
            }

            $user->roles()->save(Role::where("slug", "student")->first());

        } catch (QueryException $exception) {
            //dd($exception->getMessage());
            return [
                "status" => false,
                "error" => $exception->getMessage()
            ];
        }

        return [
            "status" => true,
            "error" => $user
        ];
    }

    private function findOrCreateCity($name)
    {
        if (empty($name)) {
            return 1;
        }
        $found = City::where('name', $name);
        if ($found->count() == 0) {
            $city = City::create([
                'name' => $name
            ]);
            return $city->id;
        } else {
            return $found->first()->id;
        }
    }

    function createErrorArray($validation)
    {
        $key = $validation->errors()->keys();
        $errors = [];
        foreach ($validation->errors()->all() as $index => $value) {
            $errors[$key[$index]] = $validation->errors()->first($key[$index]);
        }
        return $errors;
    }

    function getFeeType($feeType)
    {
        switch ($feeType) {
            case "M" :
                return 1;
                break;
            case "Y" :
                return 2;
                break;
            case "HY" :
                return 4;
                break;
            case "S" :
                return 3;
                break;
        }

        return 0;
    }

    public function show()
    {
        return view('studentimporttemp::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('studentimporttemp::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
