var tableDiv = $('#tableDiv');
tableDiv.hide();
$('#activity_name').change(function () {
    var id = $(this).val();
    var div = $('#selectBox');

    div.empty();
    tableDiv.hide();
    if(id)
    {
        $.get("/student-extra-fee-payment/selectbox/"+id, function(data, status)
        {
            div.append(data);
        });
    }
});

$(document).on('change','#items',function () {
    var selected = $(this).find(':selected');
    var total = 0;
    tableDiv.show();
    $('#tableRow').empty();
    selected.each(function()
    {

        total= total + parseFloat($(this).data('amount'));

        $('#tableRow').append('<tr><td>'+$(this).text()+'</td><td>'+parseFloat($(this).data('amount'))+'</tr>');
    });
    $('#tableRow').append('<tr><th>Total Amount</th><td>'+total+'</tr>');

    if(total <= 0)
    {
        tableDiv.hide();
    }
});


$('form').submit(function (event) {


    $('.help-block').text('');

    event.preventDefault();
    var response = $(this).serialize();

    $.ajax({

        type: 'POST',
        url: '/student-extra-fee-payment',
        data: response,
        success: function (response) {
            if(response == "success"){
                $.pjax.reload("#pjax-container");
                toastr.success('Added Successfully');
            }
        },
        error: function (errorThrown) {

            errorThrown = errorThrown.responseJSON;

            if(errorThrown.payment_details){
                var errors = errorThrown.payment_details;

                $.each(errors, function (key, value) {

                    $('.' + key).text(value);

                    console.log(value);
                });
            }
        }
    })
});