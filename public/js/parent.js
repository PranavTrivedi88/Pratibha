$("body").unbind('click').on("click", '.open-branch-view-modal', function(){
    var id = $(this).data('id');
    NProgress.start();
    $.ajax({
        type:'GET',
        url:'/parents/'+ id,
        success:function (response) {
            $("#modal-default-body").html(response);
            NProgress.done();
            $("#modal-default").modal("show");
        },
        error:function (error) {
            toastr.error('Something went wrong.');
        }

    });
});