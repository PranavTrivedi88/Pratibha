$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue'
});

var checkBox = $("input[type='checkbox'].minimal.individual");
var checkBoxDropdown = $("input[type='checkbox'].minimal.individual_dropdown");
var checkBoxDiscount = $("input[type='checkbox'].minimal.discount-cbox");
var selectAll = $("input[type='checkbox'].minimal.all");
var lumpSumAmount = $("#lump_sum_amount");
var counter = 0;
var individualPaidAmount = $(".paid_amount");
var individualDiscount = $(".discount");
var totalPaidAmount = $("#total_paid_amount");
var netAmount = $("#net_amount");
var advanceAmount = $("#advance");
var totalDiscount = $("#total_discount");
var cashRadio = $("#cash_radio");
var chequeRadio = $("#cheque_radio");
var chequeDetails = $("#cheque_details");
var isLumpSum = false;
var totalFee = 0;
var isSelectAllChecked = true;
var discountAmount = 0;
$('.fee_type').select2({placeholder: 'Select Fees Type'});

cashRadio.iCheck("disable");
chequeRadio.iCheck("disable");
checkBoxDiscount.iCheck("disable");

selectAll.on('ifClicked', function () {
    if (isSelectAllChecked) {
        checkBox.iCheck("check");
        checkBoxDropdown.iCheck('check');
    } else {
        checkBox.iCheck("uncheck");
        checkBoxDropdown.iCheck('uncheck');
    }
});

selectAll.on('ifChecked', function () {
    isSelectAllChecked = false;
});

selectAll.on('ifUnchecked', function () {
    isSelectAllChecked = true;
});

checkBox.on('ifChecked', function (event) {
    var fee = $(event.target).data("fee");
    var parent = $(event.target).closest('td');
    parent.siblings().find('.minimal').iCheck("enable");
    parent.siblings().find('input').attr('disabled', false);
    $(event.target).parent().parent().siblings().find('input').attr('disabled', false);
    var session = $(event.target).parent().parent().siblings().find('.session_count').val();
    if (session != undefined) {
        fee = fee * session;
        parent.siblings(":last").find('input').val(fee);
    } else {
        parent.siblings(":last").find('input').val(fee);
        $(event.target).closest('tr .extra-tr').next().find('.minimal').iCheck("enable");
        $(event.target).closest('tr .extra-tr').next().find('input').attr('disabled', false);
        $(event.target).closest('tr .extra-tr').next().find('.individual').iCheck('check');
    }
    counter++;
    totalFee += fee;

    individualPaid();
    calculateNetAmount();
    calculateAdvanceAmount();

    if (counter > 0) {
        enableControls();
        console.log()
        if (counter === (checkBox.length + checkBoxDropdown.length)) {
            lumpSumAmount.attr("disabled", false);
            selectAll.iCheck("check");
        }
    }

});

checkBoxDropdown.on('ifChecked', function (event) {
    var parent = $(event.target).closest('td');
    parent.children('select').attr('disabled', false);
    parent.children('select').select2().val(parent.children('select').select2().find('option:eq(1)').val()).trigger("change")
    counter++;
    if (counter > 0) {
        enableControls();

        if (counter === (checkBox.length + checkBoxDropdown.length)) {
            lumpSumAmount.attr("disabled", false);
            selectAll.iCheck("check");
        }
    }
    $(event.target).closest('tr').next('.extra-tr').find('.minimal').iCheck("enable");
    $(event.target).closest('tr').next('.extra-tr').find('input').attr('disabled', false);
    $(event.target).closest('tr').next('.extra-tr').find('.individual').iCheck('check');
});

checkBoxDropdown.on('ifUnchecked', function (event) {
    var fee = $(event.target).data("fee");
    var parent = $(event.target).closest('td');
    parent.siblings().find('.minimal').iCheck("disable");
    parent.siblings().find('input').attr('disabled', true);
    parent.siblings().find('input').val("");
    parent.children('select').attr('disabled', true);
    counter--;
    totalFee -= parseInt(fee);

    selectAll.iCheck("uncheck");
    if (counter === 0) {
        disableControls();
    }
    lumpSumAmount.attr("disabled", true);
    lumpSumAmount.val("");
    isLumpSum = false;

    checkBox.filter(":checked").each(function () {

        var fee = $(this).data("fee");
        var parent = $(this).closest('td');

        parent.siblings().find('input').attr('disabled', false);
        parent.siblings(":last").find('input').val(fee);

    });

    $(event.target).closest('tr').next('.extra-tr').find('.minimal').iCheck("disabled");
    $(event.target).closest('tr').next('.extra-tr').find('input').attr('disabled', true);
    $(event.target).closest('tr').next('.extra-tr').find('.individual').iCheck('uncheck');

    individualPaid();
    individualDisc();
    calculateNetAmount();
    calculateAdvanceAmount();
});

checkBox.on('ifUnchecked', function (event) {
    var parent = $(event.target).closest('td');
    parent.siblings().find('.minimal').iCheck("uncheck");
    parent.siblings().find('.minimal').iCheck("disable");
    parent.siblings().find('input').attr('disabled', true);
    parent.siblings().find('input').val("");
    var session = $(event.target).parent().parent().siblings().find('.session_count').val();
    if (session == undefined) {

        $(event.target).closest('tr .extra-tr').next().find('.minimal').iCheck("disabled");
        $(event.target).closest('tr .extra-tr').next().find('input').attr('disabled', true);
        $(event.target).closest('tr .extra-tr').next().find('.individual').iCheck('uncheck');
    }
    counter--;

    totalFee -= $(event.target).data("fee");

    selectAll.iCheck("uncheck");

    if (counter === 0) {
        disableControls();
    }

    lumpSumAmount.attr("disabled", true);
    lumpSumAmount.val("");
    isLumpSum = false;

    checkBox.filter(":checked").each(function () {

        var fee = $(this).data("fee");
        var parent = $(this).closest('td');

        parent.siblings().find('input').attr('disabled', false);
        parent.siblings(":last").find('input').val(fee);
        $(this).parents('tr').find('.discount').val("");
        $(this).parents('tr').find('.discount_per').text("0");

    });

    individualPaid();
    individualDisc();
    calculateNetAmount();
    calculateAdvanceAmount();

});

$('.fee_type').on('select2:selecting', function () {
    var fee = parseInt($(this).find('option:selected').data('fee'));
    var id = parseInt($(this).find('option:selected').val());

    if (id == 1) {
        var d = new Date();
        var n = d.getMonth() + 1;
        var y = d.getFullYear();

        MonthCount = Math.ceil(moment([y, n, 28]).diff(moment([$(this).data('year'), $(this).data('month'), 1]), 'months', true));
        totalFee -= (fee * MonthCount);
    } else {
        totalFee -= fee;
    }


});

$('.fee_type').change(function (event) {

    var feeValue = $(this).find('option:selected').data('fee');
    var id = $(this).find('option:selected').val();
    var parent = $(event.target).closest('td');

    if (id == 3) {
        $(this).siblings().find('.session_count').show();
    } else {
        $(this).siblings().find('.session_count').hide();
    }
    var MonthCount = 1;
    // ---- pre month calculation
    if ($(this).val() == 1) {
        var d = new Date();
        var n = d.getMonth() + 1;
        var y = d.getFullYear();

        MonthCount = Math.ceil(moment([y, n, 28]).diff(moment([$(this).data('year'), $(this).data('month'), 1]), 'months', true));
    }

    $(this).siblings().find('.activity_fee').html(feeValue + ' /-');
    if (MonthCount > 0) {
        feeValue = feeValue * MonthCount;
    }

    parent.siblings().find('.paid_amount').val(feeValue);
    parent.siblings().find('.minimal').iCheck("enable");
    parent.siblings().find('input').attr('disabled', false);

    parent.find('.individual_dropdown').attr('data-fee', feeValue);
    parent.find('.individual_dropdown').attr('data-originalFee', feeValue);

    parent.next().find('.discount').attr('data-fee', feeValue);
    parent.next().find('.discount').attr('data-originalFee', feeValue);

    $(this).siblings().find('.pending_fee').html(feeValue + ' /-');
    $(this).siblings().find('.pending_fee').attr('data-value',feeValue);
    $(this).siblings('label').find('.course_pending_amount').val(feeValue);
    var fee = parseInt(feeValue);
    totalFee += fee;
    individualPaid();
    calculateNetAmount();

});

$('.session_count').keydown(function (event) {
    var fee = $(event.target).closest('td').siblings().find('.paid_amount').val();
    totalFee -= fee;
});

$('.session_count').keyup(function (event) {
    var session = $(this).val();
    var parent = $(event.target).closest('td');
    var feeValue = $(this).parent().siblings('.fee_type').find('option:selected').data('fee');
    if (feeValue == undefined) {
        feeValue = $(this).data('fee-value');
    }
    feeValue = feeValue * session;
    parent.siblings().find('.paid_amount').val(feeValue);
    parent.find('.individual_dropdown').attr('data-fee', feeValue);
    parent.find('.individual_dropdown').attr('data-originalFee', feeValue);

    parent.next().find('.discount').attr('data-fee', feeValue);
    parent.next().find('.discount').attr('data-originalFee', feeValue);
    $(this).parent().siblings().find('.pending_fee').html(feeValue + ' /-');
    $(this).parent().siblings().find('.pending_fee').attr('data-value',feeValue);
    $(this).parent().siblings('label').find('.course_pending_amount').val(feeValue);

    var fee = parseInt(feeValue);
    totalFee += fee;
    individualPaid();
    calculateNetAmount();

});

individualPaidAmount.on("input paste", function () {
    individualPaid();
    calculateAdvanceAmount();
});

individualDiscount.on("input paste", function () {
    individualDisc();
    calculateDiscountInPercentage(this, "amt");
    if (!isLumpSum) {
        calculatePaidAmount(this);
        individualPaid();
    }
    calculateNetAmount();
    calculateAdvanceAmount();
});

lumpSumAmount.on("input paste", function () {
    isLumpSum = true;
    individualPaidAmount.val("").attr("disabled", true);
    totalPaidAmount.text(0);
    calculateAdvanceAmount();
});

function individualPaid() {
    var total = 0;
    var value = 0;
    individualPaidAmount.each(function () {
        value = this.value ? parseInt(this.value) : 0;
        total = total + value;
    });
    totalPaidAmount.text(total);
}

function individualDisc() {
    var total = 0;
    var value = 0;
    individualDiscount.each(function () {
        value = this.value ? parseInt(this.value) : 0;
        $(this).closest("td").next().find(".discount-cbox").val(value);
        total = total + value;
    });
    totalDiscount.text(total);
}

function calculatePaidAmount(element) {

    var paidAmountSibling = $(element).closest('td').siblings(":last").find('input');
    var fee = $(element).data("fee");
    var thisValue = $(element).val() ? $(element).val() : 0;
    paidAmountSibling.val(fee - thisValue);

}

function calculateNetAmount() {
    var discount = parseInt(totalDiscount.text());
    var total = totalFee - discount;
    total = total < 0 ? 0 : total;
    netAmount.text(total);
    $("#net_amount_hidden").val(total);
}

function calculateAdvanceAmount() {
    var paidAmountValue = lumpSumAmount.val() ? parseInt(lumpSumAmount.val()) : 0;
    var advance = paidAmountValue + parseInt(totalPaidAmount.text()) - parseInt(netAmount.text());
    if (advance > 0) {
        advanceAmount.css("background-color", "#e9fded")
    } else if (advance < 0) {
        advanceAmount.css("background-color", "#fde9e9");
    } else {
        advanceAmount.css("background-color", "#eee");
    }
    advanceAmount.val(advance);
}

function enableControls() {
    cashRadio.iCheck("check");
    cashRadio.iCheck("enable");
    chequeRadio.iCheck("enable");
}

function disableControls() {
    cashRadio.iCheck("uncheck");
    cashRadio.iCheck("disable");
    chequeRadio.iCheck("uncheck");
    chequeRadio.iCheck("disable");
}

chequeRadio.on("ifChecked", function () {
    chequeDetails.show();
    $('select').select2();
});

cashRadio.on("ifChecked", function () {
    chequeDetails.hide();
});

function isNumberKey(txt, evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (evt.key === "%") {

        calculateDiscountInPercentage(txt, "%");

        individualDisc();
        if (!isLumpSum) {
            calculatePaidAmount(txt);
            individualPaid();
        }
        calculateNetAmount();
        calculateAdvanceAmount();

    } else {
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            evt.preventDefault();
        }
        return true;
    }
    evt.preventDefault();

}

function calculateDiscountInPercentage(txt, type) {

    var fee = $(txt).data("originalfee");
    discountAmount = $(txt).val();
    var amount = 0;
    if (type === "%") {
        amount = Math.round(discountAmount > 0 ? (fee * discountAmount) / 100 : 0);
        $(txt).val(amount);
        $(txt).parent().next("span").children().text(discountAmount + "%");
    } else if (type === "amt") {
        amount = discountAmount > 0 ? (discountAmount * 100) / fee : 0;
        if (amount % 10 !== 0) {
            amount = amount.toFixed(2);
        }
        $(txt).parent().next("span").children().text(amount + "%");
    }
}

$(".open-fee-history-modal").on("click", function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    NProgress.start();
    $.ajax({
        type: 'GET',
        url: '/fees/' + id,
        success: function (response) {
            $("#fee-history-body").html(response);
            NProgress.done();
            $("#fee-history-modal").modal("show");
        },
        error: function () {
            toastr.error('Something went wrong.');
        }

    });
});

function hideShowFeeDetails(ele) {

    var id = $(ele).attr("id");
    if ($(ele).hasClass("fa-plus")) {
        $(ele).removeClass("fa-plus");
        $(ele).addClass("fa-minus");
    } else {
        $(ele).removeClass("fa-minus");
        $(ele).addClass("fa-plus");
    }
    $("." + id).toggle();
}

function hideShowFeeExtraDetails(ele) {

    var id = $(ele).attr("id");
    if ($(ele).hasClass("fa-plus")) {
        $(ele).removeClass("fa-plus");
        $(ele).addClass("fa-minus");
    } else {
        $(ele).removeClass("fa-minus");
        $(ele).addClass("fa-plus");
    }
    $("." + id).toggle();
}

$('.paid_amount').change(function () {
    var input = $(this);
    var pendingFee = input.parents('tr').find('.pending_fee').data('value');
    var discount = input.parents('tr').find('.discount').val();
    input.parents('tr').find('.paid_amount_error').empty();
    if(input.val() > pendingFee)
    {
        input.parents('tr').find('.paid_amount_error').text('Paid Amount Not Greater Than Pending Fee Amount.');
        input.val(pendingFee);
        individualPaid();
        calculateAdvanceAmount();
    }

    if(discount)
    {
      var discountValue = pendingFee - discount;
      if(input.val() > discountValue)
      {
          input.val(discountValue);
      }
      individualPaid();
      calculateAdvanceAmount();
    }

});


