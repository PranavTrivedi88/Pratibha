$('#logoUpload').submit(function (event) {

    $('.help-block').text('');
    event.preventDefault();
    $.ajax({

        type: 'POST',
        url: '/auth/logoupload',
        data:  new FormData(this),
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        success: function (response) {
            if(response === "success"){
                location.reload();
                toastr.success('Logo Uploaded Successfully');
            }
        },
        error: function (errorThrown) {

            errorThrown = errorThrown.responseJSON;

            if(errorThrown.logo_details){
                var errors = errorThrown.logo_details;

                $.each(errors, function (key, value) {

                    $('.' + key).text(value);

                    console.log(value);
                });
            }
        }
    })
});