var otherBox = $("#otherSource").closest(".form-group");
var friendBox = $("#friend_name").closest(".form-group");
var tagbox = $("#tag").closest(".form-group");

otherBox.hide();
friendBox.hide();
tagbox.hide();

friendBox.css("background-color", "#f1f1f1");
tagbox.css("background-color", "#f1f1f1");
otherBox.css("background-color", "#f1f1f1");

$("tr > td:not(:last)").unbind("click").click(function () {
    $(this).siblings(":last").find(".open-inquiry-modal").click();
});


$("#inquirySource").on("change", function () {

    var selectedSource = $("#inquirySource").find(":selected").text();
    if (selectedSource === "Telephonic" || selectedSource === "Walk-in") {
        otherBox.hide();
        friendBox.show();
        tagbox.show();
    } else if (selectedSource === "Other") {
        friendBox.hide();
        tagbox.hide();
        otherBox.show();
    } else {
        friendBox.hide();
        tagbox.hide();
        otherBox.hide();
    }

});

$(".open-inquiry-modal").on("click", function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    NProgress.start();
    $.ajax({
        type: 'GET',
        url: '/inquiries/' + id,
        success: function (response) {
            $("#inquiry-modal-body").html(response);
            NProgress.done();
            $("#inquiry-modal").modal("show");
        },
        error: function () {
            toastr.error('Something went wrong.');
        }

    });
});

var searchTimeout = null;

$('#register_no').on("input", function () {

    var searchInput = $('#register_no').val();

    clearTimeout(searchTimeout);
    searchTimeout = setTimeout(function () {

        $.get('/search-student?no=' + searchInput, function (response) {
            if (response) {
                $('#name').val(response.user.name);
                $('#mobile').val(response.user.mobile);
                $('#email').val(response.user.email);
                $('#address').val(response.user.address);
                $('#city').val(response.user.city_id);
                $('#city').trigger('change');
            }
        });

    }, 500);

});

$('.delete-row').unbind('click').click(function () {
    var id = $(this).data('id');
    swal({
        title: "Are you sure to delete this item ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Confirm",
        closeOnConfirm: false,
        cancelButtonText: "Cancel"
    }, function () {
        $.ajax({
            method: 'post',
            url: '/inquiries/' + id,
            data: {
                _method: 'delete',
                _token: LA.token,
            },
            success: function (data) {
                console.log(data);
                $.pjax.reload('#pjax-container');
                if (typeof data === 'object') {
                    if (data.status) {
                        swal(data.message, '', 'success');
                    } else {
                        swal(data.message, '', 'error');
                    }
                }
            }
        });
    });
});
