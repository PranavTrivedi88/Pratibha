$(document).ready(function () {
    $(".grid-row-delete").addClass("btn");
    $(".tree_branch_delete").addClass("btn");
    $(".fa.fa-edit").removeClass("fa-edit").addClass("fa-pencil").parent().addClass("btn grid-row-edit");

    $('[type="reset"]').click(function () {
        var form = $(this).parents('form');
        form.find("select").val("").trigger('change');
        $('.select2-selection__choice').remove();
    });
});

$(document).on('pjax:end', function () {
    $(".grid-row-delete").addClass("btn");
    $(".tree_branch_delete").addClass("btn");
    $(".fa.fa-edit").removeClass("fa-edit").addClass("fa-pencil").parent().addClass("btn grid-row-edit");

    $('.modal').on('hide.bs.modal', function (e) {
        element.pop();
        console.log('closed');
        console.log(element);
    });
    $(document).on('show.bs.modal', '.modal', function (event) {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function () {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });

    $('[type="reset"]').click(function () {
        var form = $(this).parents('form');
        form.find("select").val("").trigger('change');
        $('.select2-selection__choice').remove();
    });
});

/*
 * START : Generic Functions to open modal to make entry in Masters.
 */

var element = [];

function openModal(ele, e) {

    e.preventDefault();
    element.push($(ele));
    var id = $(ele).data("modal");

    if (id === "add-parent-modal") {

        setTimeout(function () {

            $('#' + id + ' .profession').select2({placeholder: 'Select Profession'});
            $('#' + id + ' .blood-group').select2({placeholder: 'Select Blood Group'});
            $('#' + id + ' .city').select2({placeholder: 'Select City'});

        }, 500);

    } else if (id === "add-school-modal") {

        /*setTimeout(function () {
            $('#' + id + ' .city_id').select2({placeholder: 'Select City'});
            $('#' + id + ' .school_status').select2({placeholder: 'Select School Status'});
            $('#' + id + ' .branch_name').select2({placeholder: 'Select Branch'});
        }, 500);*/

    } else if (id === 'add-activity-modal') {
        $('.checkbox-minimal-blue').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
        });
    } else if(id == 'add-batch-modal')
    {
        if($(ele).parent().siblings().find('.course').val() == '')
        {
            toastr.error('Please Select Course');
            return false;
        }
        setTimeout(function () {
            $('#' + id + ' .course_id').select2({placeholder: 'Select course'});
            $('#' + id + ' .course_id').val($(ele).parent().siblings().find('.course').val()).trigger('change');
        },300);
    }

    //Remove error label and outline from controls if present
    var modal = $("#" + id);
    modal.find('[for=inputError]').siblings("br").remove();
    modal.find('[for=inputError]').remove();
    modal.find(".form-group").removeClass("has-error");

    modal.modal("show");
}

function addData(id, e) {

    e.preventDefault();
    var form = $("#" + id);

    elementModal = element[element.length - 1];

    $.ajax({

            type: 'POST',
            url: form.data("url"),
            data: form.serialize(),

            success: function (response) {

                var data = JSON.parse(response);
                form.closest('.modal').modal('hide');
                form.trigger("reset");

                if (data != null) {

                    //Get closest select element to add button
                    var select = elementModal.siblings("div").find("select");

                    //Check if it is has a profession class
                    if (select.hasClass("profession")) {
                        var profession = $(".profession");

                        //Add profession in each dropdown.
                        profession.each(function () {
                            if ($(this).find("option[value='" + data.id + "']").length) {
                                $(this).val(data.id).trigger('change');
                            } else {
                                //Set selected to true for current profession dropdown.
                                var newOption = new Option(data.name, data.id, false, select.is($(this)));
                                $(this).append(newOption).trigger('change');
                            }
                        });

                    } else if (select.hasClass("course")) {
                        var course = $(".course");

                        course.each(function () {
                            if ($(this).find("option[value='" + data.id + "']").length) {
                                $(this).val(data.id).trigger('change');
                            } else {
                                //Set selected to true for current profession dropdown.
                                var newOption = new Option(data.name, data.id, false, select.is($(this)));
                                $(this).append(newOption).trigger('change');
                            }
                        });

                    } else {

                        if (select.find("option[value='" + data.id + "']").length) {

                            select.val(data.id).trigger('change');

                        } else {

                            var name = "";

                            if (select.hasClass("parent")) {

                                name = data.name + " - " + data.mobile;

                            } else {

                                name = data.name;

                            }

                            var newOption = new Option(name, data.id, false, true);
                            select.append(newOption).trigger('change');

                        }
                    }

                    toastr.success('Record added successfully.');

                } else {
                    toastr.error('Something went wrong.');
                }
            },

            error: function (errors) {

                var parentElement = "";
                var modal = "";

                modal = $("#" + elementModal.data("modal"));
                modal.find('[for=inputError]').siblings("br").remove();
                modal.find('[for=inputError]').remove();
                modal.find(".form-group").removeClass("has-error");
                // $(".form-group").removeClass("has-error");
                if (errors.status === 406) {
                    $.each(JSON.parse(errors.responseText), function (key, value) {
                        parentElement = modal.find("#" + key.replace(".", "_")).closest(".form-group");
                        parentElement.addClass("has-error");
                        parentElement.find(".col-sm-8").prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>' + value[0] + '</label><br>')

                    });

                } else {

                    $("#" + elementModal.data("modal")).modal("hide");
                    form.trigger("reset");
                    toastr.error('Something went wrong.');

                }
            }
        }
    )
    ;
}

/*
 * END : Generic Functions to open modal to make entry in Masters.
 */
