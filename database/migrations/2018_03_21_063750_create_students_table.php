<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('student_category')->default(0);
            $table->text('second_contact_no')->nullable();
            $table->text('reference_no');
            $table->string('gender')->nullable();
            $table->string('remark')->nullable();
            $table->unsignedInteger('parent_id')->nullable();
            $table->unsignedTinyInteger('student_blood_group');
            $table->string('guardian_name')->nullable();
            $table->string('guardian_mobile')->nullable();
            $table->string('guardian_relation')->nullable();
            $table->string('guardian_email')->nullable();
            $table->unsignedTinyInteger('guardian_profession')->nullable();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('school_id')->nullable();
            $table->date('bdate')->nullable();
            $table->boolean('is_inquiry')->default(0);
            $table->string('aadhar_no')->nullable();
            $table->date('membership_date')->nullable();
            $table->boolean('membership_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
