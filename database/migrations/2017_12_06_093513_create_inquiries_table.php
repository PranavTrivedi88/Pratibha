<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inquiries', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger("user_id");
            $table->string("username")->nullable();
            $table->boolean("is_registered")->default(0);
            $table->string('name');
            $table->string('mobile');
            $table->string('alternate_mobile')->nullable();
            $table->string('email')->nullable();
            $table->text('address')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->unsignedInteger('source_id')->nullable();
            $table->string('friend_name')->nullable();
            $table->string('tag')->nullable();
            $table->string('other_source')->nullable();
            $table->unsignedInteger('branch_id')->nullable();
            $table->text('remark')->nullable();
            $table->unsignedInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inquiries');
    }
}
