<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolPaymentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_payment_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id');
            $table->date('date');
            $table->decimal('discount')->nullable();
            $table->decimal('paid_amount');
            $table->string('payment_mode');
            $table->integer('bank_id')->nullable();
            $table->string('cheque_no')->nullable();
            $table->text('remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_payment_histories');
    }
}
