<div class="box-body dependent-exam">
    <div id="dependentExamMarks">

        <table class="table table-bordered table-responsive table-striped no-header">
            <thead>
            <th></th>
            <th>Listening</th>
            <th>Reading</th>
            <th>Writing</th>
            <th>Speaking</th>
            <th>Over All</th>
            <th>Exam Year</th>
            <th>Result Status</th>
            </thead>
            <tbody>
            <tr>
                <td>
                    <b>IELTS</b>
                    <input type="hidden" name="dependent_exam[1][exam_type]" value="IELTS">
                </td>
                <td>
                    <select name="dependent_exam[0][listening]" class="form-control minimal1" id="listening1">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="dependent_exam[0][reading]" class="form-control minimal1" id="reading1">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="dependent_exam[0][writing]" class="form-control minimal1" id="writing1">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="dependent_exam[0][speaking]" class="form-control minimal1" id="speaking1">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="dependent_exam[0][overall]" class="form-control minimal1" id="overall1">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <input type="number" contenteditable="true" name="dependent_exam[0][year]" placeholder="Exam Year" id="examYear"  class="form-control input-sm" value="{{old('dependent_exam[0][year]')}}">
                </td>
                <td style="width: 12vw;">
                    <select name="dependent_exam[0][result]" class="form-control minimal1">
                        <option value=""></option>
                        @foreach(config("app.result_status") as $result)
                            <option value="{{$result}}">{{$result}}</option>
                        @endforeach
                    </select>
                </td>
            </tr>

            <tr>
                <td>
                    <b>PTE</b>
                    <input type="hidden" name="dependent_exam[1][exam_type]" value="PTE">
                </td>
                <td>
                    <select name="dependent_exam[1][listening]" class="form-control minimal1" id="listening2">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="dependent_exam[1][reading]" class="form-control minimal1" id="reading2">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="dependent_exam[1][writing]" class="form-control minimal1" id="writing2">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="dependent_exam[1][speaking]" class="form-control minimal1" id="speaking2">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="dependent_exam[1][overall]" class="form-control minimal1" id="overall2">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <input type="number" contenteditable="true" name="dependent_exam[1][year]" placeholder="Exam Year" id="examYear"  class="form-control input-sm" value="{{old('dependent_exam[1][year]')}}">
                </td>
                <td style="width: 12vw;">
                    <select name="dependent_exam[1][result]" class="form-control minimal1">
                        <option value=""></option>
                        @foreach(config("app.result_status") as $result)
                            <option value="{{$result}}">{{$result}}</option>
                        @endforeach
                    </select>
                </td>
            </tr>

            <tr>
                <td>
                    <b>TOFEL</b>
                    <input type="hidden" name="dependent_exam[2][exam_type]" value="TOFEL">
                </td>
                <td>
                    <select name="dependent_exam[2][listening]" class="form-control minimal1" id="listening3">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="dependent_exam[2][reading]" class="form-control minimal1" id="reading3">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="dependent_exam[2][writing]" class="form-control minimal1" id="writing3">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <div class="form-group" style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                        <span>- N/A -</span>
                    </div>
                </td>
                <td>
                    <select name="dependent_exam[2][overall]" class="form-control minimal1" id="overall3">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <input type="number" contenteditable="true" name="dependent_exam[2][year]" placeholder="Exam Year" id="examYear"  class="form-control input-sm" value="{{old('dependent_exam[0][year]')}}">
                </td>
                <td style="width: 12vw;">
                    <select name="dependent_exam[2][result]" class="form-control minimal1">
                        <option value=""></option>
                        @foreach(config("app.result_status") as $result)
                            <option value="{{$result}}">{{$result}}</option>
                        @endforeach
                    </select>
                </td>
            </tr>

            <tr>
                <td>
                    <b>GRE</b>
                    <input type="hidden" name="dependent_exam[3][exam_type]" value="GRE">
                </td>
                <td>
                    <div class="form-group" style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                        <span>- N/A -</span>
                    </div>
                </td>
                <td>
                    <div class="form-group" style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                        <span>- N/A -</span>
                    </div>
                </td>
                <td>
                    <div class="form-group" style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                        <span>- N/A -</span>
                    </div>
                </td>
                <td>
                    <div class="form-group" style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                        <span>- N/A -</span>
                    </div>
                </td>
                <td>
                    <select name="dependent_exam[3][overall]" class="form-control minimal1" id="overall4">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <input type="number" contenteditable="true" name="dependent_exam[3][year]" placeholder="Exam Year" id="examYear"  class="form-control input-sm" value="{{old('dependent_exam[3][year]')}}">
                </td>
                <td style="width: 12vw;">
                    <select name="dependent_exam[3][result]" class="form-control minimal1">
                        <option value=""></option>
                        @foreach(config("app.result_status") as $result)
                            <option value="{{$result}}">{{$result}}</option>
                        @endforeach
                    </select>
                </td>
            </tr>

            </tbody>
        </table>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Remarks</label>
                    <textarea name="dependentExamRemark" class="form-control" rows="3">{{old('remark')}}</textarea>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group pull-right" style="margin-top: 40px; margin-bottom: 0;">
                    <button type="button" class="btn btn-primary inquiryDetailSubmit" id="submit-dependent-exam"
                            style="margin: 20px 10px 20px 0">Save
                    </button>
                    <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
