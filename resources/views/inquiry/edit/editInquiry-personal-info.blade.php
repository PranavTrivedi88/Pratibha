<div class="form-group">
    <form action="{{url('/inquiries')}}" method="POST" name="personal-infomation" id="edit-personalInfoForm">
        {{ csrf_field() }}
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <input type="hidden" name="formName" value="personalInformation">
        <input type="hidden" id="inquiryFormId" value="{{$users->id}}" name="inquiry_id">

        <div class="row" style="background: #d6d6d6; padding-top: 20px;">
            <div class="col-md-4" style="padding: 0 5px;">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Personal Information</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            {{Session::get('inquiry1')}}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name*</label>
                                    <input type="text" class="form-control" name="name" value="{{$inquiry->name or ''}}">
                                    <span class="help-block name" style="color: red" id="name0"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email" value="{{$inquiry->email or ''}}">
                                    <span class="help-block email" style="color: red" id="email0"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Mobile*</label>
                                    <input type="text" class="form-control" name="mobile" maxlength="10" value="{{$inquiry->mobile or ''}}">
                                    <span class="help-block mobile" style="color: red" id="mobile0"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Alternate Number</label>
                                    <input type="text" class="form-control" name="alternate_mobile" maxlength="10" id="alternateMobile" value="{{$inquiry->alternate_mobile or ''}}">
                                    <span class="help-block alternate_mobile" style="color: red" id="alternate_mobile0"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Birth Date</label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control datepicker" name="birth_date" id="birth_date" value="@if(isset($inquiry->birth_date)) {{date("d-m-Y", strtotime($inquiry->birth_date))}} @else {{""}} @endif">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Gender</label><br>
                                    <label class="radio-inline"><input type="radio" name="gender" class="minimal" id="gender" value="2" @if($inquiry->gender == 2) checked @endif> Male</label>
                                    <label class="radio-inline"><input type="radio" name="gender" class="minimal" id="gender" value="1" @if($inquiry->gender == 1) checked @endif> Female</label>
                                    <label class="radio-inline"><input type="radio" name="gender" class="minimal" id="gender" value="3" @if($inquiry->gender == 3) checked @endif> Other</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Address*</label>
                                    <textarea class="form-control" name="address" style="height: 108px;">{{$inquiry->address or ''}}</textarea>
                                    <span class="help-block address" style="color: red" id="address0"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>City*</label>
                                            <select class="form-control" id="city" name="city">
                                                <option value="select">Select</option>
                                                @foreach(App\City::all() as $city)
                                                    <option value="{{$city->id}}" @if($city->id == $inquiry->city_id) selected @endif>{{$city->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Area</label><br>
                                            <select name="area" class="form-control" id="area">
                                                <option value="select">Select</option>
                                                @foreach(App\Area::all() as $area)
                                                    <option value="{{$area->id}}" @if($area->id == $inquiry->area_id) selected @endif>{{$area->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Marital Status</label></br>
                                    <label class="radio-inline"><input type="radio" name="m_status" onclick="show1()" id="m_status" value="1" @if($inquiry->marital_status == 1 ) checked @endif> Married</label>
                                    <label class="radio-inline"><input type="radio" name="m_status" onclick="show2()" id="m_status" value="2" @if($inquiry->marital_status == 2 ) checked @endif> Single</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="padding: 0 5px;">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Program Information</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Country*</label>
                                            <select name="inquiryCountry" class="form-control" id="inquiryCountry">
                                                <option value="">Select</option>
                                                @foreach(App\Country::all() as $country)
                                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="help-block inquiryCountry" style="color: red" id="country_id0"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Program*</label>
                                            <select name="course_id" class="form-control" id="inquiryProgram">
                                                <option value="">Select</option>
                                                @foreach(App\Course::groupBy('name')->get() as $course)
                                                    <option value="{{$course->id}}">{{$course->name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="help-block course_id" style="color: red" id="program_id0"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group pull-right" style="margin-top: 25px;">
                                            <button type="button" class="btn btn-info btn-sm pull-right" id="programCountryAdd" onclick="addRowCountry('preferedCountryTable')">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <textarea name="remarks" class="form-control" style="height: 107px;">{{$inquiry->remark or ''}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="preference-country-box">
                                    <table id="preferedCountryTable" class="table table-striped table-bordered"
                                           cellspacing="0" style=" width: 100%; ">
                                        <thead>
                                        <tr>
                                            <th style="width: 48%;">Country</th>
                                            <th style="width: 48%;">Program</th>
                                            <th style="width: 4%;"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="preferedCountryList">
                                        @php $i=1;
                                            $preferredCountry = $inquiry->preferedCountry;
                                        @endphp
                                        @foreach($preferredCountry as $k=>$v)
                                            <tr class="removeRow{{$i}} remove-program">
                                                <td> {{$v->country->name or ''}}
                                                    <input type="hidden" class="form-control" name="program[]" value="{{$v->country->id or ''}}">
                                                </td>
                                                <td>{{$v->program->name or ''}}
                                                    <input type="hidden" class="form-control" name="country[]" value="{{$v->program->id or ''}}">
                                                </td>
                                                <td>
                                                    <button value="Remove " type="button" class="btn btn-danger btn-sm hide1" onclick="deleteRowCountry('removeRow{{$i}}')">
                                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            @php $i++; @endphp
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="padding: 0 5px;">
                <div class="box box-primary" style="margin-bottom: 10px;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Source Information</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Source</label>
                                    <select name="source" class="form-control" id="inquiry-source" onchange="displayOtherBox();">
                                        @foreach(App\InquirySource::all() as $source)
                                            <option value="{{$source->id}}" @if($source->id == $inquiry->source_id) selected @endif>{{$source->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Branch</label>
                                    <select name="branch_id" class="form-control">
                                        @foreach(App\Branch::all() as $branch)
                                            <option value="{{$branch->id}}"  @if($branch->id == $inquiry->branch_id) selected @endif>{{$branch->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" id="otherBox" style="display: none;">
                                <div class="form-group">
                                    <label>Other Source</label>
                                    <textarea name="otherSource" class="form-control" rows="3" placeholder="Enter your source other than given source">
                                            {{$inquiry->other_source or ''}}
                                        </textarea>
                                </div>
                            </div>
                            <div class="col-md-6" id="friendBox" style="display: none;">
                                <div class="form-group">
                                    <label>Friend Name</label>
                                    <input type="text" name="friendource" class="form-control" placeholder="Enter your friend`s name" id="friendId" value="{{$inquiry->friend_name or ''}}">
                                </div>
                            </div>
                            <div class="col-md-6" id="tagbox" style="display: none;">
                                <div class="form-group" id="friendBox">
                                    <label>Tag</label>
                                    <input type="text" name="tagfriend" class="form-control" placeholder="Enter your friend`s name" id="tagId" value="{{$inquiry->tag or ''}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Inquiry Action</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Assign To</label>
                                    <select name="user_id" class="form-control" id="userData"
                                            onchange="displayReasonBox()">
                                        @foreach(Encore\Admin\Auth\Database\Administrator::all() as $user)
                                            <option value="{{$user->id}}" @if($user->id == $users->user_id) {{'selected'}} @endif>{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Status:</label>
                                    <select name="status" class="form-control">
                                        @foreach(config('app.inquiry_status') as $key => $value)
                                            <option value="{{$key}}" @if($key==$inquiry->status) {{'selected'}} @endif>{{$value['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group pull-right" style="margin-top: 20px;">
                    <button type="button" class="btn btn-primary" id="edit-personalInfo">Update</button>
                    <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
                </div>
            </div>
        </div>



    </form>
</div>
