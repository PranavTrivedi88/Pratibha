<div class="box-body">
    <table id="academicDataTable" class="form table  compact">
        <thead>
        <tr>
            <th>Exam Passed</th>
            <th>Institute / University</th>
            <th>Year Of Passing</th>
            <th>Board</th>
            <th>Result</th>
            <th>Backlogs</th>
            <th style="width: 5%;"></th>
        </tr>
        </thead>
        <tbody id="academic_container">
        <!-- row1 -->
        <tr>
            <td>
                <input type="text" placeholder="SSC" name="academic[0][exam_passed]"
                       class="form-control input-sm" id="exam_passed">
            </td>
            <td>
                <input type="text" name="academic[0][institute]" class="form-control input-sm"
                       id="institute">
            </td>
            <td>
                <input type="text" name="academic[0][year]" class="form-control input-sm" id="year">
            </td>
            <td>
                <input type="text" name="academic[0][board]" class="form-control input-sm" id="board">
            </td>
            <td>
                <input type="text" name="academic[0][result]" class="form-control input-sm"
                       id="result">
            </td>
            <td>
                <input type="text" name="academic[0][backlog]" class="form-control input-sm"
                       id="result">
            </td>
        </tr>
        <!-- row2 -->
        <tr>
            <td>
                <input type="text" placeholder="HSC" name="academic[1][exam_passed]"
                       class="form-control input-sm" id="exam_passed">
            </td>
            <td>
                <input type="text" name="academic[1][institute]" class="form-control input-sm"
                       id="institute">
            </td>
            <td>
                <input type="text" name="academic[1][year]" class="form-control input-sm" id="year">
            </td>
            <td>
                <input type="text" name="academic[1][board]" class="form-control input-sm" id="board">
            </td>
            <td>
                <input type="text" name="academic[1][result]" class="form-control input-sm"
                       id="result">
            </td>
            <td>
                <input type="text" name="academic[1][backlog]" class="form-control input-sm"
                       id="result">
            </td>
        </tr>
        <!-- row3 -->
        <tr>
            <td>
                <input type="text" placeholder="Diploma" name="academic[2][exam_passed]"
                       class="form-control input-sm" id="exam_passed">
            </td>
            <td>
                <input type="text" name="academic[2][institute]" class="form-control input-sm"
                       id="institute">
            </td>
            <td>
                <input type="text" name="academic[2][year]" class="form-control input-sm" id="year">
            </td>
            <td>
                <input type="text" name="academic[2][board]" class="form-control input-sm" id="board">
            </td>
            <td>
                <input type="text" name="academic[2][result]" class="form-control input-sm"
                       id="result">
            </td>
            <td>
                <input type="text" name="academic[2][backlog]" class="form-control input-sm"
                       id="result">
            </td>
        </tr>
        <!-- row4 -->
        <tr>
            <td>
                <input type="text" placeholder="Graduation" name="academic[3][exam_passed]"
                       class="form-control input-sm" id="exam_passed">
            </td>
            <td>
                <input type="text" name="academic[3][institute]" class="form-control input-sm"
                       id="institute">
            </td>
            <td>
                <input type="text" name="academic[3][year]" class="form-control input-sm" id="year">
            </td>
            <td>
                <input type="text" name="academic[3][board]" class="form-control input-sm" id="board">
            </td>
            <td>
                <input type="text" name="academic[3][result]" class="form-control input-sm"
                       id="result">
            </td>
            <td>
                <input type="text" name="academic[3][backlog]" class="form-control input-sm"
                       id="result">
            </td>
        </tr>
        <!-- row5-->
        <tr>
            <td>
                <input type="text" placeholder="Master" name="academic[4][exam_passed]"
                       class="form-control input-sm" id="exam_passed">
            </td>
            <td>
                <input type="text" name="academic[4][institute]" class="form-control input-sm"
                       id="institute">
            </td>
            <td>
                <input type="text" name="academic[4][year]" class="form-control input-sm" id="year">
            </td>
            <td>
                <input type="text" name="academic[4][board]" class="form-control input-sm" id="board">
            </td>
            <td>
                <input type="text" name="academic[4][result]" class="form-control input-sm"
                       id="result">
            </td>
            <td>
                <input type="text" name="academic[4][backlog]" class="form-control input-sm"
                       id="result">
            </td>
        </tr>
        <!-- row6 -->
        <tr>
            <td>
                <input type="text" placeholder="ITI" name="academic[5][exam_passed]"
                       class="form-control input-sm" id="exam_passed">
            </td>
            <td>
                <input type="text" name="academic[5][institute]" class="form-control input-sm"
                       id="institute">
            </td>
            <td>
                <input type="text" name="academic[5][year]" class="form-control input-sm" id="year">
            </td>
            <td>
                <input type="text" name="academic[5][board]" class="form-control input-sm" id="board">
            </td>
            <td>
                <input type="text" name="academic[5][result]" class="form-control input-sm"
                       id="result">
            </td>
            <td>
                <input type="text" name="academic[5][backlog]" class="form-control input-sm"
                       id="result">
            </td>
        </tr>
        <!-- row7 -->
        <tr>
            <td>
                <input type="text" placeholder="Other" name="academic[6][exam_passed]"
                       class="form-control input-sm" id="exam_passed">
            </td>
            <td>
                <input type="text" name="academic[6][institute]" class="form-control input-sm"
                       id="institute">
            </td>
            <td>
                <input type="text" name="academic[6][year]" class="form-control input-sm" id="year">
            </td>
            <td>
                <input type="text" name="academic[6][board]" class="form-control input-sm" id="board">
            </td>
            <td>
                <input type="text" name="academic[6][result]" class="form-control input-sm"
                       id="result">
            </td>
            <td>
                <input type="text" name="academic[6][backlog]" class="form-control input-sm"
                       id="result">
            </td>
            <td>
                <button type="button" class="btn btn-info btn-sm" id="academicAdd">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
            </td>
        </tbody>
    </table>
    <div class="col-md-12 form-group" align="right">
        <button type="button" class="btn btn-primary inquiryDetailSubmit" id="submitAcademic"
                style="margin: 20px 10px 20px 0">Save
        </button>
        <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
    </div>
</div>
