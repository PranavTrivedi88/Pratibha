<div class="box-body">
    <table id="dependentWorkTable" class="form table  compact">
        <thead>
        <tr>
            <th>Company Name</th>
            <th>Designation</th>
            <th>Duration</th>
            <th>Location</th>
            <th>Remark</th>
            <th></th>
        </tr>
        </thead>
        <tbody id="dependent_work_experience_container">
        <!-- row1 -->
        <tr>
            <td>
                <input type="text" name="dependentWork[0][company]" class="form-control input-sm"
                       id="dependent_company">
            </td>
            <td>
                <input type="text" name="dependentWork[0][designation]"
                       class="form-control input-sm"
                       id="dependent_designation">
            </td>
            <td>
                <div class="row">
                    <div class="col-md-6">
                        <input type="number" name="dependentWork[0][durationYears]" placeholder="Years" class="form-control input-sm"
                               id="duration">
                    </div>
                    <div class="col-md-6">
                        <input type="number" name="dependentWork[0][durationMonths]" placeholder="Months" class="form-control input-sm"
                               id="duration">
                    </div>
                </div>
            </td>
            <td>
                <input type="text" name="dependentWork[0][location]" class="form-control input-sm"
                       id="dependent_location">
            </td>
            <td>
                <input type="text" name="dependentWork[0][remark]" class="form-control input-sm"
                       id="dependent_remark">
            </td>
        </tr>
        <!-- row2 -->
        <tr>
            <td>
                <input type="text" name="dependentWork[1][company]" class="form-control input-sm"
                       id="dependent_company">
            </td>
            <td>
                <input type="text" name="dependentWork[1][designation]"
                       class="form-control input-sm"
                       id="dependent_designation">
            </td>
            <td>
                <div class="row">
                    <div class="col-md-6">
                        <input type="number" name="dependentWork[1][durationYears]" placeholder="Years" class="form-control input-sm"
                               id="duration">
                    </div>
                    <div class="col-md-6">
                        <input type="number" name="dependentWork[1][durationMonths]" placeholder="Months" class="form-control input-sm"
                               id="duration">
                    </div>
                </div>
            </td>
            <td>
                <input type="text" name="dependentWork[1][location]" class="form-control input-sm"
                       id="dependent_location">
            </td>
            <td>
                <input type="text" name="dependentWork[1][remark]" class="form-control input-sm"
                       id="dependent_remark">
            </td>
        </tr>
        <!-- row3 -->
        <tr>
            <td>
                <input type="text" name="dependentWork[2][company]" class="form-control input-sm"
                       id="dependent_company">
            </td>
            <td>
                <input type="text" name="dependentWork[2][designation]"
                       class="form-control input-sm"
                       id="dependent_designation">
            </td>
            <td>
                <div class="row">
                    <div class="col-md-6">
                        <input type="number" name="dependentWork[2][durationYears]" placeholder="Years" class="form-control input-sm"
                               id="duration">
                    </div>
                    <div class="col-md-6">
                        <input type="number" name="dependentWork[2][durationMonths]" placeholder="Months" class="form-control input-sm"
                               id="duration">
                    </div>
                </div>
            </td>
            <td>
                <input type="text" name="dependentWork[2][location]" class="form-control input-sm"
                       id="dependent_location">
            </td>
            <td>
                <input type="text" name="dependentWork[2][remark]" class="form-control input-sm"
                       id="dependent_remark">
            </td>
            <td>
                <button type="button" class="btn btn-info btn-sm"
                        onclick="addRowWorkExperience('dependentWorkTable')">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
            </td>
        </tr>
        </tbody>
    </table>
    <div class="col-md-12 form-group" align="right">
        <button type="button" class="btn btn-primary inquiryDetailSubmit" id="submit-dependent-work-experience"
                style="margin-right: 10px">Save
        </button>
        <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
    </div>
</div>