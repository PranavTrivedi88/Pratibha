<table class="table no-header table-bordered">
    <tbody>
    <tr>
        <td width="30%">
            <b>Father Name</b>
        </td>
        <td>
            {{$parents->father_name or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Father Mobile No.</b>
        </td>
        <td>
            {{$parents->father_mobile or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Father Email</b>
        </td>
        <td>
            {{$parents->father_email or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Father Profession</b>
        </td>
        <td>
            {{$parents->fatherProfession->name or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Father Blood Group</b>
        </td>
        <td>
            @if(isset($parents->father_blood_group))
                @foreach(config('app.blood_group') as $key => $value)
                    @if($parents->father_blood_group == $key)
                        {{ $value }}
                    @endif
                @endforeach
            @else
                -
            @endif
        </td>
    </tr>
    <tr>
        <td>
            <b>Father Aadhar No.</b>
        </td>
        <td>
            {{$parents->aadhar_no_father or "-"}}
        </td>
    </tr>
    <tr>
        <td width="20%">
            <b>Mother Name</b>
        </td>
        <td>
            {{$parents->mother_name or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Mother Mobile No.</b>
        </td>
        <td>
            {{$parents->mother_mobile or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Mother Email</b>
        </td>
        <td>
            {{$parents->mother_email or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Mother Profession</b>
        </td>
        <td>
            {{$parents->motherProfession->name or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Mother Blood Group</b>
        </td>
        <td>
            @if(isset($parents->mother_blood_group))
                @foreach(config('app.blood_group') as $key => $value)
                    @if($parents->mother_blood_group == $key)
                        {{ $value }}
                    @endif
                @endforeach
            @else
                -
            @endif
        </td>
    </tr>
    <tr>
        <td>
            <b>Mother Aadhar No.</b>
        </td>
        <td>
            {{$parents->aadhar_no_mother or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>City</b>
        </td>
        <td>
            {{$parents->city->name or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Address</b>
        </td>
        <td>
            {{$parents->address or "-"}}
        </td>
    </tr>
    </tbody>
</table>
