<div class="box-body">

    <div class="fields-group">
        <div class="form-group">
            <label class="col-sm-3 control-label">Activity:</label>
            <div class="col-sm-8">
                <select class="form-control" id="activity_name" name="activity_name">
                    <option value="">Select Activity</option>
                    @if(isset($activities))
                        @foreach($activities as $activity)
                            <option value="{{$activity->id}}" {{ (isset($branchExtraFee->activity_id) && $branchExtraFee->activity_id == $activity->id) ? 'selected' : '' }} >{{$activity->name}}</option>
                        @endforeach
                    @endif
                </select>
                <span class="help-block activity_name" style="color: red" id="errorMessage"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Student:</label>
            <div class="col-sm-8">
                <select class="form-control" id="student_name" name="student_name">
                    <option value="">Select Student</option>
                    @if(isset($students))
                        @foreach($students as $student)
                            <option value="{{$student->id}}" {{ (isset($studentEFPayment->student_id) && $studentEFPayment->student_id == $student->id) ? 'selected' : '' }} >{{$student->name}}</option>
                        @endforeach
                    @endif
                </select>
                <span class="help-block student_name" style="color: red" id="errorMessage"></span>
            </div>
        </div>
        <div id="selectBox">

        </div>
        <div class="form-group" id="tableDiv">
            <div class="col-sm-12">
                <table class="table table-bordered">
                    <thead>
                        <th>Items</th>
                        <th>Amount</th>
                    </thead>
                    <tbody id="tableRow">

                    </tbody>
                </table>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Paid Amount:</label>
            <div class="col-sm-8">
                <input type="text" class="form-control " name="amount" placeholder="Input Paid Amount"
                       value="{{ $studentEFPayment->amount or '' }}">
                <span class="help-block amount" style="color: red"></span>
            </div>
        </div>

        @if(isset($branch))
            <input type="hidden" name="branch_id" value="{{ $branch->id }}">
        @endif

    </div>

</div>

<div class="box-footer">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="btn-group pull-right">
            <button type="submit" class="btn btn-info pull-right"
                    data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit
            </button>
        </div>
        <div class="btn-group pull-left">
            <button type="reset" class="btn btn-warning">Reset</button>
        </div>
    </div>
</div>