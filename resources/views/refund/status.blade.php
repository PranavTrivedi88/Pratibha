<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title">School Refund Information</h3>
  </div>
  <table class="table table-bordered text-center">
    <tbody>
        <tr>
            <th>Date</th>
            <th>By</th>
            <th>Amount</th>
        </tr>
      @forelse ($refundHistory as $history)
        <tr>
            <td>{{ Carbon\Carbon::parse($history->date)->format('d-m-Y') }}</td>
            <td>
                  
                  @if($history->refund_mode == "Cheque")

                      <span class="btn btn-link" title="Cheque Details" data-toggle="popover" data-trigger="hover" data-content="<b>Bank Name: {{ App\Bank::select('name')->find($history->bank_id)->name }} </b> <br/> <b>Cheque No: {{ $history->cheque_no}}</b>">{{ $history->refund_mode }}</span>
                
                  @else
                  
                    {{ $history->refund_mode }}
                 
                  @endif
            </td>
            <td>{{ number_format($history->amount,2) }}</td>
        </tr>

      @empty
        <tr>
          <td colspan="3">Records Not Available</td>
        </tr>
      @endforelse
      <tr>
          <th colspan="2">Total Refund Amount</th>
          <th>
                {{ number_format($refundHistory->sum('amount'),2) }}
          </th>
      </tr>
    </tbody>
</table>
    
</div>