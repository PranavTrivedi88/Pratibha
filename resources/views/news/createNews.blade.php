
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Post News</h3>

    </div>
    <form action="{{url('/news')}}" method="POST" class="form-horizontal" >

        {{ csrf_field() }}

        @include('news.form')

    </form>
    <!-- /.box-body -->
</div>
<style>
    hr {
        border: 0;
        width: 96%;
        height: 1px;
    }
</style>