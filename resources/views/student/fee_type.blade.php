<div class="col-md-12">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title"> {{ $user->name }}</h3>

            <div class="box-tools">
                <div class="btn-group pull-right" style="margin-right: 10px">
                    <a class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
                </div>
            </div>
        </div>
        <form method="post" action="/students/edit-fee-type" id="" name="">
            {{ csrf_field() }}
            <div class="container-fluid" style="padding-top: 10px;">

                <table class="table table-responsive table-bordered">
                    <tr>
                        <th width="30%"> Course</th>
                        <th width="30%"> Types</th>
                        <th width="30%"> Change</th>
                    </tr>
                    @if(count($courses) == 0)
                        <tr>
                            <td colspan="3"> No Data</td>
                        </tr>
                    @else
                        @foreach($courses as $key => $course)
                            <tr>
                                <td> {{ $course->course->activity->name }} </td>
                                <td>
                                    {{ ($course->fee_type) ? config('app.fee_type')[$course->fee_type]['name'] : 'Fee Type Not Decided'  }}
                                    @if($course->pendingFees)
                                        <label class="label label-danger">
                                            Your fees are pending for this course. Please submit fees first.
                                        </label>
                                    @endif
                                </td>
                                <td>
                                    <input type="hidden" name="course[{{ $key }}][courseStudentId]"
                                           value="{{ $course->id }}">
                                    <select class="form-control fee_type" name="course[{{ $key }}][feeType]">
                                        <option></option>
                                        @foreach(config('app.fee_type') as $types)
                                            <option value="{{ $types['id'] }}"> {{ $types['name'] }} </option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3">
                                <button type="submit" class="btn btn-primary pull-right"> Submit</button>
                            </td>
                        </tr>
                    @endif
                </table>
            </div>

        </form>
    </div>
</div>