<div class="modal-content">
    <form id="markUpload" action="{{ url('students/import') }}" method="post" enctype="multipart/form-data">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-2 text-right">
                    <h5><b>STEP 1</b></h5>
                </div>
                <div class="col-md-10">
                    <h5><a href="{{ url('students/import/download?' . time()) }}" target="_blank" class="label label-success">Click here</a> to download blank file to fill students.</h5>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-2 text-right">
                    <h5><b>STEP 2</b></h5>
                </div>
                <div class="col-md-10">
                    <h5>Fill all information of teachers.</h5>
                    <h4 style="color: darkorange"><i class="fa fa-info-circle"></i> Please remove all demo data from sheet.</h4>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-2 text-right">
                    <h5><b>STEP 3</b></h5>
                </div>
                <div class="col-md-10">
                    <h5><b>Upload File</b></h5>
                    <div>
                        <label class="btn btn-default btn-sm btn-file">
                            Browse <input type="file" accept=".xls" required class="mark_file" name="bulk_file" style="display: none" />
                        </label>
                        <span id="fileName">select file</span>
                        <p class="text-danger">{{ $errors->first('bulk_file') }}</p>
                    </div>
                    <hr>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-2">&nbsp;</div>
                <div class="col-md-10">
                    <button type="submit" data-loading-text="Uploading..."
                            class="btn btn-info" title="Please Select File"
                            disabled="true" id="submitMarkUpload">Upload File</button>
                </div>
            </div>

        </div>

        {{ csrf_field() }}
    </form>


    <script>
        $(document).on('change', ':file', function() {
            var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });

        $(document).ready( function() {
            var fileName = "";
            var same = false;
            $(':file').on('fileselect', function (event, numFiles, label) {

                same = (fileName != label);

                $('#fileName').text(label);
                $('#submitMarkUpload').removeAttr('disabled');
            });

            $('#departments').select2({width: 280});

            $('#markUpload').submit(function () {
                return confirm('This action can not be undone. Are you sure wanted to upload student?');
            });
        });
    </script>
</div>
@if($excel_errors)
    @include('errors.excel_error')
@endif