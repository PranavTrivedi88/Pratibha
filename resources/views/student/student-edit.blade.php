<form action="/students/{{$student->id}}" id="student_edit_form" method="patch" pjax-container
      class="form-horizontal">
    {{ csrf_field() }}

    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Edit</h3>

                <div class="box-tools">
                    <div class="btn-group pull-right" style="margin-right: 10px">
                        <a class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
                    </div>
                </div>
            </div>
            <div class="container-fluid" style="padding-top: 10px;">
                <div class="form-group">
                    <label class="col-sm-2 control-label"><img src="{{ url('img/boy.png') }}" width="25px"></label>
                    <div class="col-sm-8">
                        <h4> Student's Information </h4>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                            <input type="text" id="name" name="name" value="{{ $student->name }}"
                                   class="form-control name" placeholder="Input Name" autofocus>
                        </div>
                        <div class="col-sm-12 error" style="color: red;" id="error_name">

                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Select Gender</label>
                    <div class="col-sm-8">
                        <select placeholder="Select Gender" class="form-control gender" name="gender">
                            <option value="">Select Gender</option>
                            <option value="M" {{ ('M' == $student->student->gender) ? 'selected' : '' }}>Male</option>
                            <option value="F" {{ ('F' == $student->student->gender) ? 'selected' : '' }}>Female</option>
                        </select>
                        <div class="col-sm-12 error" style="color: red;" id="error_gender">

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Branch Name</label>

                    <div class="col-sm-8">
                        <select placeholder="Select Branch" class="form-control branch_id" name="branch_name" id="branch_name">
                            <option></option>
                            @foreach(App\Branch::all() as $branch)
                                <option value="{{ $branch->id }}" {{ ($branch->id == $student->student->branch_id) ? 'selected' : '' }}> {{ $branch->name }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Parent</label>

                    <div class="col-sm-8">
                        <select class="form-control parent" id="parents" placeholder="Select Parent" name="parent">
                            <option></option>
                            @foreach($parents as $k => $val)
                                <option value="{{ $val->id }}"
                                        {{ ($val->id == $student->student->parent_id) ? 'selected' : '' }} >
                                    {{ $val->father_name }} - {{ $val->father_mobile }}
                                </option>
                            @endforeach
                        </select>
                        <div id="error_parent" class="col-sm-12 error" style="color: red;">

                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">Birth Date </label>
                    <div class="col-sm-8" style="">
                        <input type="text" value="{{ $student->student->bdate }}" name="bdate" placeholder="Birth Date"
                               class="bdate form-control">
                    </div>
                </div>
                <div class="form-group  ">
                    <label for="mobile" class="col-sm-2 control-label">Mobile</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                            <input style="width: 150px" maxlength="10" type="text" id="mobile" name="mobile"
                                   value="{{ $student->contact_no }}" class="form-control mobile"
                                   placeholder="Primary Contact">
                        </div>
                        <label id="error_mobile"  class="error col-sm-12">

                        </label>
                    </div>
                </div>
                <div class="form-group  ">
                    <label for="mobile" class="col-sm-2 control-label">Secondary Contact</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                            <input style="width: 150px" maxlength="10" type="text" id="second_contact_no" name="second_contact_no" value=""
                                   class="form-control mobile" placeholder="Secondary Contact">
                        </div>
                        <label id="error_mobile"  class="error col-sm-12">

                        </label>
                    </div>

                </div>
                <div class="form-group  ">
                    <label for="email" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                            <input type="text" id="email" name="email" value="{{$student->email}}"
                                   class="form-control email" placeholder="Input Email">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">School</label>

                    <div class="col-sm-8">
                        <select placeholder="Select School" class="form-control school" name="school" id="school">
                            <option></option>
                            @foreach($schools as $k => $val)
                                <option value="{{ $val->id }}" {{ ($val->id == $student->student->school_id) ? 'selected' : '' }}> {{ $val->name }} </option>
                            @endforeach
                        </select>
                        <div id="error_school" class="col-sm-12 error" style="color: red;">

                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Same As Parent</label>

                    <div class="col-sm-8 ">
                        <input type="checkbox" name="same_as_parent" id="same_as_parent">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">City</label>

                    <div class="col-sm-8">
                        <select placeholder="Select City" class="form-control city_id" name="city" id="city">
                            <option></option>
                            @foreach($cities as $k => $val)
                                <option value="{{ $val->id }}" {{ ($val->id == $student->city_id) ? 'selected' : '' }}> {{ $val->name }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group  ">
                    <label for="address" class="col-sm-2 control-label">Address</label>

                    <div class="col-sm-8">
                        <textarea name="address" class="form-control" rows="5"
                                  placeholder="Input Address"> {{ $student->address }} </textarea>
                        <div id="error_address" style="color: red;" class="error col-sm-12">

                        </div>
                    </div>

                </div>
                <div class="form-group  ">
                    <label for="address" class="col-sm-2 control-label">Aadhar No</label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="aadhar_no" name="aadhar_no" value="{{ $student->student->aadhar_no }}" placeholder="Input Aadhar No">
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="box box-warning">
            <div class="container-fluid" style="padding-top: 10px;">

                <div class="form-group">
                    <label class="col-sm-2 control-label"><img src="{{ url('img/guardian.png') }}" width="25px"></label>
                    <div class="col-sm-8">
                        <h4> Guardian's Information </h4>
                    </div>
                </div>

                <div class="form-group  ">
                    <label for="address" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-8">
                        <input type="text" value="{{ $student->student->guardian_name }}" class="form-control"
                               name="guardian_name">
                    </div>

                </div>

                <div class="form-group  ">
                    <label for="address" class="col-sm-2 control-label">Relation</label>

                    <div class="col-sm-8">
                        <select type="text" class="form-control relation" name="guardian_relation">
                            <option value="">Select Relation</option>
                            @foreach(config('app.relation') as $k => $val)

                                <option value="{{ $k }}" {{ ($student->student->guardian_relation == $k) ? 'selected' : '' }} > {{ $val }} </option>

                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="form-group  ">
                    <label for="address" class="col-sm-2 control-label">Profession</label>

                    <div class="col-sm-8">

                        <select class="form-control professions" id="profession" name="guardian_profession">
                            <option></option>
                            @foreach($professions as $profession)
                                <option value="{{ $profession->id }}" {{ ($profession->id == $student->student->guardian_profession) ? 'selected' : '' }}> {{ $profession->name }} </option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <div class="form-group  ">
                    <label for="address" class="col-sm-2 control-label">Mobile</label>

                    <div class="col-sm-8">
                        <input type="text" maxlength="10" value="{{ $student->student->guardian_mobile }}"
                               class="form-control" name="guardian_mobile">
                    </div>

                </div>

                <div class="form-group  ">
                    <label for="address" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-8">
                        <input type="text" value="{{ $student->student->guardian_email }}" class="form-control"
                               name="guardian_email">
                    </div>

                </div>


            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="box box-success">
            <div class="container-fluid" style="padding-top: 10px;">
                <div class="form-group">
                    <label class="col-sm-2 control-label"><img src="{{ url('img/activity.png') }}" width="25px"></label>
                    <div class="col-sm-8">
                        <h4> Enroll To Activity </h4>
                    </div>
                </div>

                <input type="hidden" name="username" value="" class="username">
                <input type="hidden" name="password" value="" class="password">
                <span id="course_date_div">

					<div class="form-group">
						<label for="address" class="col-sm-2 control-label">Activity</label>
						<div class="col-sm-8">
							<select placeholder="Select Activity" class="form-control course"
                                    onchange="courseChange(this)" id="1" name="courses[]">
									<option value="">Select Activity</option>
                                @foreach($course as $key => $value)
                                    <option value="{{ $value->id }}">
										{{ $value->activity->name }} {{ showActivityBranchName($value) }}
									</option>

                                @endforeach
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="address" class="col-sm-2 control-label">Batch</label>
						<div class="col-sm-8">
							<select placeholder="Select Batch" class="form-control batch" id="batch-1" name="batches[]">
								<option></option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="address" class="col-sm-2 control-label"> Starts From </label>
						<div class="col-sm-5" style="">
							<input type="text" name="date[]" placeholder="Start Month" class="date form-control">
						</div>

						<button class="btn btn-default add-box" type="button"><i class="fa fa-plus"></i></button>

					</div>
				</span>
                @foreach($combo as $key => $val)

                    <div>

                        <div class="form-group">
                            <label for="address" class="col-sm-2 control-label">Activity</label>
                            <div class="col-sm-8">
                                <select placeholder="Select Course" class="form-control course"
                                        onchange="courseChange(this)" id="{{$key + 2}}" name="courses[]">
                                    <option value="">Select Activity</option>
                                    @foreach($course as $k => $value)
                                        <option value="{{ $value->id }}"
                                                {{ ($value->id == $val['course_id']) ? 'selected' : '' }}>
                                            {{ $value->activity->name }} {{ showActivityBranchName($value) }}
                                        </option>

                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="address" class="col-sm-2 control-label">Batch</label>
                            <div class="col-sm-8">
                                <select placeholder="Select Batch" class="form-control batch" id="batch-{{$key + 2}}"
                                        name="batches[]">
                                    <option></option>
                                    @foreach($val['batches'] as $k => $v)

                                        <option value="{{ $k }}" {{ ($k == $val['batch_id']) ? 'selected' : '' }}> {{ $v }} </option>

                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="col-sm-2 control-label">Date</label>
                            <div class="col-sm-5" style="">
                                <input type="text" name="date[]" value="{{ $val['start_date'] }}"
                                       placeholder="Start Date" class="date form-control">
                            </div>
                            <button class="btn btn-primary remove-box" type="button"><i class="fa fa-minus"></i></button>

                        </div>
                        </span>

                    </div>

                @endforeach

                <div class="form-group">
                    <div class="col-sm-12">
                        <label id="error_course">

                        </label>
                    </div>
                    <div class="col-sm-12">

                        <label id="error_batch">

                        </label>

                    </div>
                    <div class="col-sm-12">
                        <label id="error_date">

                        </label>
                    </div>
                </div>

                <div class="form-group  pull-right">
                    <div class="col-sm-12 pull-right">
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>


<div id="template" style="display: none;">

    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">Activity</label>
        <div class="col-sm-8">
            <select class="form-control course" onchange="courseChange(this)" id="" name="courses[]">
                <option value="">Select Activity</option>
                @foreach($course as $key => $value)

                    <option value="{{ $value->id }}">
                        {{ $value->activity->name }} {{ showActivityBranchName($value) }}
                    </option>

                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">Batch</label>
        <div class="col-sm-8">
            <select class="form-control batch" id="" name="batches[]">
                <option>Batch</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">Starts From</label>
        <div class="col-sm-5" style="">
            <input type="text" name="date[]" placeholder="Start Month" class="date form-control">
        </div>
        <button class="btn btn-danger remove-box" type="button"><i class="fa fa-minus"></i></button>
    </div>
</div>

