<div class="modal fade student-modal" tabindex="-1" role="dialog" style="display: none;" id="openModal" aria-labelledby="myModalLabel">
    <div class="modal-dialog detail" role="document" style="width: 900px;">
        <div class="modal-content data-div">

        </div>
    </div>
</div>

<div class="modal fade history-modal" tabindex="-1" style="display: none" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content history-data">
            
        </div>
    </div>
</div>

<div class="modal fade course-stop-modal" tabindex="-1" style="display: none" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content ourse-stop-data">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Stop Activity</h4>
            </div>
            <div class="modal-body text-center">
                <h2 id="course-stop-heading">Some text in the modal.</h2>
                <h4 id="course-stop-heading2"></h4>
                <label style="margin-top: 5px;"><input type="checkbox" id="refund"> &nbsp;Refund Fee</label>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" id="stopActivity" data-userid="" data-courseid="">Stop</button>
            </div>

        </div>
    </div>
</div>