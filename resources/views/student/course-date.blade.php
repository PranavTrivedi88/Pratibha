<form action="/students" id="student_form" method="post" pjax-container class="form-horizontal">
    {{ csrf_field() }}
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Create</h3>

                <div class="box-tools">
                    <div class="btn-group pull-right" style="margin-right: 10px">
                        <a class="btn btn-sm btn-default form-history-back"><i
                                    class="fa fa-arrow-left"></i>&nbsp;Back</a>
                    </div>
                </div>
            </div>
            <div class="container-fluid" style="padding-top: 10px;">
                <div class="form-group">
                    <label class="col-sm-2 control-label"><img src="{{ url('img/boy.png') }}" width="25px"></label>
                    <div class="col-sm-8">
                        <h4> Student's Information </h4>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-8">
                        <input type="text" id="name" name="name" value="" class="form-control name"
                               placeholder="Input Name" autofocus>
                        <div class="col-sm-12 error" style="color: red;" id="error_name">

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Select Gender</label>
                    <div class="col-sm-8">
                        <select placeholder="Select Gender" class="form-control gender" name="gender">
                            <option value="">Select Gender</option>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                        <div class="col-sm-12 error" style="color: red;" id="error_gender">

                        </div>
                    </div>
                </div>
                @if(isRole('administrator') || isRole('super-administrator'))
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Branch Name</label>

                        <div class="col-sm-8">
                            <select placeholder="Select Branch" class="form-control branch_id" name="branch_name"
                                    id="branch_name">
                                <option></option>
                                @foreach(App\Branch::all() as $branch)
                                    <option value="{{ $branch->id }}"> {{ $branch->name }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif

                @if(isset($branch))
                    <input type="hidden" name="branch_name" value="{{ $branch->id }}">
                @endif
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Parent</label>

                    <div class="col-sm-8">
                        <select class="form-control parent" id="parents" placeholder="Select Parent" name="parent">
                            <option></option>
                            @foreach($parents as $k => $val)
                                <option value="{{ $val->id }}"> {{ $val->father_name }}
                                    - {{ $val->father_mobile }} </option>
                            @endforeach
                        </select>
                        <label id="error_parent" class="col-sm-12 error">

                        </label>
                    </div>

                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">Birth Date</label>
                    <div class="col-sm-8" style="">
                        <input type="text" name="bdate" placeholder="Birth Date" class="bdate form-control">
                    </div>
                </div>
                <div class="form-group  ">
                    <label for="mobile" class="col-sm-2 control-label">Primary Contact</label>
                    <div class="col-sm-8">

                        <input style="width: 150px" maxlength="10" type="text" id="mobile" name="mobile" value=""
                               class="form-control mobile" placeholder="Primary Contact">
                        <label id="error_mobile" class="error col-sm-12">

                        </label>
                    </div>

                </div>
                <div class="form-group ">
                    <label for="mobile" class="col-sm-2 control-label">Secondary Contact</label>
                    <div class="col-sm-8">


                        <input style="width: 150px" maxlength="10" type="text" id="second_contact_no"
                               name="second_contact_no" value=""
                               class="form-control mobile" placeholder="Secondary Contact">

                        <label id="error_mobile" class="error col-sm-12">

                        </label>
                    </div>

                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-8">
                        <input type="text" id="email" name="email" value="" class="form-control email"
                               placeholder="Input Email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label"> Blood Group </label>

                    <div class="col-sm-8">
                        <select class="form-control" placeholder="Select Blood Group" id="blood_group">
                            <option></option>
                            @foreach(config('app.blood_group') as $k => $group)
                                <option value="{{ $k }}"> {{ $group }} </option>
                            @endforeach
                        </select>

                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">School</label>

                    <div class="col-sm-8">
                        <select placeholder="Select School" class="form-control school" name="school" id="school">
                            <option></option>
                            @foreach($schools as $k => $val)
                                <option value="{{ $val->id }}"> {{ $val->name }} </option>
                            @endforeach
                        </select>
                        <label id="error_school" class="col-sm-12 error">

                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Same As Parent</label>

                    <div class="col-sm-8 ">
                        <input type="checkbox" name="same_as_parent" id="same_as_parent">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">City</label>

                    <div class="col-sm-8">
                        <select placeholder="Select City" class="form-control city_id" name="city" id="city">
                            <option></option>
                            @foreach($cities as $k => $val)
                                <option value="{{ $val->id }}"> {{ $val->name }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group ">
                    <label for="address" class="col-sm-2 control-label">Address</label>

                    <div class="col-sm-8">
                        <textarea id="address" name="address" class="form-control" rows="5"
                                  placeholder="Input Address"></textarea>
                        <label id="error_address" class="error col-sm-12">

                        </label>
                    </div>

                </div>
                <div class="form-group ">
                    <label for="address" class="col-sm-2 control-label">Membership</label>

                    <div class="col-sm-8">
                        <select name="isMember" id="isMember" class="form-control">
                            <option></option>
                            <option value="0">Fee Not Paid</option>
                            <option value="1">Fee Paid</option>
                            <option value="2"> Fee Waiver</option>
                        </select>
                        <label id="error_isMember" class="error col-sm-12">

                        </label>
                    </div>

                </div>
                <div class="form-group ">
                    <label for="address" class="col-sm-2 control-label">Aadhar No</label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="aadhar_no" name="aadhar_no"
                               placeholder="Input Aadhar No">
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="box box-info">
            <div class="container-fluid" style="padding-top: 10px;">
                <div class="form-group">
                    <label class="col-sm-2 control-label"><img src="{{ url('img/guardian.png') }}" width="25px"></label>
                    <div class="col-sm-8">
                        <h4> Guardian's Information </h4>
                    </div>
                </div>

                <div class="form-group  ">
                    <label for="address" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" placeholder="Input Name" name="guardian_name">
                    </div>

                </div>
                <div class="form-group  ">
                    <label for="address" class="col-sm-2 control-label">Relation</label>

                    <div class="col-sm-8">
                        <select type="text" class="form-control relation" name="guardian_relation">
                            <option></option>
                            @foreach(config('app.relation') as $k => $value)

                                <option value="{{ $k }}"> {{ $value }} </option>

                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="form-group  ">
                    <label for="address" class="col-sm-2 control-label">Profession</label>

                    <div class="col-sm-8">
                        <select class="form-control professions" id="profession" name="guardian_profession">
                            <option></option>
                            @foreach($professions as $profession)
                                <option value="{{ $profession->id }}"> {{ $profession->name }} </option>
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="form-group  ">
                    <label for="address" class="col-sm-2 control-label">Mobile</label>

                    <div class="col-sm-8">
                        <input type="text" maxlength="10" class="form-control" placeholder="Input Mobile"
                               name="guardian_mobile">
                    </div>


                </div>
                <div class="form-group  ">
                    <label for="address" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" placeholder="Input Email" name="guardian_email">
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="box box-info">
            <div class="container-fluid" style="padding-top: 10px;">

                <input type="hidden" name="is_inquiry" value="0">

                <div class="form-group">
                    <label class="col-sm-2 control-label"><img src="{{ url('img/activity.png') }}" width="25px"></label>
                    <div class="col-sm-8">
                        <h4> Enroll To Activity </h4>
                    </div>
                </div>

                <span id="course_date_div">

					<div class="form-group">
						<label for="address" class="col-sm-2 control-label">Activity</label>
						<div class="col-sm-8">
							<select placeholder="Select Activity" class="form-control course"
                                    onchange="courseChange(this)" id="1" name="courses[]">
									<option></option>
                                @foreach($course as $value)
                                    <option value="{{ $value->id }}">
										{{ $value->activity->name }}{{  showActivityBranchName($value) }}
									</option>
                                @endforeach
							</select>
						</div>
                        <button class="btn btn-default" data-modal='add-activity-modal' onclick='openModal(this, event)'
                                type="button"><i class="fa fa-plus"></i></button>
					</div>

					<div class="form-group">
						<label for="address" class="col-sm-2 control-label">Batch</label>
						<div class="col-sm-8">
							<select placeholder="Select Batch" class="form-control batch" id="batch-1" name="batches[]">
								<option></option>
							</select>
						</div>
                        <button class="btn btn-default" data-modal='add-batch-modal' onclick='openModal(this, event)'
                                type="button"><i class="fa fa-plus"></i></button>
					</div>

					<div class="form-group">
						<label for="address" class="col-sm-2 control-label">Date</label>
						<div class="col-sm-5" style="">
							<input type="text" name="date[]" placeholder="Start Month" class="date form-control">
						</div>
						<button class="btn btn-primary add-box" type="button"><i class="fa fa-plus"></i></button>
					</div>
				</span>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="error" id="error_course">
                        </label>
                    </div>
                    <div class="col-sm-12">
                        <label class="error" id="error_batch">
                        </label>
                    </div>
                    <div class="col-sm-12">
                        <label class="error" id="error_date">
                        </label>
                    </div>
                </div>

                <div class="form-group  pull-right">
                    <div class="col-sm-12 pull-right">
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>


<div id="template" style="display: none;">

    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">Activity</label>
        <div class="col-sm-8">
            <select class="form-control course" onchange="courseChange(this)" id="" name="courses[]">
                <option value="">Select Activity</option>
                @foreach($course as $key => $value)

                    <option value="{{ $value->id }}">
                        {{ $value->activity->name }}{{showActivityBranchName($value)}}
                    </option>

                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">Batch</label>
        <div class="col-sm-8">
            <select class="form-control batch" id="" name="batches[]">
                <option value="">Batch</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">Starts from</label>
        <div class="col-sm-5" style="">
            <input type="text" name="date[]" placeholder="Start Month" class="date form-control">
        </div>
        <button class="btn btn-danger remove-box" type="button"><i class="fa fa-minus"></i></button>
    </div>
</div>

