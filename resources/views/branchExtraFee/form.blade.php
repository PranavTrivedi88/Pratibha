<div class="box-body">

    <div class="fields-group">
        <div class="form-group">
            <label class="col-sm-3 control-label">Extra Fee:</label>
            <div class="col-sm-8">
                <select class="form-control" id="extra_fee" name="extra_fee">
                    <option value="">Select</option>
                    @foreach(App\ExtraFee::all() as $fee)
                        <option value="{{$fee->id}}" {{ (isset($branchExtraFee->extra_fee_id) && $branchExtraFee->extra_fee_id == $fee->id) ? 'selected' : '' }} >{{$fee->name}}</option>
                    @endforeach
                </select>
                <span class="help-block extra_fee" style="color: red" id="errorMessage"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Activity:</label>
            <div class="col-sm-8">
                <select class="form-control" id="activity_name" name="activity_name">
                    <option value="">Select</option>
                    @if(isset($activities))
                        @foreach($activities as $activity)
                            <option value="{{$activity->id}}" {{ (isset($branchExtraFee->activity_id) && $branchExtraFee->activity_id == $activity->id) ? 'selected' : '' }} >{{$activity->name}}</option>
                        @endforeach
                    @endif
                </select>
                <span class="help-block activity_name" style="color: red" id="errorMessage"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Amount:</label>
            <div class="col-sm-8">
                <input type="text" class="form-control " name="amount" placeholder="Input Amount"
                       value="{{ $branchExtraFee->amount or '' }}">
                <span class="help-block amount" style="color: red"></span>
            </div>
        </div>
        @if(Admin::user()->isAdministrator() || isRole('super-administrator'))
            <div class="form-group">
                <label class="col-sm-3 control-label">Branch:</label>
                <div class="col-sm-8">
                    <select class="form-control" id="branch_id" name="branch_id">
                            <option value="">Select</option>
                            @foreach(\App\Branch::all() as $branch)
                                <option value="{{$branch->id}}" {{ (isset($branchExtraFee->branch_id) && $branchExtraFee->branch_id == $branch->id) ? 'selected' : '' }} >{{$branch->name}}</option>
                            @endforeach
                    </select>
                    <span class="help-block branch_id" style="color: red" id="errorMessage"></span>
                </div>
            </div>
        @else
            <input type="hidden" name="branch_id" value="{{ (isset($branch->id))?  $branch->id : $branchExtraFee->branch_id}}">
        @endif
    </div>

</div>

<div class="box-footer">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="btn-group pull-right">
            <button type="submit" class="btn btn-info pull-right"
                    data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit
            </button>
        </div>
        <div class="btn-group pull-left">
            <button type="reset" class="btn btn-warning">Reset</button>
        </div>
    </div>
</div>