<style>
    .image-bottom-margin {
        margin-bottom: 10px;
    }

    .delete-button {
        position: absolute;
        top: -7px;
        right: 11px;
        background: white;
        border-radius: 50%;
        border: none;
        box-shadow: 0px 0px 7px #000;
        cursor: pointer;
        display: none;
    }

    .image {
        position: relative;
    }

    .image img {
        width: 100%;
        vertical-align: top;
    }

    .image:after {
        content: '\A';
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        background: rgba(0, 0, 0, 0.6);
        opacity: 0;
        transition: all 0.5s;
        -webkit-transition: all 0.5s;
    }

    .image:hover:after {
        opacity: 1;
    }
    .dropzone{
        border: 2px dashed #0087F7;
        border-radius: 5px;
        background: white;
    }
    .gallery{
        border-radius: 10px;
        background: white;
        -moz-box-shadow: 0 0 5px #888;
        -webkit-box-shadow: 0 0 5px#888;
        box-shadow: 0 0 5px #888;
        padding-top: 20px;
        padding-bottom: 20px;
        margin-left: 0px;
        margin-right: 0px;
        min-height: 400px;
    }

</style>

<h3 class="my-4 text-center text-lg-left">Drop files inside box to upload</h3>
<form action="media"  class="dropzone" id="fileUpload">
    {{ csrf_field() }}
    <input type="hidden" name="eventId" value="{{ request("event") }}" id="eventId">
    <input type="hidden"  value="{{ csrf_token() }}" id="token">
</form>
<div id="gallery">

</div>
<script type="application/javascript">

    setTimeout(function () {
        //load First Time Gallery
        gallery();

        Dropzone.autoDiscover = false;

        myDropzone = new Dropzone("#fileUpload");
        myDropzone.on("queuecomplete", function (file) {
            gallery();
        });

    },500);


    function gallery() {
        var eventId = $('#eventId').val();
        $.ajax(
            {
                url: "media/gallery/" + eventId,
                type: 'GET',
                data: {},
                success: function (response) {
                    $('#gallery').empty();
                    $('#gallery').append(response);
                    gallaryInti()
                }
            });
    }

    function gallaryInti() {
        //Gallery
        var sd = $("#lightgallery").lightGallery({
            selector: '.image'
        });
        //Delete Button hover
        $(".image, .delete-button").hover(function () {
                $(this).parent().find('.delete-button').show();
            }, function () {
                $(this).parent().find('.delete-button').hide();
            }
        );

        // Initialize
        var bLazy = new Blazy();
    }

    $(document).on('click', '.delete-button', function () {

        var token = $('#token').val();
        $.ajax(
            {
                url: "media/" + this.id,
                type: 'POST',
                data: {
                    "id": this.id,
                    "_method": 'DELETE',
                    "_token": token,
                },
                success: function () {
                    console.log("Image Deleted");
                    gallery();
                }
            });
    });

</script>