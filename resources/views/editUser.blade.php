@extends('adminlte::page')

@section('title', 'Manage User')

@section('content')

    <div class="register-box">
        <div class="register-logo">
            <a href="/">Register Team</a>
        </div>

        <div class="register-box-body">
            <p class="login-box-msg">Edit membership details</p>

            <form action="{{url('/users/'.$user->id)}}" method="post">

                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="form-group has-feedback">
                    <input type="text" name="name" class="form-control" value="{{ $user->name }}"
                           placeholder="Full name">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    @if ($errors->has('name'))
                        <span class="help-block" style="color: #dd4b39">
                            {{ $errors->first('name') }}
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="email" name="email" class="form-control" value="{{ $user->email }}"
                           placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            {{ $errors->first('email') }}
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">
                    <input type="test" name="username" class="form-control" value="{{ $user->username }}"
                           placeholder="Username">
                    <span class="glyphicon glyphicons-nameplate form-control-feedback"></span>
                    @if ($errors->has('username'))
                        <span class="help-block">
                            {{ $errors->first('username') }}
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <select name="branch_id" class="form-control">
                        <option value="0">Select Branch</option>
                        @foreach($branches as $branch)
                            <option value="{{$branch->id}}" @if($branch->id == $user->branch_id) {{'selected'}} @endif>{{$branch->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('branch_id'))
                        <span class="help-block" style="color: #dd4b39">
                            {{ $errors->first('branch_id') }}
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <select name="role_id" class="form-control">
                        <option value="0">Select Role</option>
                        @foreach($roles as $role)
                            <option value="{{$role->id}}" @if($role->id == $user->role_id) {{'selected'}} @endif>{{$role->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('role_id'))
                        <span class="help-block" style="color: #dd4b39">
                            {{ $errors->first('role_id') }}
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <select name="designation_id" class="form-control">
                        <option value="0">Select Designation</option>
                        @foreach($designations as $designation)
                            <option value="{{$designation->id}}" @if($designation->id == $user->designation_id) {{'selected'}} @endif>{{$designation->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('designation_id'))
                        <span class="help-block" style="color: #dd4b39">
                            {{ $errors->first('designation_id') }}
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <label class="radio-inline ">
                        <input type="radio" name="status" value="1"  @if($user->status == 1) checked @endif> Active
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="status" value="2"  @if($user->status == 2) checked @endif> Inactive
                    </label>
                </div>
                {{--<div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input type="password" name="password" class="form-control"
                           placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <input type="password" name="password_confirmation" class="form-control"
                           placeholder="Retype Password">
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>--}}
                <div align="right">
                    <button type="submit" class="btn btn-primary btn-flat">Update</button>
                    <a href="{{url('/users')}}" class="btn btn-primary btn-flat">Cancel</a>
                </div>
            </form>
        </div>
        <!-- /.form-box -->
    </div><!-- /.register-box -->
@stop

@section('adminlte_js')
    @yield('js')
@stop

