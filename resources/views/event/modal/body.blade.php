<table class="table no-header table-bordered">
    <tbody>
    <tr>
        <td width="25%">
            <b>Event Title</b>
        </td>
        <td>
            {{$event->title or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Organizer</b>
        </td>
        <td>
            {{$event->organizer or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Start Date & Time</b>
        </td>
        <td>
            {{$event->start_date or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>End Date & Time</b>
        </td>
        <td>
            {{$event->end_date or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Location</b>
        </td>
        <td>
            {{$event->location or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Description</b>
        </td>
        <td>
            {!! $event->description !!}
        </td>
    </tr>
    <tr>
        <td>
            <b>Youtube Video</b>
        </td>
        <td>
            @if(isset($event->gallery))
                @foreach($event->gallery as $media)
                    @if($media->type == 'video')
                        <a href="{{ $media->path }}" target="_blank" style="color: #0000EE;">{{ $media->path }}</a> <br/>
                    @endif
                @endforeach
            @else
                -
            @endif
        </td>
    </tr>
    <tr>
        <td>
            <b>Total Images</b>
        </td>
        <td>
            @if(isset($event->gallery))
                @php $count = $event->gallery()->where('type','image')->count() @endphp
                @if($count > 0)
                    <button data-href="/media?event={{ $event->id }}" class="btn btn-info" id="goToGallery"><span class="badge">{{ $count }}</span> Open Gallery</button>
                @else
                    -
                @endif
            @else
                -
            @endif
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <button data-href="/events/{{ $event->id }}/edit" class="btn btn-primary pull-right" id="goToEdit">Edit Details</button>
        </td>
    </tr>
    </tbody>
</table>
