<div class="modal fade" id="add-parent-modal" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" accept-charset="UTF-8" id="add-parent-form" data-url="/parents/add-parent" onsubmit="addData(this.id, event)"
                  class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Add Parent</h4>
                </div>
                <div class="modal-body">

                    {{ csrf_field()}}

                    <div class="fields-group">
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><img src="{{asset("img/male.png")}}" width="25px"></label>
                            <div class="col-sm-8">
                                <h4>Father's Information</h4>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <input type="text" id="name" name="name" value="" class="form-control name" placeholder="Input Name">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="mobile" class="col-sm-2 control-label">Mobile</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                    <input style="width: 150px" type="text" id="mobile" name="mobile" value="" maxlength="10" class="form-control mobile" placeholder="Input Mobile" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <input type="text" id="email" name="email" value="" class="form-control email" placeholder="Input Email">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parent_profession" class="col-sm-2 control-label">Profession</label>
                            <div class="col-sm-8">
                                <select class="form-control profession" id="parent_profession" name="parent[profession]">
                                    <option value=""></option>
                                    @foreach($professions as $profession)
                                        <option value="{{ $profession->id }}"> {{ $profession->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                            <a href="#" class="btn btn-default add-button" data-modal="add-profession-modal" onclick="openModal(this, event)"><i class="fa fa-plus"></i></a></div>
                        <div class="form-group">
                            <label for="parent_father_blood_group" class="col-sm-2 control-label">Blood Group</label>
                            <div class="col-sm-8">
                                <select class="form-control blood-group" name="parent[father_blood_group]">
                                    <option value=""></option>
                                    @foreach(config('app.blood_group') as $k => $group)
                                        <option value="{{ $k }}"> {{ $group }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parent_aadhar_no_father" class="col-sm-2 control-label">Aadhar No</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <input type="text" id="parent_aadhar_no_father" name="parent[aadhar_no_father]" value="" class="form-control parent_aadhar_no_father_" placeholder="Input Aadhar No">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><img src="{{asset("img/user.png")}}" width="25px"></label>
                            <div class="col-sm-8">
                                <h4> Mother's Information </h4>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parent_mother_name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <input type="text" id="parent_mother_name" name="parent[mother_name]" value="" class="form-control parent_mother_name_" placeholder="Input Name">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parent_mother_mobile" class="col-sm-2 control-label">Mobile</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                    <input style="width: 150px" type="text" id="parent_mother_mobile" name="parent[mother_mobile]" maxlength="10" value="" class="form-control mobile" placeholder="Input Mobile">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parent_mother_email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <input type="text" id="parent_mother_email" name="parent[mother_email]" value="" class="form-control parent_mother_email_" placeholder="Input Email">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parent_mother_profession" class="col-sm-2 control-label">Profession</label>
                            <div class="col-sm-8">
                                <select class="form-control profession" id="parent_mother_profession" name="parent[mother_profession]">
                                    <option value=""></option>
                                    @foreach($professions as $profession)
                                        <option value="{{ $profession->id }}"> {{ $profession->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                            <a href="#" class="btn btn-default add-button" data-modal="add-profession-modal" onclick="openModal(this, event)"><i class="fa fa-plus"></i></a></div>
                        <div class="form-group">
                            <label for="parent_mother_blood_group" class="col-sm-2 control-label">Blood Group</label>
                            <div class="col-sm-8">
                                <select class="form-control blood-group" name="parent[mother_blood_group]">
                                    <option value=""></option>
                                    @foreach(config('app.blood_group') as $k => $group)
                                        <option value="{{ $k }}"> {{ $group }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parent_aadhar_no_mother" class="col-sm-2 control-label">Aadhar No</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <input type="text" id="parent_aadhar_no_mother" name="parent[aadhar_no_mother]" value="" class="form-control parent_aadhar_no_mother_" placeholder="Input Aadhar No">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-8">
                                <hr>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><img src="{{asset("img/location.png")}}" width="25px"></label>
                            <div class="col-sm-8">
                                <h4> Location </h4>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="city_id" class="col-sm-2 control-label">City</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="city_id">
                                <select class="form-control city" name="city_id">
                                    <option value=""></option>
                                    @foreach($cities as $k => $val)
                                        <option value="{{ $val->id }}"> {{ $val->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                            <a href="#" class="btn btn-default add-button" data-modal="add-city-modal" onclick="openModal(this, event)"><i class="fa fa-plus"></i></a></div>
                        <div class="form-group">
                            <label for="address" class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-8">
                                <textarea name="address" id="address" class="form-control" rows="5" placeholder="Input Address"></textarea>
                            </div>
                        </div>
                        @if(isset($branch))
                            <input type="hidden" name="branch_id" value="{{ $branch->id }}">
                        @else
                            <div class="form-group">
                                <label for="branch_id" class="col-sm-2 control-label">Branch</label>
                                <div class="col-sm-8">
                                    <select class="form-control branch_id" name="branch_id" required>
                                        <option value="">Select Branch</option>
                                        @foreach(\App\Branch::all() as $k => $val)
                                            <option value="{{ $val->id }}"> {{ $val->name }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-8">
                        <div class="btn-group pull-right">
                            <button type="submit" class="btn btn-info pull-right"
                                    data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit
                            </button>
                        </div>
                        <div class="btn-group pull-left">
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>