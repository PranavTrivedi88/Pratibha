<div class="modal fade courses" id="add-activity-modal" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <form data-url="{{url('/courses')}}" method="POST" onsubmit="addData(this.id, event)"
                  class="form-horizontal"
                  id="add-activity-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Add Activity</h4>
                </div>

                {{ csrf_field() }}

                <div class="box-body">

                    <div class="fields-group">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-9">
                                <select name="name" style="width: 65%">
                                    @foreach(\App\Activity::all() as $activity)
                                        <option value="{{ $activity->id }}">{{ $activity->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-8 error" id="name"></div>
                            </div>
                        </div>
                        @if(isRole('super-administrator') || isRole('administrator'))
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Branch</label>
                                <div class="col-sm-9">
                                    <select name="branchId" style="width: 65%">
                                        @foreach(\App\Branch::all() as $branch)
                                            <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Fee Types</label>
                            <div class="col-sm-9">
                                @foreach($feeTypes as $key => $type)
                                    <div class="form-group">
                                        <div class="col-md-12" style="padding-top: 5px;">
                                            <div class="col-sm-1">
                                                <input type="checkbox" class="checkbox-minimal-blue"
                                                       name="fees[{{$key}}][check]">
                                            </div>
                                            <div class="col-sm-5">
                                                 <span style="padding-left: 15px;">
                                                    {{ $type['name'] }}
                                                </span>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" placeholder=" {{ $type['name'] }} Fees"
                                                       class="form-control"
                                                       name="fees[{{$key}}][fees]" id="fees_{{$key}}">
                                                <input type="hidden" name="fees[{{$key}}][id]"
                                                       value="{{$type['id']}}">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-4"></div>
                                            <div class="col-sm-8 error" id="fees_{{$key}}_error"></div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                            @if(count($feeTypes) == 0)
                                <div class="col-sm-9">
                                    <h5> No Data In Fees Types</h5>
                                </div>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-2"></div>
                    <div class="col-md-9">
                        <div class="btn-group pull-right">
                            <button type="submit" class="btn btn-info pull-right"
                                    data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit
                            </button>
                        </div>
                        <div class="btn-group pull-left">
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
