<table class="table no-header table-bordered">
    <caption><h4>School Details:</h4></caption>
    <tbody>
    <tr>
        <td width="20%">
            <b>School Name</b>
        </td>
        <td>
            {{$school->name or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Contact No.</b>
        </td>
        <td>
            {{$school->contact_no or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>City</b>
        </td>
        <td>
            {{$school->city->name or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Address</b>
        </td>
        <td>
            {{$school->address or "-"}}
        </td>
    </tr>
    </tbody>
</table>
<table class="table no-header table-bordered">
    <caption><h4>Contact Person Details:</h4></caption>
    <thead>
    <th>Name</th>
    <th>Mobile</th>
    <th>Email</th>
    </thead>
    <tbody>
    @foreach($contactDetails as $key=>$value)
        <tr>
            <td>
                {{$value->name or "-"}}
            </td>
            <td>
                {{$value->mobile or "-"}}
            </td>
            <td>
                {{$value->email or "-"}}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>