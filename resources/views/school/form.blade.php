        <div class="box-body">

            <div class="fields-group">
                <div class="form-group">
                    <label class="col-sm-3 control-label">School Name:</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control " name="name" placeholder="Input School Name" value="{{ $school->name or '' }}" autofocus>
                        <span class="help-block name" style="color: red"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Contact No:</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="contact_no" placeholder="Input Contact Number" maxlength="10" value="{{ $school->contact_no or '' }}">
                        <span class="help-block contact_no" style="color: red;" id="errorMessage"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">City:</label>
                    <div class="col-sm-7">
                        <select class="form-control city_id" id="city" name="city_id">
                            @foreach(App\City::all() as $city)
                                <option value="{{$city->id}}" {{ (isset($school->city_id) && $school->city_id == $city->id) ? 'selected' : '' }} >{{$city->name}}</option>
                            @endforeach
                        </select>
                        <span class="help-block city_id" style="color: red" id="errorMessage"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Address:</label>
                    <div class="col-sm-8">
                        <textarea name="address" placeholder="Input Address" class="form-control" rows="4"> {{ (isset($school->address)) ? $school->address : '' }} </textarea>
                        <span class="help-block address" style="color: red" id="errorMessage"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">School Status:</label>
                    <div class="col-sm-8">
                        <select class="form-control" id="school_status" name="school_status">
                            @foreach(config('app.school_status') as $status)

                                @if(isset($school->school_status))

                                    <option value="{{ $status }}" @if($status == $school->school_status) selected @endif>{{ $status }}</option>
                                @else
                                    <option value="{{ $status }}" @if($status == "Green") selected @endif >{{ $status }}</option>
                                @endif
                            @endforeach
                        </select>
                        <span class="help-block school_status" style="color: red" id="errorMessage"></span>
                    </div>
                </div>
                @if(isset($branch))
                    <input type="hidden" value="{{ $branch->id }}" name="branch_name">
                @else
                <div class="form-group">
                    <label class="col-sm-3 control-label">Branch Name:</label>
                    <div class="col-sm-8">
                        <select class="form-control" id="branch_name" name="branch_name">
                            <option value="">Select Branch</option>
                            @foreach(App\Branch::all() as $branch)
                                <option value="{{$branch->id}}" {{ (isset($school->branch_id) && $school->branch_id == $branch->id) ? 'selected' : '' }} >{{$branch->name}}</option>
                            @endforeach
                        </select>
                        <span class="help-block branch_name" style="color: red" id="errorMessage"></span>
                    </div>
                </div>
            @endif
            </div>
            <div class="col-md-12">
                <table id="dataTable" class="form table table-bordered table-responsive table-striped no-header compact">
                    <thead>
                    <tr>
                        <th><span style="font-size: 15px">Contact Person Details</span></th>
                    </tr>
                    </thead>
                    <tbody id="contact_person_container">
                        @if(isset($schoolDetails))
                            @php $i=1; @endphp
                            @foreach($schoolDetails as $key=>$detail)
                            <tr class="remove-row{{$i}} contact-person-row">

                                  <input type="hidden" value="{{$detail->id}}"
                                       name="contactDetail[{{$key + 1 }}][id]" />
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label for="">Name</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" name="contactDetail[{{$key + 1 }}][contactname]"
                                                   class="form-control input-sm" id="name"  value="{{$detail->name}}">
                                            <span class="help-block contactname-0" style="color: red" id="contactname{{$i}}"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label for="">Email</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" name="contactDetail[{{$key + 1}}][email]" id="email"
                                                   class="form-control input-sm" value="{{$detail->email}}">
                                            <span class="help-block email" style="color: red" id="email{{$i}}"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label for="">Mobile</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <input type="text" name="contactDetail[{{$key + 1}}][mobile]" class="form-control input-sm"
                                                   id="mobile" maxlength="10" value="{{$detail->mobile}}">
                                            <span class="help-block mobile" style="color: red" id="mobile{{$i}}"></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <button value="Remove" class="btn btn-danger btn-sm pull-right" style="padding: 4px 8px" type="button" onclick="deleteRow('remove-row{{$i}}')">
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <button class="btn btn-default btn-sm pull-right" style="padding: 4px 8px" type="button" onclick="addRow('dataTable')">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @php $i++ ;@endphp
                            @endforeach

                            <input type="hidden" name="contactPersonCount" id="contactPersonCount" value="{{ $i-1}}">

                        @else
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label for="">Name</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" name="contactDetail[0][contactname]"
                                                   class="form-control input-sm" id="name">
                                            <span class="help-block contactname-0" style="color: red" id="contactname0"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label for="">Email</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" name="contactDetail[0][email]" id="email"
                                                   class="form-control input-sm">
                                            <span class="help-block email" style="color: red" id="email0"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label for="">Mobile</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <input type="text" name="contactDetail[0][mobile]" class="form-control input-sm"
                                                   id="mobile" maxlength="10">
                                            <span class="help-block mobile" style="color: red" id="mobile0"></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <button class="btn btn-default btn-sm pull-right" style="padding: 4px 8px" type="button" onclick="addRow('dataTable')">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>

        </div>

        <div class="box-footer">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="btn-group pull-right">
                    <button type="submit" class="btn btn-info pull-right" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit</button>
                </div>
                <div class="btn-group pull-left">
                    <button type="reset" class="btn btn-warning">Reset</button>
                </div>
            </div>
        </div>