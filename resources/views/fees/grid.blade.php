<div class="box" id="data-container">
    <div class="box-header with-border">
        <h3 style="margin: -5px 8px 5px;">Fee Information</h3>
    </div>

    <div class="container-fluid">
        <div id="box-body">
            <table class="table table-condensed table-small">
                <thead>
                <tr>
                    <td>Name</td>
                    <td>{{$user->name or "-"}}</td>
                </tr>
                <tr>
                    <td>Total Fees</td>
                    <td id="totalAmount" data-fee="25000"><b>{{$totalFees or "-"}}</b></td>
                </tr>
                <tr>
                    <td>Fee Paid</td>
                    <td id="paidAmount" data-fee="5000"><b>{{$totalPaidFees or "-"}}</b></td>
                </tr>
                <tr>
                    <td>Total Discount</td>
                    <td id="paidAmount" data-fee="5000"><b>{{$totalDiscount or "-"}}</b></td>
                </tr>
                <tr>
                    <td>Total Advance/Due</td>
                    <td id="paidAmount" data-fee="5000"><b>{{$user->advance_amount or "-"}}</b></td>
                </tr>
                <tr>
                    <td>Fee Pending</td>
                    <td id="pendingAmount" data-fee="20000"><b>{{$totalPendingFees or "-"}}</b></td>
                </tr>
                </thead>
            </table>
            <hr>
            <h5 style="margin: 5px 8px 5px;">Fee History</h5>
            <table class="table table-small no-header table-bordered table-striped">
                <thead>
                <tr>
                    <th width="auto"></th>
                    <th width="25%">Amount</th>
                    <th width="30%">Date</th>
                    <th width="35%">Payment Mode</th>
                </tr>
                </thead>
                <tbody>
                @php $i=0; $fees = $user->fees; $count = count($fees); @endphp
                @if($count > 0)
                    @foreach($fees as $fee)
                        <tr>
                            <td><i class="fa fa-plus btn btn-default" onclick="hideShowFeeDetails(this.id)" id="detailButton-{{$i}}" style="font-size: 12px; padding: 5px 7px"></i></td>
                            <td>
                                @if($fee->lump_sum_amount > 0)
                                    {{$fee->lump_sum_amount}}
                                @else
                                    {{$fee->feeDetails->sum("paid_amount")}}
                                @endif
                            </td>
                            <td>{{date("d-m-Y", strtotime($fee->payment_date))}}</td>
                            <td>
                                {{config("app.payment_mode.".$fee->payment_mode)}}
                                @if($fee->payment_mode == 1)
                                    <br>{{$fee->cheque_number}}
                                    <br>{{$fee->bank->name}}
                                @endif
                            </td>
                        </tr>
                        <tr class="detailButton-{{$i}}" style="display: none">
                            <td class="active"></td>
                            <th colspan="2" class="info">Course</th>
                            <th class="info">Amount</th>
                        </tr>
                        @foreach($fee->feeDetails as $feeDetail)
                            <tr class="detailButton-{{$i}}" style="display: none">
                                <td class="active"></td>
                                <td colspan="2" class="warning">{{$feeDetail->course->name}}</td>
                                <td class="warning">{{$feeDetail->paid_amount}}</td>
                            </tr>
                        @endforeach
                        @php $i++; @endphp
                    @endforeach
                @else
                    <tr>
                        <td colspan="4" style="text-align: center">No data available</td>
                    </tr>
                @endif

                </tbody>
            </table>

            <div id="applied_fees" style="display: none" data-is-fixed="0">
            </div>

        </div>
    </div>

</div>