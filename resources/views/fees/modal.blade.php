<div class="modal fade" id="fee-history-modal" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Fee History</h4>
            </div>
            <div class="modal-body" id="fee-history-body">

            </div>
        </div>
    </div>
</div>

