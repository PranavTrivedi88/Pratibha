<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Create</h3>

        <div class="box-tools">
            <div class="btn-group pull-right" style="margin-right: 10px">
                <a class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
            </div>
            <div class="btn-group pull-right" style="margin-right: 10px">
                <a href="" data-id="{{$user->id}}" class="btn btn-sm bg-info open-fee-history-modal">View Fee
                    History</a>
            </div>
        </div>
    </div>

    <form action="{{url('/fees')}}" method="POST" class="form-horizontal" id="createBranch">

        {{ csrf_field() }}

        <input type="hidden" name="user_id" value="{{$user->id}}">

        <div class="box-body">

            <div class="fields-group">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Student Name</label>
                    <div class="col-sm-9">
                        <span class="form-control">{{$user->name or "-"}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Payment Date*</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input style="width: 100%;" type="text" id="payment_date" name="payment_date"
                                   value="{{$currentDate or ""}}"
                                   class="form-control payment_date" placeholder="Input Payment Date">
                        </div>
                        <span class="payment_date" style="color: red;" id="errorMessage"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Payment*</label>
                    <div class="col-sm-9">
                        <table class="table no-header table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="33%"><input type="checkbox" class="minimal all" id="selectAllCourses"
                                                       @if(count($pendingFees) == 0) disabled="disabled" @endif>&nbsp;&nbsp;Activity
                                    Name
                                </th>
                                <th width="33%">Discount</th>
                                <th style="text-align: center;"><i class="fa fa-info-circle" data-toggle="tooltip"
                                                                   title="Select to give perement discount"
                                                                   style="font-size:20px;"></i></th>
                                <th width="33%">Paid Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0; @endphp
                            @foreach($pendingFees as $pendingFee)
                                @php
                                    $discount = $pendingFee->getPermanentDiscount($pendingFee->user_id, $pendingFee->course_id);
                                    $pendingAmount = ($pendingFee->fees != 0) ? $pendingFee->pendingAmountByCourse($pendingFee->user_id, $pendingFee->course_id) : 0;

                                @endphp
                                <tr>
                                    <td>
                                        <label style="font-weight: normal">
                                            <input type="checkbox"
                                                   class="minimal  {{($pendingFee->fees_type != 0)  ? 'individual' : 'individual_dropdown'}}"
                                                   value="{{$pendingFee->course->id}}"
                                                   data-fee="{{$pendingFee->pendingAmountByCourse($pendingFee->user_id, $pendingFee->course_id)}}"
                                                   data-originalFee="{{$pendingFee->course->fees}}"
                                                   name="course[{{$i}}][course_id]">&nbsp;&nbsp;{{$pendingFee->course->activity->name}}
                                            <input type="hidden" name="course[{{$i}}][pending_amount]"
                                                   class="course_pending_amount" value="{{ $pendingAmount }}">
                                            &nbsp;&nbsp;
                                        </label>
                                        <br>
                                        @php $originalFees = ($pendingFee->fees_type == 0) ? 0 : $pendingFee->course->courseFeeType->where('fee_type_id',$pendingFee->fees_type)->first()->fees; @endphp
                                        @if($pendingFee->fees_type == 0)

                                            <select class="form-control fee_type col-md-12" id=""
                                                    name="course[{{$i}}][fee_type]"
                                                    disabled="disabled" data-fee=""
                                                    data-month="{{ $pendingFee->month }}"
                                                    data-year="{{ $pendingFee->year }}"
                                                    data-originalFee="">
                                                <option></option>
                                                @foreach($pendingFee->course->courseFeeType as $types)
                                                    <option value="{{ $types->fee_type_id }}"
                                                            data-fee="{{ $types->fees }}"> {{ config('app.fee_type')[$types->fee_type_id]['name'] }}</option>
                                                @endforeach
                                            </select>
                                            <div class="col-md-12" style="padding-left: 0px;">
                                                <input type="text" class="form-control session_count" style="display: none;"
                                                       id="" placeholder="Session Count" value="1">
                                            </div>
                                            <span style="font-size: 11px; margin-top: 5px; margin-right: 10px">Activity Fees:
                                            <span style="color: #3c8dbc" class="activity_fee"> 0 /-</span></span>
                                            <span style="font-size: 11px; margin-top: 5px;">Pending Fees:
                                                <span style="color: #3c8dbc" class="pending_fee" data-value="">
                                                    {{ 0 }} /-
                                                </span>
                                            </span>

                                        @else
                                            <label class="label bg-blue"> {{ config('app.fee_type')[$pendingFee->fees_type]['name'] }} </label>
                                            <br><br>

                                            @if($pendingFee->fees_type == 3)
                                                <div class="col-md-12" style="padding-left: 0px;">
                                                    <input type="text" class="form-control session_count" disabled id="session_count"
                                                           placeholder="Session Count" value="1" data-fee-value="{{ $originalFees }}">
                                                </div>
                                                <br>
                                            @endif
                                            <span style="font-size: 11px; margin-top: 5px; margin-right: 10px">Activity Fees:
                                                <span style="color: #3c8dbc">{{ getCourseFees($pendingFee->user_id , $pendingFee->course_id)}}
                                                    /-</span>
                                            </span>
                                            <span style="font-size: 11px; margin-top: 5px;">Pending Fees:
                                                <span style="color: #3c8dbc" class="pending_fee" data-value="{{ $pendingAmount }}">{{ $pendingAmount }}
                                                    /-
                                                </span>
                                            </span>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-rupee"></i></span>
                                            <input type="text" name="course[{{$i}}][discount]"
                                                   onkeypress="isNumberKey(this, event)"
                                                   data-fee="{{ $pendingAmount }}"
                                                   data-originalfee="{{ $originalFees }}"
                                                   disabled="disabled"
                                                   value="{{$discount != 0 ? $discount : ""}}"
                                                   class="form-control discount" placeholder="ex. 10% or 80">

                                        </div>
                                        <span style="font-size: 11px; margin-top: 5px;">Discount(%): <span
                                                    style="color: #3c8dbc" class="discount_per">{{(!$pendingFee->fees) ? 0 : (float)number_format(($discount * 100) / $pendingFee->fees, 2, ".", "") + 0 }}
                                                %</span></span>
                                        <span style="font-size: 11px; margin-top: 5px;" class="pull-right">Given Discount : <span
                                                    style="color: #3c8dbc" class="discount_per"> {{ ($pendingFee->total_discount) ? $pendingFee->total_discount : 0 }} </span>/-</span>
                                    </td>
                                    <td style="padding-top: 13px"><input type="checkbox" class="minimal discount-cbox"
                                                                         name="course[{{$i}}][per_discount]"
                                                                         @if($discount) checked @endif></td>
                                    <td>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-rupee"></i></span>
                                            <input type="number" id="discount" name="course[{{$i}}][paid_amount]"
                                                   disabled="disabled" class="form-control paid_amount"
                                                   placeholder="Input Paid Amount">
                                        </div>
                                        <span class="help-block paid_amount_error" style="color: red;"></span>
                                    </td>
                                </tr>
                                <?php $extraFees = App\PendingExtraFees::where('user_id', $pendingFee->user_id)->where('course_id', $pendingFee->course_id)->get(); ?>
                                @if($extraFees->count() > 0)
                                    <tr class="extra-tr">
                                        <td colspan="4">
                                            @foreach($extraFees as $key => $extra)
                                                <table class="table no-header table-bordered table-striped table-responsive "
                                                       style="margin: 0px;">
                                                    <tr class="info">
                                                        <td width="35%">
                                                            <label style="font-weight: normal">
                                                                <input type="checkbox"
                                                                       disabled
                                                                       class="minimal individual extra-individual"
                                                                       value="{{$extra->extra_fee_type_id}}"
                                                                       data-fee="{{$extra->fees}}"
                                                                       data-originalFee="{{$extra->fees}}"
                                                                       name="course[{{$i}}][extra][{{$key}}][id]">&nbsp;&nbsp;
                                                                {{$extra->extraFee->name}}
                                                                &nbsp;&nbsp;
                                                            </label>
                                                            <br>


                                                            <span style="font-size: 11px; margin-top: 5px; margin-right: 10px">
                                                                Activity Fees:
                                                                <span style="color: #3c8dbc">
                                                                    {{ $extra->fees }} /-
                                                                </span>
                                                            </span>
                                                            <span style="font-size: 11px; margin-top: 5px;">Pending Fees:
                                                                <span style="color: #3c8dbc">
                                                                    {{ $extra->fees }} /-
                                                                </span>
                                                            </span>
                                                        </td>

                                                        <td width="35%">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i
                                                                            class="fa fa-rupee"></i></span>
                                                                <input type="text"
                                                                       name="course[{{$i}}][extra][{{$key}}][discount]"
                                                                       onkeypress="isNumberKey(this, event)"
                                                                       data-fee="{{ $extra->fees }}"
                                                                       data-originalfee="{{ $extra->fees }}"
                                                                       disabled="disabled"
                                                                       value="{{$discount != 0 ? $discount : ""}}"
                                                                       class="form-control discount"
                                                                       placeholder="ex. 10% or 80">

                                                            </div>
                                                            <span style="font-size: 11px; margin-top: 5px;">Discount(%): <span
                                                                        style="color: #3c8dbc" class="discount_per">{{(!$extra->fees) ? 0 : (float)number_format(($discount * 100) / $extra->fees, 2, ".", "") + 0 }}
                                                                    %</span></span>
                                                            <span style="font-size: 11px; margin-top: 5px;"
                                                                  class="pull-right">Given Discount : <span
                                                                        style="color: #3c8dbc"
                                                                        class="discount_per">0</span>/-</span>
                                                        </td>

                                                        <td width="30%">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i
                                                                            class="fa fa-rupee"></i></span>
                                                                <input type="number" id="discount"
                                                                       name="course[{{$i}}][extra][{{$key}}][paid_amount]"
                                                                       disabled="disabled"
                                                                       class="form-control paid_amount"
                                                                       placeholder="Input Paid Amount">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            @endforeach
                                        </td>
                                    </tr>

                                @endif
                                @php $i++; @endphp
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>
                                    <label style="color: gray">Net Amount:</label>
                                    <span class="pull-right">/-</span>
                                    <label class="pull-right" style="color: black" id="net_amount">0</label>
                                    <input type="hidden" name="net_amount" id="net_amount_hidden">
                                </td>
                                <td>
                                    <label style="color: gray">Total Discount:</label>
                                    <span class="pull-right">/-</span>
                                    <label class="pull-right" style="color: black" id="total_discount">0</label>
                                </td>
                                <td></td>
                                <td>
                                    <label style="color: gray">Total Paid Amount:</label>
                                    <span class="pull-right">/-</span>
                                    <label class="pull-right" style="color: black" id="total_paid_amount">0</label>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Lump Sum Amount</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-rupee"></i></span>
                                    <input type="number" id="lump_sum_amount" disabled="disabled" name="lump_sum_amount"
                                           value=""
                                           class="form-control" placeholder="Input Lump Sum Amount">
                                </div>
                                <span class="paid-amount" style="color: red;" id="errorMessage"></span>
                            </div>
                            <div class="col-md-7">
                                <label class="col-sm-4 control-label">Advance / Due</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-rupee"></i></span>
                                    <input type="number" readonly="readonly" id="advance" name="advance" value="0"
                                           class="form-control">
                                </div>
                                <span class="paid-amount" style="color: red;" id="errorMessage"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Payment Mode*</label>
                    <div class="col-sm-9" style="margin-top: 5px">
                        <label style="font-weight: normal"><input type="radio" id="cash_radio" value="0" class="minimal"
                                                                  name="payment_mode">&nbsp;&nbsp;Cash&nbsp;&nbsp;</label>
                        <label style="font-weight: normal"><input type="radio" id="cheque_radio" value="1"
                                                                  class="minimal" name="payment_mode">&nbsp;&nbsp;Cheque</label>
                    </div>
                </div>
                <div id="cheque_details" style="display: none">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Cheque Number*</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-pencil"></i>
                                        </span>
                                        <input type="number" class="form-control " name="cheque_number"
                                               placeholder="Input Cheque Number">
                                    </div>
                                    <span class="cheque_number" style="color: red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Bank Name*</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="bank_name" name="bank_name">
                                @foreach($banks as $bank)
                                    <option value="{{$bank->id}}">{{$bank->name}}</option>
                                @endforeach
                            </select>
                            <span class="cheque_number" style="color: red"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Remarks</label>
                    <div class="col-sm-9">
                        <textarea name="remarks" placeholder="Input Remarks" class="form-control" rows="4"></textarea>
                    </div>
                </div>
                @if ($errors->any())
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Errors</label>
                        <div class="col-sm-9">
                            {{--{{dd($errors->first("course"))}}--}}
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>

        <div class="box-footer">
            <div class="col-md-2"></div>
            <div class="col-md-9">
                <div class="btn-group pull-right">
                    <button type="submit" class="btn btn-info pull-right"
                            data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit
                    </button>
                </div>
                <div class="btn-group pull-left">
                    <button type="reset" class="btn btn-warning">Reset</button>
                </div>
            </div>
        </div>
    </form>
    <!-- /.box-body -->
</div>

<div class="modal fade" id="modal-default" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Branch Details</h4>
            </div>
            <div class="modal-body" id="modal-default-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
</script>