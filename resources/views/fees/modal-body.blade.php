<table class="table table-condensed table-small">
    <thead>
    <tr>
        <td>Name</td>
        <td>{{$user->name or "-"}}</td>
    </tr>
    <tr>
        <td>Total Fees</td>
        <td id="totalAmount" data-fee="25000"><b>{{$totalFees or "-"}}</b></td>
    </tr>
    <tr>
        <td>Fee Paid</td>
        <td id="paidAmount" data-fee="5000"><b>{{$totalPaidFees or "-"}}</b></td>
    </tr>
    <tr>
        <td>Total Discount</td>
        <td id="paidAmount" data-fee="5000"><b>{{$totalDiscount or "-"}}</b></td>
    </tr>
    <tr>
        <td>Total Advance</td>
        <td id="paidAmount" data-fee="5000"><b>{{$user->student->advance_amount or "-"}}</b></td>
    </tr>
    <tr>
        <td>Total Refund</td>
        <td id="refundAmount" ><b>{{ $totalRefund or "-"}}</b></td>
    </tr>
    <tr>
        <td>Fee Pending</td>
        <td id="pendingAmount" data-fee="20000"><b>{{$totalPendingFees or "-"}}</b></td>
    </tr>
    </thead>
</table>
<hr>
<h5 style="margin: 5px 8px 5px;">Fee History</h5>
<table class="table table-small no-header table-bordered custom-table-striped">
    <thead>
    <tr>
        <th width="auto"></th>
        <th width="25%">Amount</th>
        <th width="30%">Date</th>
        <th width="35%">Payment Mode</th>
    </tr>
    </thead>
    <tbody>
    @php $i=0; $fees = $user->fees->sortByDesc('id'); $count = count($fees);  @endphp
    @if($count > 0)
        @foreach($fees as $fee)
            @if($fee->payment_type == 0)

                <tr>
                    <td><i class="fa fa-plus btn btn-default" onclick="hideShowFeeDetails(this)" id="detailButton-{{$i}}"
                           style="font-size: 12px; padding: 5px 7px"></i></td>
                    <td>
                        {{$fee->feeDetails->sum("paid_amount") + $fee->getFeeExtraDetailFees($fee->id)}}
                    </td>
                    <td>{{date("d-m-Y", strtotime($fee->payment_date))}}</td>
                    <td>
                        {{config("app.payment_mode.".$fee->payment_mode)}}
                        @if($fee->payment_mode == 1)
                            <br>{{$fee->cheque_number}}
                            <br>{{$fee->bank->name}}
                        @endif
                    </td>
                </tr>
                <tr class="detailButton-{{$i}}" style="display: none">
                    <td class="active"></td>
                    <td colspan="3" style="padding: 0">
                        <table class="table table-small no-header table-bordered table-striped" style="margin-bottom: 0">
                            <thead>
                            <tr>
                                <th width="10%"></th>
                                <th width="40%" class="info">Course</th>
                                <th width="20%" class="info">Fee</th>
                                <th width="20%" class="info">Discount</th>
                                <th width="20%" class="info">Paid</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php  $j=0;  @endphp
                            @foreach($fee->feeDetails as $feeDetail)
                                <tr class="warning">
                                    <td>
                                        @if($feeDetail->extraFeeDetail->count() > 0)
                                            <i class="fa fa-plus btn btn-default" onclick="hideShowFeeDetails(this)"
                                               id="detailButtonExtra-{{$j}}"
                                               style="font-size: 12px; padding: 5px 7px">
                                            </i>
                                        @endif
                                    </td>
                                    <td>{{$feeDetail->course->activity->name}}</td>
                                    <td>{{ getCourseFees($fee->user_id , $feeDetail->course_id) }}</td>
                                    <td>{{$feeDetail->discount}}</td>
                                    <td>{{$feeDetail->paid_amount}}</td>
                                </tr>
                                @if($feeDetail->extraFeeDetail->count() > 0)

                                    <tr class="detailButtonExtra-{{ $j }}" style="display: none;">
                                        <td></td>
                                        <td colspan="4">
                                            <table class="table table-small no-header table-bordered table-striped"
                                                   style="margin-bottom: 0">
                                                <thead>
                                                <tr>
                                                    <th width="40%" class="info">Extra Fees</th>
                                                    <th width="20%" class="info">Fee</th>
                                                    <th width="20%" class="info">Discount</th>
                                                    <th width="20%" class="info">Paid</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($feeDetail->extraFeeDetail as $extraDetails)
                                                    <tr class="success">
                                                        <td> {{ $extraDetails->extraFee->name }} </td>
                                                        <td> {{ \App\CourseExtraFee::where('course_id',$feeDetail->course_id)->where('extra_fee_id',$extraDetails->extra_fee_id)->first()->fees  }} </td>
                                                        <td> {{ $extraDetails->discount }} </td>
                                                        <td> {{ $extraDetails->paid_amount }} </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </td>
                </tr>
            @elseif($fee->payment_type == 1)
                <tr>
                    <td><i class="fa fa-plus btn btn-default" onclick="hideShowFeeDetails(this)" id="detailButton-{{$i}}"
                           style="font-size: 12px; padding: 5px 7px"></i></td>
                    <td>
                        {{$fee->feeDetails->sum("paid_amount") + $fee->getFeeExtraDetailFees($fee->id)}}
                    </td>
                    <td>{{date("d-m-Y", strtotime($fee->payment_date))}}</td>
                    <td>
                        {{config("app.payment_mode.".$fee->payment_mode)}}
                    </td>
                </tr>
                <tr class="detailButton-{{$i}}" style="display: none">
                    <td class="active"></td>
                    <td colspan="3" style="padding: 0">
                        <table class="table table-small no-header table-bordered table-striped" style="margin-bottom: 0">
                            <thead>
                            <tr>
                                <th width="40%" class="info">Course</th>
                                <th width="20%" class="info">Refund</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php  $j=0;  @endphp
                            @foreach($fee->feeDetails as $feeDetail)
                                <tr class="warning">
                                    <td>{{$feeDetail->course->activity->name}}</td>
                                    <td>{{$feeDetail->paid_amount}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </td>
                </tr>
            @endif
            {{--@foreach($fee->feeDetails as $feeDetail)
                <tr class="detailButton-{{$i}}" style="display: none">
                    <td class="active"></td>
                    <td colspan="2" class="warning">{{$feeDetail->course->name}}</td>
                    <td class="warning">{{$feeDetail->paid_amount}}</td>
                </tr>
            @endforeach--}}
            @php $i++; @endphp
        @endforeach
    @else
        <tr>
            <td colspan="4" style="text-align: center">No data available</td>
        </tr>
    @endif

    </tbody>
</table>

<div id="applied_fees" style="display: none" data-is-fixed="0">
</div>