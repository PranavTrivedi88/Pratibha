<div class="row">
    <div class="col-md-2">
        <div class="small-box bg-blue-gradient" style="box-shadow: 3px 4px 10px #888;">
            <div class="inner">
                <h3>{{$totalInquiries}}</h3>
                <p>Total Inquiries</p>
            </div>
            <div class="icon">
                <i class="fa fa-list-alt"></i>
            </div>
            {{--<a href="{{url('/inquiries')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
        </div>
    </div>
    <div class="col-md-2">
        <div class="small-box bg-green-gradient" style="box-shadow: 3px 4px 10px #888;">
            <div class="inner">
                <h3>{{$completedInquiries}}</h3>
                <p>Completed Inquiries</p>
            </div>
            <div class="icon">
                <i class="fa fa-thumbs-o-up"></i>
            </div>
            {{--<a href="{{url('/inquiries')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
        </div>
    </div>
    <div class="col-md-2">
        <div class="small-box bg-purple-gradient" style="box-shadow: 3px 4px 10px #888;">
            <div class="inner">
                <h3>{{$cancelledInquiries}}</h3>
                <p>Cancelled Inquiries</p>
            </div>
            <div class="icon">
                <i class="fa fa-thumbs-o-down"></i>
            </div>
            {{--<a href="{{url('/inquiries')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
        </div>
    </div>
</div>