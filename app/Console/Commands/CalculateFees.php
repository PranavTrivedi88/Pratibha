<?php

namespace App\Console\Commands;

use App\Fee;
use App\FeeDetail;
use App\PendingFee;
use App\StudentDiscount;
use App\User;
use DateTime;
use DateTimeZone;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CalculateFees extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CalculateFees:calculateFees';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate student\'s fees per month.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::whereHas("userCourse", function ($q) {
            $q->where("status", 0);
        })->get();

        $dateTime = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
        $currentDate = $dateTime->format("Y-m-d");
        $currentMonth = $dateTime->format("m");
        $currentYear = $dateTime->format("Y");

        DB::transaction(function() use($users, $currentDate, $currentYear, $currentMonth){

            foreach ($users as $user) {
                $advance = $user->advance_amount;

                if ($advance > 0) {
                    $fees = Fee::create([
                        "user_id" => $user->id,
                        "payment_date" => $currentDate,
                        "net_amount" => 0,
                        "lump_sum_amount" => 0,
                        "payment_mode" => 0,
                    ]);

                    foreach ($user->userCourse as $course) {

                        $discount = StudentDiscount::where("user_id", $user->id)->where("course_id", $course->course_id)->value("discount_amount");
                        $discount = $discount ? $discount : 0;

                        $fee_amount = $course->course->fees;

                        if (($advance + $discount) >= $fee_amount) {

                            FeeDetail::create([
                                "fee_id" => $fees->id,
                                "course_id" => $course->course_id,
                                "discount" => $discount,
                                "paid_amount" => $fee_amount - $discount
                            ]);

                            $advance -= $fee_amount + $discount;

                        } else if($advance > 0){

                            FeeDetail::create([
                                "fee_id" => $fees->id,
                                "course_id" => $course->course_id,
                                "discount" => $discount,
                                "paid_amount" => $advance - $discount
                            ]);

                            PendingFee::create([
                                "user_id" => $user->id,
                                "course_id" => $course->course_id,
                                "month" => $currentMonth,
                                "year" => $currentYear,
                                "fees" => $fee_amount - $advance - $discount
                            ]);

                            $advance = 0;

                        } else{

                            PendingFee::create([
                                "user_id" => $user->id,
                                "course_id" => $course->course_id,
                                "month" => $currentMonth,
                                "year" => $currentYear,
                                "fees" => $fee_amount
                            ]);

                        }
                    }

                    $user->advance_amount = $advance;
                    $user->save();

                } else{

                    foreach ($user->userCourse as $course) {

                        $fee_amount = $course->course->fees;

                        PendingFee::create([
                            "user_id" => $user->id,
                            "course_id" => $course->course_id,
                            "month" => $currentMonth,
                            "year" => $currentYear,
                            "fees" => $fee_amount
                        ]);
                    }

                }
            }

        });

    }
}
