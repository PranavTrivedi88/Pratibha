<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolPaymentHistory extends Model
{

	protected $guarded = [];

    public function school(){
    	$this->belongsTo(School::class,'school_id');
    }

    public static function paidAmount($school_id){
    	return SchoolPaymentHistory::where("school_id", $school_id)->sum("paid_amount");
    }
}
