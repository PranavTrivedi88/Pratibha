<?php

namespace App\Admin\Extensions\Tools;

use Encore\Admin\Admin;
use Encore\Admin\Grid\Tools\AbstractTool;
use Illuminate\Support\Facades\Request;

class GridSearch extends AbstractTool
{
    protected function script()
    {
        $url = Request::fullUrlWithQuery(['search' => '_search_']);

        return <<<EOT
        
            var searchTimeout = null;
            var searchInput = $('#grid-search');
            var resetSearch = $("#reset-search");
            var searchSubmit = $("#search-submit");
            var isFees = false;
            
            if("$url".indexOf("fees") !== -1){
                searchInput.attr("placeholder", "Search by Name");                                
            }
            
            searchInput.keydown(function(event){ 
                var keyCode = (event.keyCode ? event.keyCode : event.which);   
                if (keyCode == 13) {
                    searchSubmit.trigger('click');
                }
            });
            
            searchSubmit.on("click", function () {
            
                clearTimeout(searchTimeout);
                searchTimeout = setTimeout(function(){
                
                    var url = "$url".replace("_search_", searchInput.val());
                    $.pjax({container:'#pjax-container', url: url });
                    
                }, 500);
            
            });
            
            resetSearch.on("click", function(){
                var url = "$url".replace("_search_", "");
                $.pjax({container:'#pjax-container', url: url });
            })
EOT;
    }

    public function render()
    {
        Admin::script($this->script());

        return view('vendor.admin.tools.gridSearch');
    }
}