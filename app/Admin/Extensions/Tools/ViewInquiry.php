<?php

namespace App\Admin\Extensions\Tools;

use App\Inquiry;
use Encore\Admin\Admin;
use Encore\Admin\Grid\Tools\AbstractTool;

class ViewInquiry extends AbstractTool
{
    protected function script()
    {
        return <<<EOT
            $(".open-inquiry-modal").on("click", function(){
                var id = $(this).data('id');
                NProgress.start();
                $.ajax({
                    type:'GET',
                    url:'/inquiries/'+ id,
                    success:function (response) {
                        $("#inquiry-modal-body").html(response);
                        NProgress.done();
                        $("#inquiry-modal").modal("show");
                    },
                    error:function () {
                        toastr.error('Something went wrong.');
                    }
            
                });
            });
EOT;
    }

    public function render()
    {
        Admin::script($this->script());

        $inq_id = request('inquiry_id');

        $inquiry = Inquiry::find($inq_id);

        return view('vendor.admin.tools.viewInquiry',compact('inquiry'));
    }
}