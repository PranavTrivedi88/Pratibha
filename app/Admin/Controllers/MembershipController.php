<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\GridSearch;
use App\Admin\Extensions\Tools\MembershipSearchButtons;
use App\Branch;
use App\Http\Controllers\Controller;
use App\Student;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
Use App\Admin\Extensions\Tools\ExcelExport;
use Carbon\Carbon;

class MembershipController extends Controller
{
    use ModelForm;

    public function index()
    {
        return Admin::content(function (Content $content) {

            if(isRole('branch-admin'))
            {
                $branch = Branch::where("user_id",Admin::user()->id)->first();
            }
            $content->header("Membership Status");

            if(isset($branch))
            {
                $expired = Student::where(function ($q){
                    $q->where("membership_date", null)->orWhere("membership_status", 0);
                })->where('branch_id',$branch->id)->count();

                $aboutToExpire = Student::where("membership_status", 1)->where(DB::raw("datediff(DATE_ADD(membership_date, INTERVAL 1 YEAR), CURDATE())"), "<=", 30)->where('branch_id',$branch->id)->count();
                $active = Student::where("membership_status", 1)->where(DB::raw("datediff(DATE_ADD(membership_date, INTERVAL 1 YEAR), CURDATE())"), ">", 30)->where('branch_id',$branch->id)->count();
            }
            else
            {
                $expired = Student::where("membership_date", null)->orWhere("membership_status", 0)->count();
                $aboutToExpire = Student::where("membership_status", 1)->where(DB::raw("datediff(DATE_ADD(membership_date, INTERVAL 1 YEAR), CURDATE())"), "<=", 30)->count();
                $active = Student::where("membership_status", 1)->where(DB::raw("datediff(DATE_ADD(membership_date, INTERVAL 1 YEAR), CURDATE())"), ">", 30)->count();
            }


            $content->row(view("membership.infoboxes", compact("expired", "aboutToExpire", "active")));
            $content->row($this->grid());

        });
    }

    protected function grid()
    {
        return Admin::grid(Student::class, function (Grid $grid) {

            if(isRole('branch-admin'))
            {
                $branch = Branch::where("user_id",Admin::user()->id)->first();
                $grid->model()->where('students.branch_id',$branch->id);
            }
            $grid->column( "user.name")->sortable();
            $grid->column( "user.contact_no")->sortable();

            $grid->column("membership_date")->display(function ($membership_date){
                if($membership_date){
                    $membership_date = Carbon::parse($membership_date);
                    return $membership_date->format('d/m/Y');
                }
                else
                {
                    return "-";
                }
            });

            $grid->column("membership", "Membership Status")->display(function(){

                $status =  Student::getMembershipStatus($this->user_id);

                if($status >= 0){
                    if($status){
                        return '<span class="span-grid label label-warning">' . ($status + 1) . ' Days Left</span>';
                    } else{
                        return '<span class="span-grid label label-success">Active</span>';
                    }
                } else{
                    if($status == -2){
                        return '<span class="span-grid label label-info">New Student</span> <a href="/fees/create?student_id=' . $this->user_id . '" data-toggle="tooltip" title="Start Membership" class="btn" style="background-color: #32c861; border: #32c861; padding: 0 6px;"><i class="fa fa-plus" style="font-size: 11px; color: white;"></i></a>';
                    } else {
                        return '<span class="span-grid label label-danger">Expired</span> <a href="/fees/create?student_id=' . $this->user_id . '" data-toggle="tooltip" title="Renew Membership" class="btn" style="background-color: #32c861; border: #32c861; padding: 0 6px;"><i class="fa fa-plus" style="font-size: 11px; color: white;"></i></a>';
                    }
                }
            });

            $grid->disableActions();
            $grid->disableRowSelector();
            $grid->disableCreateButton();


            $excel_headers = ["Student Name","Mobile No","Membership Date","Membership"];
            $excel_columns = ["user.name","user.mobile","membership_date","membership_status"];
            $grid->exporter(new ExcelExport("Membership", $excel_headers, $excel_columns));

            $grid->filter(function ($filter){

                $filter->disableIdFilter();
                $filter->like('user.name','Student Name');
                $filter->like('user.contact_no','Mobile No');

                $status = [];
                $status[1] = "Active";
                $status[-2] = "New Student";
                $status[0] = "Expired";

                $filter->like('membership_status','Membership Date')->select($status);


            });

        });
    }
}
