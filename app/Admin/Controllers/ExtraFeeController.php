<?php

namespace App\Admin\Controllers;

use App\Branch;
use App\ExtraFee;
use App\Admin\Extensions\Tools\GridSearch;
use Illuminate\Support\Facades\Input;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Widgets\Box;
use Encore\Admin\Widgets\Form as FormWidget;
use Encore\Admin\Layout\Row;
use App\Admin\Extensions\Tools\ExcelExport;

class ExtraFeeController extends Controller
{
    use ModelForm;

    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Extra Fee');
            $content->row(function(Row $row) {
                    $row->column(6, $this->inline());
                    $row->column(6, $this->grid());
                });

        });
    }

    public function inline($id = null)
    {
        $form = new FormWidget();
        $data = ExtraFee::find($id);
        $form->attribute(['class' => 'form-inline inline-custom-form']);
        if ($id != null) {
            $form->action("/extra-fees/$id");
            $form->text('name', '')->rules('required')->default($data->name)->placeholder('Enter Extra Fees')->attribute('autofocus');
            $form->hidden('_method')->default('PUT');
        } else {
            $form->action('/extra-fees');
            $form->text('name', '')->rules('required')->placeholder('Enter Extra Fees')->attribute('autofocus');
        }
        $form->saving(function ($form){
            $form->name = ucwords($form->name);
        });

        $box = new Box('Add New', $form->render());
        $box->style('custom-form');
        return $box;
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Fee Types');
            $content->row(function(Row $row) use ($id) {
                $row->column(6, $this->inline($id));
                $row->column(6, $this->grid());
            });
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    protected function grid()
    {
        return Admin::grid(ExtraFee::class, function (Grid $grid) {
            $grid->model()->orderBy('name','ASC');
            $grid->name();
            $grid->disableRowSelector();
            $grid->disableCreateButton();
            $grid->disableFilter();

            $grid->actions(function ($action){
                $action->disableEdit();
                $action->prepend('<a href="'.url('extra-fees').'/'.$action->row->id.'/edit" class="btn grid-row-edit"><i class="fa fa-pencil"></i></a>');
            });

            $excel_headers = ["Name"];
            $excel_columns = ["name"];
            $grid->exporter(new ExcelExport("Extra Fees", $excel_headers, $excel_columns));

            $grid->tools(function ($tools) {
                $tools->append(new GridSearch());
            });

            $value = Input::get('search');

            if (!empty($value)) {

                $q = $grid->model();

                $q->where('name', "like", "%{$value}%");

            }
        });
    }

    protected function branchGrid()
    {
        return Admin::grid(Branch::class, function (Grid $grid) {
            $grid->model()->orderBy('id','Desc');
            $grid->name();
            $grid->disableRowSelector();
            $grid->disableExport();
            $grid->disableFilter();

            $grid->actions(function ($action){
                $action->disableEdit();
                $action->prepend('<a href="'.url('extra-fees').'/'.$action->row->id.'/edit" class="btn grid-row-edit"><i class="fa fa-pencil"></i></a>');
            });
        });
    }

    protected function form()
    {
        return Admin::form(ExtraFee::class, function (Form $form) {

            $form->text('name', 'Name')->rules('required')->rules(function ($form){

                // If it is not an edit state, add field unique verification
                if (!$id = $form->model()->id) {
                    return 'required|unique:extra_fees,name';
                }
                return 'required|unique:extra_fees,name,'.$form->model()->id;
            })->attribute('autofocus');
            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableListButton();
            });
            $form->saving(function (Form $form){
                 $form->name = ucwords($form->name);
            });
            $form->disableReset();
        });
    }
}
