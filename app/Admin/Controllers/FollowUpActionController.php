<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\GridSearch;
use App\FollowUpAction;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Admin\Extensions\Tools\ExcelExport;

class FollowUpActionController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            Admin::script(["
                $('form').attr('action', '/followUpAction');
            "]);

            $content->header('Follow Up Actions');

            $content->row(function(Row $row) {
                $row->column(4, $this->form());
                $row->column(8, $this->grid());
            });
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            Admin::script(["
                $('.col-md-8 .box-header .btn-group.pull-right a').attr('href', '/followUpAction');
            "]);

            $content->header('Follow Up Actions');

            $content->row(function(Row $row) use ($id){
                $row->column(4, $this->form()->edit($id));
                $row->column(8, $this->grid($id));
            });
        });
    }

    public function addFollowupAction(Request $request)
    {

        $validate = Validator::make($request->all(),
            [
                'name' => 'required'
            ],
            [
                'name.required' => "The Followup Action field is required."
            ]
        );

        if ($validate->fails()) {

            $error['error'] = $validate->getMessageBag();
            return response()->json($error, 406);

        } else {

            $followup_action = FollowUpAction::updateOrCreate(
                ["name" => $request->name]
            );

            return json_encode($followup_action);
        }
    }

    protected function grid($id = null)
    {
        return Admin::grid(FollowUpAction::class, function (Grid $grid) use($id) {
            $grid->model()->orderBy('name','ASC');
            $grid->name('Name')->sortable();

            $grid->disableRowSelector();
            $grid->disableFilter();

            $grid->actions(function ($actions) {

                $actions->disableEdit();
                $actions->prepend('<a href="/followUpAction/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');

            });

            if(!isset($id)){
                $grid->disableCreateButton();
            }

            $excel_headers = ["Name"];
            $excel_columns = ["name"];
            $grid->exporter(new ExcelExport("Follow Up Action", $excel_headers, $excel_columns));

            $grid->tools(function ($tools) {
                $tools->append(new GridSearch());
            });

            $value = Input::get('search');

            if (!empty($value)) {

                $q = $grid->model();

                $q->where('name', "like", "%{$value}%");

            }
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(FollowUpAction::class, function (Form $form) {

            $form->text('name')->rules("required")->rules(function ($form){

                // If it is not an edit state, add field unique verification
                if (!$id = $form->model()->id) {
                    return 'required|unique:follow_up_actions,name';
                }
                return 'required|unique:follow_up_actions,name,'.$form->model()->id;
            })->attribute('autofocus');

            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableListButton();
            });

            $form->saved(function () {
                admin_toastr(trans('admin.save_succeeded'));
                return redirect("/followUpAction");
            });

            $form->setWidth(8, 3);

            $form->saving(function ($form){
                $form->name = ucwords($form->name);
            });

        });
    }
}
