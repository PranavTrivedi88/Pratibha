<?php

namespace App\Admin\Controllers;

use App\AppUser;
use App\Student;
use App\StudentExtraFeePayment;
use App\Activity;
use App\Branch;
use App\BranchExtraFee;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\ExtraFee;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class StudentExtraFeePaymentController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(' $.getScript("/js/student-extra-fee-payment.js");');
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Take Extra Fee');
            $activities =null;
            $students= null;
            $branch = Branch::where('user_id',Admin::user()->id)->first();
            if(isset($branch))
            {
                $activities = Activity::join('courses','courses.activity_id','=','activities.id')->select('activities.id','activities.name')->where('courses.branch_id',$branch->id)->get();
                $students = AppUser::join('students','app_users.id','=','students.user_id')->select('app_users.id','app_users.name')->where('students.branch_id',$branch->id)->get();
            }
            else
            {
                $activities = Activity::all();
                $students = AppUser::join('students','app_users.id','=','students.user_id')->select('app_users.id','app_users.name')->get();
            }
            $content->row(function(Row $row) use($activities,$students,$branch) {
                $row->column(4, view('studentExtraFeePayment.create',compact('activities','students','branch')));
                $row->column(8, $this->grid());
            });
        });
    }



    public function store(Request $request)
    {
        $error = [];
        DB::beginTransaction();

        $validate = Validator::make($request->all(), [
            'items' => 'required',
            'activity_name' => 'required',
            'student_name' => 'required',
            'amount' => 'required'
        ]);

        if ($validate->fails())
        {
            $error['payment_details'] = $validate->getMessageBag();
        }
        else
        {
            $studentEFPayment = new StudentExtraFeePayment();
            $studentEFPayment->student_id = $request->student_name;
            $studentEFPayment->activity_id = $request->activity_name;
            $studentEFPayment->branch_id = $request->branch_id;
            $studentEFPayment->extra_fee_id = serialize($request->items);
            $studentEFPayment->amount = $request->amount;
            $studentEFPayment->save();
        }

        //Set response
        if (count($error) > 0)
        {
            DB::rollback();
            return response()->json($error, 500);

        }
        else
        {
            DB::commit();
            return response()->json("success", 200);
        }
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(StudentExtraFeePayment::class, function (Grid $grid) {

            $grid->disableExport();
            $grid->disableFilter();
            $grid->disableRowSelector();
            $grid->disableCreateButton();
            $grid->disableActions();

            if(!Admin::user()->isAdministrator() AND !isRole('super-administrator'))
            {
                $branch = Branch::where('user_id',Admin::user()->id)->first();
                $grid->model()->where('branch_id',$branch->id);
            }

            $grid->column('student_id','Student Name')->display(function ($id){
                return AppUser::find($id)->name;
            });

            $grid->column('activity_id','Activity Name')->display(function ($id){
                return Activity::find($id)->name;
            });

            $grid->column('extra_fee_id','Extra Fee For')->display(function ($id){
                $array = unserialize($id);
                $latsIndex = count($array) - 1;
                $str =null;
                foreach ($array as $key => $value)
                {
                    $extraFee = ExtraFee::join('branch_extra_fees','extra_fees.id','=','branch_extra_fees.extra_fee_id')->select('extra_fees.name')->where('branch_extra_fees.id',$value)->first();
                    if($key == $latsIndex)
                    {
                        $str = $str.$extraFee->name;
                    }
                    else
                    {
                        $str = $str.$extraFee->name.',';
                    }
                }
                return $str;
            });

            $grid->amount("Amount");

            if(Admin::user()->isAdministrator())
            {
                $grid->column('branch_id','Branch Name')->display(function (){
                    $student =  Student::where('user_id',$this->student_id)->first();
                    return  $student->branch->name;
                });
            }
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(StudentExtraFeePayment::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function selectBox($id)
    {
        $items = BranchExtraFee::join('extra_fees','branch_extra_fees.extra_fee_id','=','extra_fees.id')->select('extra_fees.name','branch_extra_fees.amount','branch_extra_fees.id')->where('branch_extra_fees.activity_id',$id)->get();
        return view('studentExtraFeePayment.selectBox',compact('items'));
    }
}
