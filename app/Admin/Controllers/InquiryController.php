<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\ExcelExport;
use App\Admin\Extensions\Tools\GridSearch;
use App\Activity;
use App\AppUser;
use App\City;
use App\Course;
use App\FollowUp;
use App\InquiryCourse;
use App\Http\Controllers\Controller;
use App\Inquiry;
use App\InquirySource;
use App\Parents;
use App\Profession;
use App\School;
use App\User;
use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Branch;

class InquiryController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $('[data-toggle=\"tooltip\"]').tooltip();
            $('input').css('width', '100%');
            $.getScript('/js/inquiry.js');
            $.getScript('/js/school.js');
            $('.city_id').closest('.form-group').append(\"<a href='#' class='btn btn-default add-button' data-modal='add-city-modal' onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>\");
            $('.source_id').closest('.form-group').append(\"<a href='#' class='btn btn-default add-button' data-modal='add-source-modal' onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>\");
        "]);
    }

    public function index()
    {
        return Admin::content(function (Content $content) {

            Admin::script(["
                $.getScript('/js/register.js');
            "]);

            $content->header('Inquiries');

            $content->row(function (Row $row) {
//                $row->column(4, $this->form());
                $row->column(12, $this->grid());
                $row->column(12, $this->getModel());
            });
            $professions = Profession::all();
            $cities = City::all();
            $content->row(view("inquiry.modal-view"));
            $content->row(view("student.modal"));
            $content->row(view("addModal.addCity"));
            $content->row(view("addModal.addParent", compact("professions", "cities")));
            $content->row(view("addModal.addSchool", compact("cities")));
            $content->row(view("addModal.addProfession"));
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Inquiry');
            $content->row($this->form());
            $content->row(view("addModal.addCity"));
            $content->row(view("addModal.addSource"));

        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit Inquiry');

            $content->body($this->form($id)->edit($id));

        });
    }

    public function show(Inquiry $inquiry)
    {
        $course_ids = (InquiryCourse::where("inquiry_id", $inquiry->id)->pluck("course_id")->toArray());
        $courses = Course::whereIn("id", $course_ids)->get();
        return view('inquiry.inquiry-modal-body', compact('inquiry', 'courses'));
    }

    public function destroy(Inquiry $inquiry)
    {

        FollowUp::where("inquiry_id", $inquiry->id)->delete();
        $inquiry->delete();

        return response()->json(array('status' => true, 'message' => "Delete succeeded !"), 200);
    }

    public function completedInquiries()
    {
        if (Session::has('user_id')) {

            $userId = Session::get('user_id');

        } else {

            $userId = Admin::user()->id;

        }

        if ($userId != 1) {

            $inquiries = Inquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->join("inquiry_wise_users", "visa_inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->where('visa_inquiries.status', '=', 1)->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();

        } else {

            $inquiries = Inquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->where('visa_inquiries.status', '=', 1)->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();

        }

        return view('/inquiries', compact("inquiries", "userId"));
    }

    public function cancelledInquiries()
    {
        if (Session::has('user_id')) {

            $userId = Session::get('user_id');

        } else {

            $userId = Admin::user()->id;

        }

        if ($userId != 1) {

            $inquiries = Inquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");

            })->join("inquiry_wise_users", "visa_inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->where('visa_inquiries.status', '=', 2)->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();

        } else {

            $inquiries = Inquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->where('visa_inquiries.status', '=', 2)->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();
            /*$inquiries = Inquiry::where("status", "=", 2)->get();*/
        }

        return view('/inquiries', compact("inquiries", "userId"));
    }

    public function forwardedInquiries()
    {
        if (Session::has('user_id')) {
            $userId = Session::get('user_id');
        } else {
            $userId = Admin::user()->id;
        }

        if ($userId != 1) {
            $inquiries = Inquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->join("inquiry_wise_users", "visa_inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->groupBy('reasons.inquiry_id')->orderBy('status', 'asc')->orderBy('visa_inquiries.updated_at', 'desc')->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();
        } else {
            $inquiries = Inquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->groupBy('reasons.inquiry_id')->orderBy('status', 'asc')->orderBy('visa_inquiries.updated_at', 'desc')->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();
            /*$inquiries = Inquiry::join('reasons', 'inquiries.id', '=', 'reasons.inquiry_id')->where('inquiries.status', '=', 0)->groupBy('reasons.inquiry_id')->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();*/
        }
        return view('/inquiries', compact("inquiries", "userId"));
    }

    public function autoCompleteFriend()
    {

        $friend = Inquiry::where('friend_name', '!=', null)->groupBy('friend_name')->pluck('friend_name')->toArray();

        $tag = Inquiry::whereIn('friend_name', $friend)->groupBy('tag')->pluck('tag')->toArray();

        return response()->json(array('friend' => $friend, 'tag' => $tag));
    }

    public function getModel()
    {
        return view('student.modal');
    }

    public function registerForm($id)
    {
        $cities = City::all();
        if(isRole('branch-admin'))
        {
            $branch = Branch::where('user_id',Admin::user()->id)->first();
            $course = Course::where('branch_id',$branch->id)->get();
            $parents = Parents::where('branch_id',$branch->id)->get();
            $schools = School::where('branch_id',$branch->id)->get();
        }
        else {
            $course = Course::all();
            $parents = Parents::all();
            $schools = School::all();
        }

        $student = Inquiry::find($id);
        if ($student->username != '') {

            $user = AppUser::where('user', $student->username)->first();
            $student->user = $user;
            $student->user_id = $user->id;
            $student->name = ucwords($user->name);
            $student->mobile = $user->mobile;
            $student->email = $user->email;
            $student->parent = $user->student->parent_id;
            $student->bdate = $user->student->bdate;
            $student->city_id = $user->city_id;
            $student->school = $user->student->school_id;
            $student->address = $user->address;
            $student->blood_group = $user->student->blood_group;
            $student->guardian_name = $user->student->guardian_name;
            $student->relation = $user->student->guardian_relation;
            $student->guardian_mobile = $user->student->guardian_mobile;
            $student->guardian_email = $user->student->guardian_email;
            $student->guardian_profession = $user->student->guardian_profession;

        }
        $professions = Profession::all();

        return view('inquiry.register', compact('course', 'parents', 'cities', 'schools', 'student', 'professions','branch'));
    }

    protected function grid($id = null)
    {
        return Admin::grid(Inquiry::class, function (Grid $grid) use ($id) {

            $grid->model()->orderBy('id', 'desc');
            $grid->model()->where('is_registered', '0');
            if (!Admin::user()->isAdministrator() AND !isRole('super-administrator')) {
                $branch = Branch::where('user_id',Admin::user()->id)->first();
                $grid->model()->where('branch_id', $branch->id);
            }
            $grid->name('Name')->sortable();
            $grid->column("courses", "Activity")->display(function () {

                $courseNames = Activity::whereHas("courses", function($q){
                    $q->whereHas("inquiryCourse", function($que){
                        $que->where("inquiry_id", $this->id);
                    });
                })->pluck("name")->toArray();

                $string = '';

                foreach ($courseNames as $course) {
                    $string .= '<span class="span-grid label label-success" style="margin-right: 5px">' . $course . '</span>';
                }

                return $string;
            });

            $grid->mobile('Mobile')->sortable();
            $grid->column('city.name', 'City');
            $grid->column('source.name', 'Source');
            if(Admin::user()->isAdministrator())
            {
                $grid->column("branch.name", "Assigned To");
            }
            $grid->actions(function ($actions) {

                $actions->disableEdit();
                $actions->disableDelete();

                $actions->prepend('<a href="/inquiries/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');
                $actions->prepend('<a href="/followUp?inquiry_id=' . $actions->row->id . '" data-toggle="tooltip" title="Follow Up" class="btn" style="background-color: #32c861; border: #32c861"><i class="fa fa-plus"></i></a>');
                $actions->prepend('<a href="" data-id="' . $actions->row->id . '" class="btn open-inquiry-modal hidden" style="background-color: #01a4f8; border: #01a4f8;"><i class="fa fa-eye"></i></a>');
                $actions->append('<a class="btn btn-danger delete-row" data-id="' . $actions->row->id . '"><i class="fa fa-trash"></i></a>');

                if ($actions->row->is_registered == 0) {
                    $actions->append('<a title="Register" data-toggle="tooltip" href="" class="btn-xs btn openModal" style="background-color:blueviolet;" data-id="' . $actions->row->id . '"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a>');
                }

            });


            $excel_headers = ["Inquiry", "Activities", "Mobile", "City", "Source", "Assigned To"];
            $excel_columns = ["name", "courses", "mobile", "city.name", "source.name", "branch.name"];
            $grid->exporter(new ExcelExport("Inquiries", $excel_headers, $excel_columns));


            //$grid->disableExport();
            //$grid->disableFilter();

            $grid->filter(function ($filter){

                $filter->disableIdFilter();

                $filter->like("name");

                $filter->where(function($q){
                    $q->WhereHas("courseOfInquiries", function($query) {
                        $query->whereHas("course", function ($que) {
                            $que->whereIn("activity_id", $this->input);
                        });
                    });
                }, "Course")->multipleSelect(Activity::where('id','!=','1')->pluck("name", "id")->toArray());

                $filter->like("mobile");

                $filter->where(function($q){
                    $q->whereHas('city', function ($query) {
                        $query->where("id", $this->input);
                    });
                }, "City")->select(City::pluck("name", "id")->toArray());

                $filter->where(function($q){
                    $q->whereHas('source', function ($query) {
                        $query->where("id", $this->input);
                    });
                }, "Source")->select(InquirySource::pluck("name", "id")->toArray());

                if(isRole('administrator') || isRole('super-administrator')){
                    $filter->where(function($q){
                        $q->where('branch_id',$this->input);
                    }, "Assigned To")->select(Branch::pluck("name", "id")->toArray());
                }

            });


            $grid->disableRowSelector();
        });
    }

    protected function form($id = null)
    {
        return Admin::form(Inquiry::class, function (Form $form) use ($id) {

            $form->tools(function (Form\Tools $tools) {
                // Disable list btn
                $tools->disableListButton();
            });

            $branch = Branch::where('user_id',Admin::user()->id)->first();

            $form->text("username", "Student Register No")->attribute(['id' => 'register_no'])->rules('is_user')
                ->help("Enter student registration number if he/she is a existing student to auto fill details.");
            $form->divider();
            $form->text('name')->rules('required')->attribute('autofocus');


            if(isRole('branch-admin'))
            {
                $branch = Branch::where('user_id',Admin::user()->id)->first();
                $course = Course::where('branch_id',$branch->id)->get();
            }
            else
            {
                $course = Course::all();
            }
            $courseTemp = [];
            foreach ($course as $value)
            {
                if(isRole('branch-admin'))
                {
                    $courseTemp[$value->id] = $value->activity->name;
                }
                else
                {
                    $courseTemp[$value->id] =$value->activity->name." (".$value->branch->name.")";
                }

            }
            $course =$courseTemp;
            if (isset($id))
            {
                $courses = InquiryCourse::where("inquiry_id", $id)->pluck("course_id")->toArray();

                $form->multipleSelect('inquiryCourses', 'Activity')->options($course)->default($courses)->rules('required');
            }
            else {

                $form->multipleSelect('inquiryCourses', 'Activity')->options($course)->rules('required');
            }
            $form->mobile('mobile')->options(['mask' => '9999999999'])->rules('required|numeric|min:10');
            $form->mobile('alternate_mobile', "Alternate Number")->options(['mask' => '9999999999'])->rules('nullable|numeric|min:10');
            $form->text('email');
            $form->textarea('address')->attribute(['id' => 'address'])->rows(3);
            $form->select('city_id', "City")->attribute(['id' => 'city'])->options(City::pluck("name", "id"))->rules('required');
            $form->select('source_id', "Source")->options(InquirySource::pluck("name", "id"))->attribute("id", "inquirySource");
            $form->text('friend_name', "Reference Name");
            $form->text('tag', "Tag");
            $form->textarea('other_source', "Other Source")->rows(3)->attribute("id", "otherSource");
            $form->textarea('remark', "Remarks")->rows(3);
                /*$form->select('user_id', "Assign To")->options(User::whereHas("roles", function ($q) {
                    $role = Role::whereIn("slug", ["branch-admin", "receptionist", "administrator"])->pluck('id')->toArray();
                    $q->whereIn('role_id', $role);
                })->pluck("name", "id"))->rules('required');*/

//            $user_ids = array();
//            $user_ids[] = Admin::user()->id;
//
//            if($branch)
//            {
//                $user_ids[] = $branch->user_id;
//            }
//
//            $form->select('user_id', "Assign To")->options(User::whereIn("id",[$user_ids])->pluck("name", "id"))->rules('required');

            if(Admin::user()->isAdministrator() || isRole('super-administrator'))
            {
                $form->select('branch_id', 'Assign To')->options(Branch::pluck('name', 'id'))->rules('required');
            }
            else
            {
                $form->hidden('branch_id')->default($branch->id);
            }

            if (isset($id)) {
                $status = array();
                foreach (config("app.inquiry_status") as $key => $value) {
                    $status[$key] = $value["name"];
                }

                $form->select('status')->default($form->model()->status)->options($status);
            }

            $form->saving(function ($form) {
                $form->name = ucwords($form->name);
            });

            $form->saved(function ($form) {
                admin_toastr(trans('admin.save_succeeded'));
                return redirect("/inquiries");
            });

            $form->setWidth(8, 3);

            /*$form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableListButton();
            });*/
        });
    }

    public function update($id)
    {
        return $this->form($id)->update($id);
    }
}
