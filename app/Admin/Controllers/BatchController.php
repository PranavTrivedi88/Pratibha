<?php

namespace App\Admin\Controllers;

use App\Activity;
use App\Batch;

use App\Branch;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use App\Course;
use App\Admin\Extensions\Tools\GridSearch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Admin\Extensions\Tools\ExcelExport;

class BatchController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $('.col-md-8 .box-header .btn-group.pull-right a.btn.btn-sm.btn-success').attr('href', '/batches');
        "]);
    }

    public function index()
    {
        return Admin::content(function (Content $content) {

            Admin::script(["
                $('form').attr('action', '/batches');
            "]);

            $content->header('Batches');

            $content->row(function(Row $row){
                $row->column(4, $this->form());
                $row->column(8, $this->grid());
            });
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Batches');

            $content->row(function (Row $row) use ($id) {
                $row->column(4, $this->form()->edit($id));
                $row->column(8, $this->grid($id));
            });
        });
    }

    protected function grid($id = null)
    {
        return Admin::grid(Batch::class, function (Grid $grid) use($id) {
            $grid->model()->orderBy('name','ASC');
            $grid->disableRowSelector();
            if(Admin::user()->isRole("branch-admin"))
            {
                $branch = Branch::where("user_id", Admin::user()->id)->first();
                if($branch) {
                    $courses = Course::where("branch_id", $branch->id)->pluck("id")->toArray();
                    $grid->model()->whereIn("course_id", $courses);
                }
            }
            $grid->name('Name')->sortable();

            $grid->column('course_id',"Activity Name")->display(function($course_id){
                return $this->activityName($course_id);
            });

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableEdit();
                $actions->prepend('<a href="/batches/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');
            });

            if(!isset($id)) {
                $grid->disableCreateButton();
            }

            $grid->paginate(10);

            $excel_headers = ["Batch Name","Activity Name"];
            $excel_columns = ["name","course_id"];
            $grid->exporter(new ExcelExport("Batches", $excel_headers, $excel_columns));

            $grid->filter(function ($filter){

                $filter->disableIdFilter();

                $filter->where(function($q){
                    $q->where('id',$this->input);
                }, "Batch")->select(Batch::pluck("name", "id")->toArray());



                $filter->where(function($q){

                    $q->whereHas("course", function ($que) {

                        $que->whereHas("activity", function ($que) {
                            $que->where("id", $this->input);
                        });

                    });
                }, "Course")->select(Activity::where('id','!=','1')->orderBy('name','ASC')->pluck("name", "id")->toArray());

            });

        });
    }

    protected function form()
    {
        return Admin::form(Batch::class, function (Form $form){
            $branch = Branch::where('user_id',Admin::user()->id)->first();
            $form->text('name','Name')->rules('required')->attribute('autofocus');

            $courses = [];
            if(isset($branch))
            {
                $branchCourse = Course::where('branch_id',$branch->id)->get();
            }
            else
            {
                $branchCourse = Course::all();
            }

            foreach ($branchCourse as $course)
            {

                if(isset($branch))
                {
                    $courses[$course->id] = $course->activity->name;
                }
                else
                {
                    $courses[$course->id] =$course->activity->name." (".$course->branch->name.")";
                }

            }
            asort($courses);
            $form->select('course_id','Activity')->options($courses)->rules('required');
            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableListButton();
            });

            $form->saving(function (Form $form){
               $form->name = ucwords($form->name);
            });

            $form->saved(function () {
                admin_toastr(trans('admin.save_succeeded'));
                return redirect("/batches");
            });

            $form->setWidth(8, 3);

            $form->saving(function ($form){
                $form->name = ucwords($form->name);
            });

            $form->submitted(function ($form) {
                $validator = Validator::make(Input::all(), [
                    'name' => 'required|unique:batches,name,NULL,id,course_id,'.Input::get('course_id'),
                    'course_id' => 'required|unique:batches,course_id,NULL,id,name,'.Input::get('name')
                ]);
                if ($validator->fails())
                {
                    return back()->withInput(Input::all())->withErrors(['name' => ['This Batch Already Exist.']]);
                }

            });

        });
    }

    public function addBatch(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'course_id' => 'required'
        ],[
            'course_id.required' => 'The Course Field is required'
        ]);

        if ($validate->fails()) {
            $error['error'] = $validate->getMessageBag();

            return response()->json($error, 406);
        } else {

            $batch = Batch::updateOrCreate(
                [
                    "name" => $request->name,
                    'course_id' => $request->course_id
                ]
            );

            return json_encode($batch);
        }
    }
}
