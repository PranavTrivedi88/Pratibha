<?php

namespace App\Admin\Controllers;

use App\AppUser;
use App\Branch;
use App\School;
use App\SchoolDetail;

use App\User;
Use App\AppLogin;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use App\City;
use App\Admin\Extensions\Tools\GridSearch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Admin\Extensions\Tools\ExcelExport;
class SchoolController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $('.city_id').closest('.form-group').append(\"<a href='#' class='btn btn-default add-button' data-modal='add-city-modal' onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>\");
            $.getScript('/js/school.js');
        "]);
    }

    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Schools');
            $branch = null;
            if(isRole('branch-admin'))
            {
                $branch = Branch::where('user_id',Admin::user()->id)->first();
            }
            $content->row(function(Row $row) use($branch) {
                $row->column(4, view('school.createSchool',compact('branch')));
                $row->column(8, $this->grid());
            });
            $content->row(view("addModal.addCity"));
        });
    }

    public function show(School $school)
    {
        $contactDetails = SchoolDetail::where('school_id', $school->id)->get();
        return view('school.modal.body', compact('school', 'contactDetails'));
    }

    public function store(Request $request)
    {
        $arr = [];
        $error = [];
        DB::beginTransaction();

        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'contact_no' => ['required', 'digits:10','unique:schools,contact_no'],
            'city_id'=>'required',
            'school_status'=>'required',
            'branch_name'=>'required'
        ]);

        if ($validate->fails()) 
        {
            $error['school_details'] = $validate->getMessageBag();
        }
        else
        {
            $appLogin = AppLogin::where('username',$request->contact_no)->first();
            if(!isset($appLogin))
            {
                $appLogin = new AppLogin();
                $appLogin->username = $request->contact_no;
                $appLogin->password = bcrypt(config('app.default_password'));
                $appLogin->save();
            }

            $appUser = new AppUser();
            $appUser->name = ucwords($request->name);
            $appUser->contact_no = $request->contact_no;
            $appUser->app_login_id = $appLogin->id;
            $appUser->role_id = Role::select('id')->where('slug','school')->first()->id;
            $appUser->save();

            $school = School::create(array_merge(request(['name','contact_no','city_id','school_status','address']),['user_id'=>$appUser->id,'branch_id'=> $request->branch_name]));
            $school->name = ucwords($school->name);
            $school->save();
            $school_id = $school->id;

            $appUser->relationship_id = $school->id;
            $appUser->save();

        } 
        foreach ($request->contactDetail as $key => $val) 
        {
            $arr['contactname'] = $val['contactname'];
            $arr['mobile'] = $val['mobile'];
            $arr['email'] = $val['email'];

            $messsages = array(
                'contactname.required' => 'The name field is required',
                'mobile.required' => 'The Mobile field is required',
                'mobile.digits' => 'The Mobile field is required 10 digits'
            );

            $rules = array(
                'contactname' => ['required'],
                'mobile' => ['required', 'digits:10 '],
                'email' => 'email|nullable'
            );

            $validateForContact = Validator::make($arr, $rules, $messsages);

            if ($validateForContact->fails()) 
            {
                $error['contact_details'][$key] = $validateForContact->getMessageBag();
            }
            else
            {
                if(isset($school_id))
                {
                   $contacts = new SchoolDetail();
                   $contacts->school_id = $school_id;
                   $contacts->name = ucwords($arr['contactname']);
                   $contacts->mobile = $arr['mobile'];
                   $contacts->email = $arr['email'];
                   $contacts->save();
               }

            }
        }

        //Set response
        if (count($error) > 0) 
        {
            DB::rollback();
            return response()->json($error, 500);

        } 
        else 
        {
            DB::commit();
            return response()->json("success", 200);
        }

    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Schools');

            $school = School::where('id',$id)->first();

            if(isset($school->id))
            {
                $schoolDetails = SchoolDetail::where('school_id',$school->id)->get();
            

                $content->row(function(Row $row) use ($school,$schoolDetails,$id) {
                    $row->column(4, view('school.editSchool',compact('school','schoolDetails','id')));
                    $row->column(8, $this->grid());
                });
                $content->row(view("addModal.addCity"));

            }

        });
    }

    public function addSchool(Request $request)
    {

        //dd($request->city_id);

        $validate = Validator::make($request->all(),
            [
                'name' => 'required',
                "city_id" => 'required'
            ],
            [
                "city_id.required" => "The City field is required."
            ]
        );

        if ($validate->fails()) {

            $error['error'] = $validate->getMessageBag();
            return response()->json($error, 406);

        } else {

            $appLogin = AppLogin::where('username',$request->contact_no)->first();
            if(!isset($appLogin))
            {
                $appLogin = new AppLogin();
                $appLogin->username = $request->contact_no;
                $appLogin->password = bcrypt(config('app.default_password'));
                $appLogin->save();
            }
            //User
            $appUser = new AppUser();
            $appUser->name = $request->name;
            $appUser->contact_no = $request->contact_no;
            $appUser->app_login_id = $appLogin->id;
            $appUser->role_id = Role::select('id')->where('slug','school')->first()->id;
            $appUser->save();


            $school = School::create(array_merge(request(['name','contact_no','city_id','school_status','address']),['user_id'=>$appUser->id,'branch_id'=> $request->branch_name]));

            $appUser->relationship_id = $school->id;
            $appUser->save();

            return json_encode($school);
        }
    }

    protected function grid($id = null)
    {
        return Admin::grid(School::class, function (Grid $grid) use ($id) {
            $branch = null;
            $grid->disableRowSelector();
            if(isRole('branch-admin'))
            {
                $branch = Branch::where('user_id',Admin::user()->id)->first();
                $grid->model()->where('branch_id',$branch->id);
            }
            $grid->model()->orderBy('name','ASC');
            $grid->name('Name')->sortable();
            $grid->contact_no('Contact No');
            $grid->column('city.name','City Name');
            $grid->school_status('School Status')->display(function ($status) {

                $label = "default";
                if($status == "Green")
                {
                    $label = "success";
                }
                else if($status == "Black")
                {
                     $label = "default";
                }
                else if($status == "Orange")
                {
                     $label = "warning";
                }

                return '<span class="label label-'.$label.'">'.$status.'</span>';
            })->sortable();

            $grid->model()->orderBy("name");

            $grid->actions(function ($actions) {
                $actions->disableEdit();
                $actions->prepend('<a href="/schools/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');
                $actions->prepend('<a href="/payments/create?school_id=' . $actions->row->id .'" class="btn btn-xs btn-success" title="Take Payment" data-toggle="tooltip"><i class="fa fa-rupee"></i></a>');
                $actions->prepend('<a href="#" data-id="' . $actions->row->id . '" class="btn open-branch-view-modal" style="background-color: #01a4f8; border: #01a4f8;"><i class="fa fa-eye"></i></a>');

            });

            if (!isset($id)) {
                $grid->disableCreateButton();
            }

            $grid->paginate(10);

            $excel_headers = ["School Name","Contact No","City Name","School Status"];
            $excel_columns = ["name","contact_no","city.name","school_status"];
            $grid->exporter(new ExcelExport("Schools", $excel_headers, $excel_columns));

            $grid->filter(function ($filter) use ($branch){

                $filter->disableIdFilter();

                if(isRole('branch-admin'))
                {
                    $filter->where(function($q){
                        $q->where('id',$this->input);
                    }, "School")->select(School::where('branch_id',$branch->id)->pluck("name", "id")->toArray());
                }
                else
                {
                    $filter->where(function($q){
                        $q->where('id',$this->input);
                    }, "School")->select(School::pluck("name", "id")->toArray());
                }


                $filter->like("contact_no");

                $filter->where(function($q){
                    $q->where('city_id',$this->input);
                }, "City")->select(City::pluck("name", "id")->toArray());

                $status = [];
                foreach (config('app.school_status') as $key=>$value)
                {
                    $status[$value] = $value;
                }
                $filter->where(function($q){
                    $q->where('school_status',$this->input);
                }, "School Status")->select($status);

            });
        });
    }

    /*protected function form()
    {
        return Admin::form(User::class, function (Form $form) {

            $form->text('school.name', 'Name')->rules('required');
            $form->select('city_id', 'City')->options(City::pluck('name', 'id')->toArray());
            $form->mobile('mobile', 'Mobile')->rules('required')->options(['mask' => '9999999999']);
            $form->textarea('address', 'Address');
            $form->hidden('username');
            $form->hidden('password');
            $form->hidden('name');
            $form->hidden('branch_id')->default(Admin::user()->branch_id);
            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableListButton();
            });

            $form->saving(function (Form $form) {
                $form->username = $form->mobile;
                $form->password = bcrypt(config('app.default_password'));
                $form->name = $form->school['name'];
            });

            $form->saved(function (Form $form) {
                $user = User::find($form->model()->id);
                $user->roles()->save(Role::where('slug', 'school')->first());
                admin_toastr(trans('admin.save_succeeded'));
                return redirect("/schools");
            });

//            $form->setWidth(8, 3);

        });
    }*/

    public function update(School $school,Request $request)
    {
        $arr = [];
        $error = [];
        DB::beginTransaction();

        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'contact_no' => ['required', 'digits:10','unique:users,mobile,'.$school->user_id],
            'city_id'=>'required',
            'school_status'=>'required',
            'branch_name'=>'required'
        ]);

        if ($validate->fails()) 
        {
            $error['school_details'] = $validate->getMessageBag();
        }
        else
        {
            $school->name = ucwords(request()->name);
            $school->contact_no = request()->contact_no;
            $school->city_id = request()->city_id;
            $school->school_status = request()->school_status;
            $school->branch_id = request()->branch_name;
            $school->save();
        } 

        SchoolDetail::where('school_id', $school->id)->delete();

        foreach ($request->contactDetail as $key => $val) 
        {
            $arr['contactname'] = $val['contactname'];
            $arr['mobile'] = $val['mobile'];
            $arr['email'] = $val['email'];

            $messsages = array(
                'contactname.required' => 'The name field is required',
                'mobile.required' => 'The Mobile field is required',
                'mobile.digits' => 'The Mobile field is required 10 digits'
            );

            $rules = array(
                'contactname' => ['required'],
                'mobile' => ['required', 'digits:10 '],
                'email' => 'email|nullable'
            );

            $validateForContact = Validator::make($arr, $rules, $messsages);

            if ($validateForContact->fails()) 
            {
                $error['contact_details'][$key] = $validateForContact->getMessageBag();
            }
            else
            {
               $contacts = new SchoolDetail();
               $contacts->school_id = $school->id;
               $contacts->name = ucwords($arr['contactname']);
               $contacts->mobile = $arr['mobile'];
               $contacts->email = $arr['email'];
               $contacts->save();
            }
        }

        //Set response
        if (count($error) > 0) 
        {
            DB::rollback();
            return response()->json($error, 500);

        } 
        else 
        {
            DB::commit();
            return response()->json("success", 200);
        }

    }

    public function destroy($id)
    {
        $school = School::find($id);
        SchoolDetail::where('school_id', $school->id)->delete();
        $appUser = AppUser::find($school->user_id);
        if($appUser)
        {
            $appUser->delete();
        }

        $schoolDetails = SchoolDetail::where('school_id',$school->id);
        if($schoolDetails)
        {
            $schoolDetails->delete();
        }

        $school->delete();
        return response()->json(array('status' => true, 'message' => "Delete succeeded !"), 200);
    }
}
