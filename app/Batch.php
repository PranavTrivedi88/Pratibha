<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $fillable = ['name','course_id'];
    public function course()
    {
    	return $this->belongsTo(Course::class,'course_id');
    }
    public function courses()
    {
        return $this->belongsTo(Course::class);
    }

    public function activityName($course_id)
    {
        if(isset($course_id))
        {
            return Course::find($course_id)->activity->name;
        }
    }
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

}
