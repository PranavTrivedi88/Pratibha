<?php

namespace App;

use Carbon\Carbon;
use Encore\Admin\Auth\Database\Administrator;
use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    protected $guarded = [];

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function source()
    {
        return $this->belongsTo(InquirySource::class);
    }

    public function inquiryCourses(){
        return $this->belongsToMany(Inquiry::class, "inquiry_courses", "inquiry_id", "course_id");
    }

    public function courseOfInquiries(){
        return $this->hasMany(InquiryCourse::class);
    }

    public static function getUserInquiries($userId)
    {
        return Inquiry::where('user_id', '=', $userId)->orderBy('status', 'asc')->orderBy('updated_at', 'desc')->get();
    }

    public static function getNumberOfFollowUps($inquiry_id){

        return FollowUp::where('inquiry_id', $inquiry_id)->count();

    }

    public static function getDaysFromStartDateOfInquiry($creationDate){

        $date1=date_create($creationDate->format('Y-m-d'));
        $date2=date_create(Carbon::now('Asia/Kolkata')->format('Y-m-d'));
        $diff=date_diff($date1,$date2);
        return $diff->format("%a");

    }
}
