<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeDetail extends Model
{

    protected $fillable = ["fee_id", "course_id", "discount", "paid_amount"];

    public function Fee(){
        return $this->belongsTo(Fee::class);
    }

    public function course(){
        return $this->belongsTo(Course::class)->withoutGlobalScope("removeMembership");
    }

    public function extraFeeDetail()
    {
        return $this->hasMany(ExtraFeeDetails::class);
    }
}
