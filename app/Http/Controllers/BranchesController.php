<?php

namespace App\Http\Controllers;

use App\Branch;
use App\BranchDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class BranchesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branch::all();
        return view('branch.branches', compact('branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('branch.createBranch');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arr = [];
        $error = [];
        $branch_id = 0;

        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'contact_no' => ['required', 'digits:10 ', 'unique:branches,contact_no'],
        ]);

        DB::beginTransaction();

        if ($validate->fails()) {

            $error['branch_details'] = $validate->getMessageBag();
        } else {
            $branch = Branch::create(request(['name', 'address', 'contact_no', 'city_id']));
            $branch_id = $branch->id;
        }

        foreach ($request->contactDetail as $key => $val) {
            $arr['contactname'] = $val['contactname'];
            $arr['mobile'] = $val['mobile'];
            $arr['email'] = $val['email'];

            $messsages = array(
                'contactname.required' => 'The name field is required',
                'contactname.unique' => 'The name is already taken',
            );

            $rules = array(
                'contactname' => ['required '],
                'mobile' => ['required', 'digits:10 ',
                    'unique:branch_details,mobile'],
                'email' => 'email|nullable',
            );

            $validateForContact = Validator::make($arr, $rules, $messsages);

            if ($validateForContact->fails()) {

                $error['contact_details'][$key] = $validateForContact->getMessageBag();
            } else {

                $contacts = new BranchDetail();
                $contacts->branch_id = $branch_id;
                $contacts->name = $val['contactname'];
                $contacts->mobile = $val['mobile'];
                $contacts->email = $val['email'];

                $contacts->save();
            }
        }
        if (count($error) > 0) {
            DB::rollback();
            return response()->json($error, 500);

        } else {
            DB::commit();
            return response()->json("success", 200);
        }

    }


    public function loadModal($id, $action)
    {

        $branch = Branch::where('id',$id)->get();
        $contactDetails = BranchDetail::where('branch_id',$id)->get();


        return view('branch.modal-view', compact('branch','contactDetails'));

    }

    public function edit(Branch $branch)
    {
        $branchDetails = BranchDetail::where('branch_id', $branch->id)->get();
        return view('branch.editBranch', compact('branch', 'branchDetails'));
    }

    public function update(Branch $branch)
    {


        $arr = [];
        $error = [];


        DB::beginTransaction();

        $validate = Validator::make(request()->all(), [
            'name' => 'required',
            'address' => 'required',
            'contact_no' => ['required', 'digits:10 ',
                Rule::unique('branches')->ignore(request()->id)
            ],
        ]);

        if ($validate->fails()) {

            $error['branch_details'] = $validate->getMessageBag();

        } else {

            Branch::where('id',request()->id)->update(['name' => request()->name, 'address' => request()->address, 'contact_no' => request()->contact_no, 'city_id' => request()->city_id]);

        }

        $contactDetails = request()->contactDetail;
        BranchDetail::where('branch_id', request()->id)->delete();

        foreach ($contactDetails as $key => $detail) {


            $arr['contactname'] = $detail['contactname'];
            $arr['mobile'] = $detail['mobile'];
            $arr['email'] = $detail['email'];

            $messsages = array(
                'contactname.required' => 'The name field is required',
                'contactname.unique' => 'The name is already taken',
            );

            $rules = array(
                'contactname' => ['required '],
                'mobile' => ['required', 'digits:10 ', 'unique:branch_details,mobile'],
                'email' => 'email|nullable',
            );

            $validateForContact = Validator::make($arr, $rules, $messsages);

            if ($validateForContact->fails()) {

                $error['contact_details'][$key] = $validateForContact->getMessageBag();

            } else {


                $contacts = new BranchDetail();
                $contacts->branch_id = request()->id;
                $contacts->name = $detail['contactname'];
                $contacts->mobile = $detail['mobile'];
                $contacts->email = $detail['email'];

                $contacts->save();
            }

        }

        if (count($error) > 0) {
            DB::rollback();
            return response()->json($error, 500);

        } else {
            DB::commit();
            return response()->json("success", 200);
        }


        return redirect('/branches');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Branch $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branch $branch)
    {
        $branch->delete();

        $details = BranchDetail::where('branch_id', $branch->id)->get();

        foreach ($details as $detail) {
            $detail->delete();
        }

        return redirect('/branches')->with('data', ['type' => 'error', 'message' => 'Deleted Successfully']);
    }
}
