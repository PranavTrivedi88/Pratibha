<?php

namespace App\Http\Controllers;

use App\CountryProgram;
use App\InquiryAcademic;
use App\InquiryCounsellingSummary;
use App\InquiryDependentAcademic;
use App\InquiryDependentChildren;
use App\InquiryDependentDetail;
use App\InquiryDependentExam;
use App\InquiryDependentTravelHistory;
use App\InquiryDependentUser;
use App\InquiryDependentWorkExperience;
use App\InquiryExam;
use App\InquiryTravelHistory;
use App\InquiryUserHistory;
use App\InquiryWiseUser;
use App\InquiryWorkExperience;
use App\VisaInquiry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Area;
use App\FollowUp;
use App\Inquiry;
use App\User;
use App\Reason;
use Illuminate\Support\Facades\Response;
use DB;

class VisaInquiryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {

//        if (Session::has('user_id')) {
//            $userId = Session::get('user_id');
//        } else {
//            $userId = Auth::id();
//        }

        $userId = User::where('is_admin', 1)->pluck('id')->toArray();

        if ($userId[0] == 1) {

            $inquiries = VisaInquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->leftjoin('inquiry_wise_users', function ($join) use ($userId) {
                $join->whereRaw("visa_inquiries.id = inquiry_wise_users.inquiry_id AND inquiry_wise_users.id IN (SELECT max(id) as id FROM inquiry_wise_users WHERE inquiry_id = visa_inquiries.id)");
            })->select('visa_inquiries.*', 'reasons.reason AS newReason', 'reasons.assignor_id', 'inquiry_wise_users.id AS inquiry_wise_user_id')->orderBy('status', 'asc')->orderBy('visa_inquiries.updated_at', 'desc')->get();

        } else {
            $inquiries = VisaInquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->leftjoin('inquiry_wise_users', function ($join) use ($userId) {
                $join->whereRaw("visa_inquiries.id = inquiry_wise_users.inquiry_id AND inquiry_wise_users.id IN (SELECT max(id) as id FROM inquiry_wise_users WHERE inquiry_id = visa_inquiries.id)");
            })->where('inquiry_wise_users.user_id', '=', $userId)->select('visa_inquiries.*', 'reasons.reason AS newReason', 'reasons.assignor_id', 'inquiry_wise_users.id AS inquiry_wise_user_id')->orderBy('status', 'asc')->orderBy('visa_inquiries.updated_at', 'desc')->get();

        }
        return view('inquiry.inquiries', compact('inquiries', 'userId'));

    }

    public function create()
    {

        $inquiry = VisaInquiry::orderBy('id', 'Desc')->first();


        return view('inquiry.createInquiry', compact('inquiry'));

    }

    public function store(Request $request)
    {
        $error = "";
        $userInquiry = "";
        $validate = array(
            'name' => 'required',
            'mobile' => 'required|unique:visa_inquiries|digits:10',
            'course_id' => 'required',
            'inquiryCountry' => 'required',
            'address' => 'required',
            'alternate_mobile' => 'numeric | nullable',
            'email' => 'email | nullable');

        $messsages = array(
            'course_id.required' => 'The program field is required',
            'inquiryCountry.required' => 'The country field is required',
            'mobile.unique' => 'The mobile number is already taken',
            'name.required' => 'The name field is required',
            'alternate_mobile.unique' => 'The alternate number is already taken',
        );

        $validateForPersonalInfo = Validator::make($request->all(), $validate, $messsages);

        if ($validateForPersonalInfo->fails()) {

            $error = $validateForPersonalInfo->getMessageBag();
        } else {
            $inquiry = new VisaInquiry();
            $inquiry->name = $request->name;
            if ($request->birth_date != null) {
                $inquiry->birth_date = date('Y-m-d', strtotime($request->birth_date));
            } else {
                $inquiry->birth_date = null;
            }
            $inquiry->gender = $request->gender;
            $inquiry->mobile = $request->mobile;
            $inquiry->alternate_mobile = $request->alternate_mobile;
            $inquiry->email = $request->email;
            $inquiry->address = $request->address;
            $inquiry->city_id = $request->city;
            $inquiry->area_id = $request->area;
            $inquiry->marital_status = $request->m_status;
            $inquiry->source_id = $request->source;
            $inquiry->friend_name = $request->friendource;
            $inquiry->tag = $request->tagfriend;
            $inquiry->other_source = $request->otherSource;
            $inquiry->branch_id = $request->branch_id;
            $inquiry->remark = $request->remarks;
            $inquiry->save();

            if (isset($request->program)) {
                for ($i = 0; $i < sizeof($request->program); $i++) {
                    $program = new CountryProgram();
                    $program->inquiry_id = $inquiry->id;
                    $program->country_id = $request->country[$i];
                    $program->program_id = $request->program[$i];
                    $program->save();
                }
            } else {
                $program = new CountryProgram();
                $program->inquiry_id = $inquiry->id;
                $program->country_id = $request->inquiryCountry;
                $program->program_id = $request->course_id;
                $program->save();
            }

//            for ($i = 0; $i < sizeof($request->user_id); $i++) {
            $user = new InquiryWiseUser();
            $user->inquiry_id = $inquiry->id;
            $user->user_id = $request->user_id[0];
            $user->save();
//            }
            $userInquiry = InquiryWiseUser::with('userInquiry')->where('inquiry_id', $inquiry->id)->first();

        }
        if ($error != '') {
            return response()->json($error, 500);

        } else {

            return response()->json(array('result' => "success", 'inquiry_id' => $userInquiry->id), 200);
        }


    }

    public function setAcademicInformation(Request $request)
    {


        $academics = $request->academic;
        $inquiry = $request->inquiryId;

        foreach ($academics as $key => $value) {

            if ($value['exam_passed'] == null && $value['institute'] == null && $value['year'] == null && $value['board'] == null && $value['result'] == null) {
                unset($academics[$key]);
            } else {
                $academic = new InquiryAcademic();
                $academic->inquiry_id = $inquiry;
                $academic->exam_passed = $value['exam_passed'];
                $academic->institute = $value['institute'];
                $academic->year = $value['year'];
                $academic->board = $value['board'];
                $academic->result = $value['result'];
                $academic->save();
            }

        }
        return "success";
    }

    public function setExamInformation(Request $request)
    {

        $exams = $request->exam;
        $inquiry = $request->inquiryId;
        $remark = $request->examRemark;


        if ($inquiry == null) {
            return "error";
        } else {

            foreach ($exams as $key => $value) {

                if ($value['exam_type'] != null && isset($value['listening']) == null && isset($value['reading']) == null && isset($value['writing']) == null && isset($value['speaking']) == null && isset($value['overall']) == null && isset($value['year']) == null) {
                    unset($exams[$key]);
                } else {

                    $exam = new InquiryExam();

                    $exam->inquiry_id = $inquiry;
                    $exam->exam_type = $value['exam_type'];
                    if (isset($value['listening'])) {
                        $exam->listening = $value['listening'];
                    }
                    if (isset($value['reading'])) {
                        $exam->reading = $value['reading'];
                    }
                    if (isset($value['writing'])) {
                        $exam->writing = $value['writing'];
                    }
                    if (isset($value['speaking'])) {
                        $exam->speaking = $value['speaking'];
                    }
                    if (isset($value['overall'])) {
                        $exam->overall = $value['overall'];
                    }
                    if (isset($value['year'])) {
                        $exam->year = $value['year'];
                    }
                    if (isset($remark)) {
                        $exam->remark = $remark;
                    }
                    $exam->save();
                }

            }

            return "success";

        }
    }

    public function setWorkExperienceInformation(Request $request)
    {

        $workExperiences = $request->work;
        $inquiry = $request->inquiryId;

        if ($inquiry == null) {
            return "error";
        } else {
            foreach ($workExperiences as $key => $value) {

                if ($value['company'] == null && $value['designation'] == null && $value['duration'] == null && $value['location'] == null && $value['remark'] == null) {
                    unset($workExperiences[$key]);
                } else {
                    $workExperience = new InquiryWorkExperience();

                    $workExperience->inquiry_id = $inquiry;
                    $workExperience->company = $value['company'];
                    $workExperience->designation = $value['designation'];
                    $workExperience->duration = $value['duration'];
                    $workExperience->location = $value['location'];
                    $workExperience->remark = $value['remark'];
                    $workExperience->save();
                }

            }
            return "success";

        }

    }

    public function setTravelHistoryInformation(Request $request)
    {

        $travelHistory = $request->travel;
        $inquiry = $request->inquiryId;


        if ($inquiry == null) {
            return "error";
        } else {

            if (isset($travelHistory) && $request->country == null && $request->program == null && $request->status == null && $request->year == null && $request->yearDuration == null && $request->remarks == null) {
                return response()->json("noData", 500);
            } else {
                $travelHistory = new InquiryTravelHistory();
                $travelHistory->inquiry_id = $inquiry;
                $travelHistory->country = $request->country;
                $travelHistory->program = $request->program;
                $travelHistory->status = $request->status;
                $travelHistory->year = $request->year;
                $travelHistory->duration = $request->yearDuration;
                $travelHistory->remark = $request->remarks;
                $travelHistory->save();


                foreach ($travelHistory as $key => $value) {

                    if ($value['country'] == null && $value['program'] == null && $value['status'] == null && $value['year'] == null && $value['duration'] == null && $value['remark'] == null) {
                        unset($travelHistory[$key]);
                    } else {

                        if (isset($request->travel)) {

                            $travel = new InquiryTravelHistory();
                            $travel->inquiry_id = $inquiry;
                            $travel->country = $value['country'];
                            $travel->program = $value['program'];
                            $travel->status = $value['status'];
                            $travel->year = $value['year'];
                            $travel->duration = $value['duration'];
                            $travel->remark = $value['remark'];
                            $travel->save();

                        } else {

                            $travel = new InquiryTravelHistory();
                            $travel->inquiry_id = $inquiry;
                            $travel->country = $request->country;
                            $travel->program = $request->program;
                            $travel->status = $request->status;
                            $travel->year = $request->year;
                            $travel->duration = $request->yearDuration;
                            $travel->remark = $request->remarks;
                            $travel->save();

                        }
                    }
                }
            }

            return "success";
        }

    }

    public function setCounsellingSummaryInformation(Request $request)
    {
        $summary = new InquiryCounsellingSummary();
        $summary->inquiry_id = $request->inquiryId;
        $summary->country_id = $request->country_id;
        $summary->program_id = $request->program_id;
        $summary->suggestion = $request->suggestion;
        $summary->remark = $request->remark;
        $summary->save();

        return "success";

    }

    public function setDependentInformation(Request $request)
    {
        $error = "";

        $validate = array(
            'mobile' => 'unique:inquiry_dependent_details|digits:10 | nullable',
            'email' => 'email | nullable');

        $messsages = array(
            'mobile.digits' => 'The mobile number must be of 10 digits',
            'mobile.unique' => 'The mobile number is already taken',
        );

        $validateForDependentInfo = Validator::make($request->all(), $validate, $messsages);

        if ($validateForDependentInfo->fails()) {

            $error = $validateForDependentInfo->getMessageBag();

        } else {

            $dependent = new InquiryDependentDetail();
            $dependent->inquiry_id = $request->dependentInquiryId;
            $dependent->name = $request->dependentName;
            if ($request->birth_date != null) {
                $dependent->birth_date = date('Y-m-d', strtotime($request->dependentBirthDate));
            } else {
                $dependent->birth_date = null;
            }
//            $dependent->birth_date = date('Y-m-d', strtotime($request->dependentBirthDate));
            $dependent->mobile = $request->mobile;
            $dependent->email = $request->email;
            $dependent->children = $request->children;
            $dependent->save();


            $userId = InquiryWiseUser::where('id', $dependent->inquiry_id)->first();

            $user = new InquiryDependentUser();
            $user->dependent_id = $dependent->id;
            $user->inquiry_id = $dependent->inquiry_id;
            $user->user_id = $userId->user_id;
            $user->save();

            if (isset($request->child)) {
                for ($i = 0; $i < sizeof($request->child); $i++) {
                    $child = new InquiryDependentChildren();
                    $child->dependent_id = $dependent->id;
                    $child->name = $request->child[$i]['child_name'];
                    $child->age_in_year = $request->child[$i]['age_yr'];
                    $child->age_in_month = $request->child[$i]['age_mnth'];
                    if ($request->birth_date != null) {
                        $child->birth_date = date('Y-m-d', strtotime($request->child[$i]['birth_date']));
                    } else {
                        $child->birth_date = null;
                    }
//                    $child->birth_date = date('Y-m-d', strtotime($request->child[$i]['birth_date']));
                    $child->save();
                }
            }

        }

        $dependentDetail = InquiryDependentUser::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        $dependentId = $dependentDetail->id;

        if ($error != '') {
            return response()->json($error, 500);

        } else {
            return response()->json(array('result' => "success", 'dependent_id' => $dependentId), 200);
        }

    }

    public function setDependentAcademicInformation(Request $request)
    {

        $academics = $request->dependent_academic;
        $dependentId = $request->dependentInquiryId;

        foreach ($academics as $key => $value) {

            if ($value['exam_passed'] == null && $value['institute'] == null && $value['year'] == null && $value['board'] == null && $value['result'] == null) {
                unset($academics[$key]);
            } else {
                $academic = new InquiryDependentAcademic();

                $academic->dependent_id = $dependentId;
                $academic->exam_passed = $value['exam_passed'];
                $academic->institute = $value['institute'];
                $academic->year = $value['year'];
                $academic->board = $value['board'];
                $academic->result = $value['result'];
                $academic->save();
            }

        }
        return "success";
    }

    public function setDependentExamInformation(Request $request)
    {

        $exams = $request->dependent_exam;
        $dependentId = $request->dependentInquiryId;
        $remark = $request->examRemark;


        if ($dependentId == null) {
            return "error";
        } else {

            foreach ($exams as $key => $value) {

                if ($value['exam_type'] != null && isset($value['listening']) == null && isset($value['reading']) == null && isset($value['writing']) == null && isset($value['speaking']) == null && isset($value['overall']) == null && isset($value['year']) == null) {
                    unset($exams[$key]);
                } else {

                    $exam = new InquiryDependentExam();

                    $exam->dependent_id = $dependentId;
                    $exam->exam_type = $value['exam_type'];
                    if (isset($value['listening'])) {
                        $exam->listening = $value['listening'];
                    }
                    if (isset($value['reading'])) {
                        $exam->reading = $value['reading'];
                    }
                    if (isset($value['writing'])) {
                        $exam->writing = $value['writing'];
                    }
                    if (isset($value['speaking'])) {
                        $exam->speaking = $value['speaking'];
                    }
                    if (isset($value['overall'])) {
                        $exam->overall = $value['overall'];
                    }
                    if (isset($value['year'])) {
                        $exam->year = $value['year'];
                    }
                    if (isset($remark)) {
                        $exam->remark = $remark;
                    }
                    $exam->save();
                }

            }
            return "success";
        }
    }

    public function setDependentWorkExperienceInformation(Request $request)
    {
        $workExperiences = $request->work;
        $dependentId = $request->dependentInquiryId;

        if ($dependentId == null) {
            return "error";
        } else {
            foreach ($workExperiences as $key => $value) {

                if ($value['company'] == null && $value['designation'] == null && $value['duration'] == null && $value['location'] == null && $value['remark'] == null) {
                    unset($workExperiences[$key]);
                } else {
                    $workExperience = new InquiryDependentWorkExperience();

                    $workExperience->dependent_id = $dependentId;
                    $workExperience->company = $value['company'];
                    $workExperience->designation = $value['designation'];
                    $workExperience->duration = $value['duration'];
                    $workExperience->location = $value['location'];
                    $workExperience->remark = $value['remark'];
                    $workExperience->save();
                }

            }
            return "success";

        }

    }

    public function setDependentTravelHistoryInformation(Request $request)
    {
        $travelHistory = $request->travel;
        $dependent_id = $request->dependentInquiryId;


        if ($dependent_id == null) {
            return "error";
        } else {

            if (isset($travelHistory) && $request->country == null && $request->program == null && $request->status == null && $request->year == null && $request->dependentYearDuration == null && $request->dependent_remarks == null) {
                return response()->json("noData", 500);
            } else {
                $travelHistory = new InquiryDependentTravelHistory();
                $travelHistory->dependent_id = $dependent_id;
                $travelHistory->country = $request->country;
                $travelHistory->program = $request->program;
                $travelHistory->status = $request->status;
                $travelHistory->year = $request->year;
                $travelHistory->duration = $request->dependentYearDuration;
                $travelHistory->remark = $request->dependent_remarks;
                $travelHistory->save();
            }

            foreach ($travelHistory as $key => $value) {

                if ($value['country'] == null && $value['program'] == null && $value['status'] == null && $value['year'] == null && $value['duration'] == null && $value['remark'] == null) {
                    unset($travelHistory[$key]);
                } else {

                    if (isset($request->travel)) {

                        $travel = new InquiryDependentTravelHistory();
                        $travel->dependent_id = $dependent_id;
                        $travel->country = $value['country'];
                        $travel->program = $value['program'];
                        $travel->status = $value['status'];
                        $travel->year = $value['year'];
                        $travel->duration = $value['duration'];
                        $travel->remark = $value['remark'];
                        $travel->save();

                    } else {

                        $travel = new InquiryDependentTravelHistory();
                        $travel->dependent_id = $dependent_id;
                        $travel->country = $request->country;
                        $travel->program = $request->program;
                        $travel->status = $request->status;
                        $travel->year = $request->year;
                        $travel->duration = $request->dependentYearDuration;
                        $travel->remark = $request->dependent_remarks;
                        $travel->save();

                    }
                }
            }

            return "success";
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(VisaInquiry $inquiry)
    {
        $users = InquiryWiseUser::where('inquiry_id', '=', $inquiry->id)->first();

        $dependentUserId = InquiryDependentUser::where('inquiry_id', $users->id)->orderBy('id', 'DESC')->first();

        $academics = InquiryAcademic::where('inquiry_id', $users->id)->get();

        $exams = InquiryExam::where('inquiry_id', $users->id)->get();
        $examData = [];

        foreach ($exams as $exam) {
            $examData[$exam->exam_type] = $exam;
        }

        $workExperiences = InquiryWorkExperience::where('inquiry_id', $users->id)->get();

        Log::debug($workExperiences);

        $travelHistory = InquiryTravelHistory::where('inquiry_id', $users->id)->get();

        Log::debug($travelHistory);

        $counsellingSummary = InquiryCounsellingSummary::where('inquiry_id', $users->id)->get();

        $dependentDetail = InquiryDependentDetail::where('inquiry_id', $users->id)->first();

        if (isset($dependentDetail)) {
            $dependentChild = InquiryDependentChildren::where('dependent_id', $dependentDetail->id)->get();
        } else {
            $dependentChild = '';
        }

        Log::debug($dependentChild);

        $dependentExam = array();

        if (isset($dependentDetail)) {

            $dependentExam = InquiryDependentExam::where('dependent_id', $dependentUserId->id)->get();
            $dependentExamData = [];

            foreach ($dependentExam as $exam) {
                $dependentExamData[$exam->exam_type] = $exam;
            }

        }

        $dependentAcademic = array();

        if (isset($dependentDetail)) {
            $dependentAcademic = InquiryDependentAcademic::where('dependent_id', $dependentUserId->id)->get();
        }

        Log::debug($dependentAcademic);

        $dependentWork = array();
        if (isset($dependentDetail)) {
            $dependentWork = InquiryDependentWorkExperience::where('dependent_id', $dependentUserId->id)->get();

        }

        Log::debug($dependentWork);

        $dependentTravel = array();
        if (isset($dependentDetail)) {
            $dependentTravel = InquiryDependentTravelHistory::where('dependent_id', $dependentUserId->id)->get();

        }

        Log::debug($dependentTravel);

        return view('inquiry.edit.editInquiry', compact(['inquiry', 'dependentUserId', 'users', 'academics', 'exams', 'examData', 'workExperiences', 'travelHistory', 'counsellingSummary', 'dependentDetail', 'dependentChild', 'dependentExam', 'dependentAcademic', 'dependentWork', 'dependentTravel', 'dependentExamData']));
    }


    public function update(VisaInquiry $visa_inquiry)
    {
        $error = "";
        $validate = array(
            'name' => 'required',
            'mobile' => ['required', 'digits:10',
                Rule::unique('visa_inquiries')->ignore($visa_inquiry->id)
            ],
            'address' => 'required',
            'alternate_mobile' => 'numeric | nullable',
            'email' => 'email | nullable');

        $messsages = array(
            'mobile.digits' => 'The mobile number must be of 10 digits',
            'mobile.unique' => 'The mobile number is already taken',
            'name.required' => 'The name field is required',
            'alternate_mobile.unique' => 'The alternate number is already taken',
        );

        $validateForPersonalInfo = Validator::make(request()->all(), $validate, $messsages);

        if ($validateForPersonalInfo->fails()) {

            $error = $validateForPersonalInfo->getMessageBag();
        } else {

            $visa_inquiry->id = request()->inquiry_id;
            $visa_inquiry->name = request()->name;
            if (request()->birth_date != null) {
                $visa_inquiry->birth_date = date('Y-m-d', strtotime(request()->birth_date));
            } else {
                $visa_inquiry->birth_date = null;
            }
            $visa_inquiry->gender = request()->gender;
            $visa_inquiry->mobile = request()->mobile;
            $visa_inquiry->alternate_mobile = request()->alternate_mobile;
            $visa_inquiry->email = request()->email;
            $visa_inquiry->address = request()->address;
            $visa_inquiry->city_id = request()->city;
            $visa_inquiry->area_id = request()->area;
            $visa_inquiry->marital_status = request()->m_status;
            $visa_inquiry->source_id = request()->source;
            $visa_inquiry->friend_name = request()->friendource;
            $visa_inquiry->tag = request()->tagfriend;
            $visa_inquiry->other_source = request()->otherSource;
            $visa_inquiry->branch_id = request()->branch_id;
            $visa_inquiry->remark = request()->remarks;
            $visa_inquiry->status = request()->status;
            $visa_inquiry->save();

        }
        CountryProgram::where('inquiry_id', $visa_inquiry->id)->delete();

        if (isset(request()->program)) {

            for ($i = 0; $i < sizeof(request()->program); $i++) {
                $program = new CountryProgram();
                $program->inquiry_id = $visa_inquiry->id;
                $program->country_id = request()->country[$i];
                $program->program_id = request()->program[$i];
                $program->save();
            }
        } else {
            $program = new CountryProgram();
            $program->inquiry_id = $visa_inquiry->id;
            $program->country_id = request()->inquiryCountry;
            $program->program_id = request()->course_id;
            $program->save();
        }

        $userWiseId = InquiryWiseUser::where('inquiry_id', request()->inquiry_id)->first();

        if ($userWiseId->user_id != request()->user_id) {

            $user = new InquiryUserHistory();
            $user->inquiry_id = $userWiseId->id;
            $user->user_id = $userWiseId->user_id;
            $user->save();

            InquiryWiseUser::where('inquiry_id', request()->inquiry_id)->update(['inquiry_id' => $userWiseId->inquiry_id, 'user_id' => request()->user_id]);

        }


        if (!empty(request()->reason)) {

            if (!empty(request()->reason)) {
                $reason = request()->reason;
            } else {
                $reason = "Reason not available.";
            }

            Reason::create([
                'assignor_id' => Auth::id(),
                'inquiry_id' => $visa_inquiry->id,
                'reason' => $reason
            ]);

        }

        if ($error != '') {
            return response()->json($error, 500);

        } else {

            return response()->json(array('result' => "updated", 'inquiry_id' => $visa_inquiry->id), 200);

        }

    }

    public function updateInquiryAcademic(Request $request, $id)
    {

        DB::beginTransaction();

        $academicRecords = request()->academic;

        InquiryAcademic::where('inquiry_id', $id)->delete();


        foreach ($academicRecords as $key => $academic) {


            $arr['exam_passed'] = $academic['exam_passed'];
            $arr['institute'] = $academic['institute'];
            $arr['year'] = $academic['year'];
            $arr['board'] = $academic['board'];
            $arr['result'] = $academic['result'];

            $messsages = array(
                'year.numeric' => 'The year field should be numeric',
            );

            $rules = array(
                'year' => ['numeric '],
            );

            $validateForAcademic = Validator::make($arr, $rules, $messsages);

            if ($validateForAcademic->fails()) {

                $error['academic'][$key] = $validateForAcademic->getMessageBag();

            } else {

                $academicInfo = new InquiryAcademic();

                $academicInfo->inquiry_id = $id;
                $academicInfo->exam_passed = $academic['exam_passed'];
                $academicInfo->institute = $academic['institute'];
                $academicInfo->year = $academic['year'];
                $academicInfo->board = $academic['board'];
                $academicInfo->result = $academic['result'];
                $academicInfo->save();

            }

        }

        DB::commit();
        return response()->json("updated", 200);
    }

    public function updateInquiryWorkExperience($id)
    {

//        $arr = [];
//        $error = [];


        DB::beginTransaction();

        $workRecords = request()->work;

        InquiryWorkExperience::where('inquiry_id', $id)->delete();


        foreach ($workRecords as $key => $work) {


            $arr['company'] = $work['company'];
            $arr['designation'] = $work['designation'];
            $arr['duration'] = $work['duration'];
            $arr['location'] = $work['location'];
            $arr['remark'] = $work['remark'];


//            $messsages = array(
//                'duration.numeric' => 'The duration field should be numeric',
//            );
//
//            $rules = array(
//                'duration' => ['numeric '],
//            );

//            $validateForWork = Validator::make($arr, $rules, $messsages);
//
//            if ($validateForWork->fails()) {
//
//                $error['work'][$key] = $validateForWork->getMessageBag();
//
//            } else {

            $workExperience = new InquiryWorkExperience();

            $workExperience->inquiry_id = $id;
            $workExperience->company = $work['company'];
            $workExperience->designation = $work['designation'];
            $workExperience->duration = $work['duration'];
            $workExperience->location = $work['location'];
            $workExperience->remark = $work['remark'];
            $workExperience->save();

        }

//        }

//        if (count($error) > 0) {
////            DB::rollback();
////            return response()->json($error, 500);
//            DB::commit();
//            return response()->json("updated", 200);
//
//        } else {
        DB::commit();
        return response()->json("updated", 200);
//        }
    }

    public function updateInquiryTravelHistory($id, Request $request)
    {
        $travelRecords = $request->all();

        InquiryTravelHistory::where('inquiry_id', $id)->delete();


        if ($id == null) {
            return "error";
        } else {

            if (isset($travelRecords) && $request->country == null && $request->program == null && $request->status == null && $request->year == null && $request->yearDuration == null && $request->remarks == null) {
                return response()->json("noData", 500);
            } else {
                $travelHistory = new InquiryTravelHistory();
                $travelHistory->inquiry_id = $id;
                $travelHistory->country = $request->country;
                $travelHistory->program = $request->program;
                $travelHistory->status = $request->status;
                $travelHistory->year = $request->year;
                $travelHistory->duration = $request->yearDuration;
                $travelHistory->remark = $request->remark;
                $travelHistory->save();
            }
            if (isset($request->travel)) {
                foreach ($request->travel as $key => $value) {

                    if ($value['country'] == null && $value['program'] == null && $value['status'] == null && $value['year'] == null && $value['duration'] == null && $value['remark'] == null) {
                        unset($travelRecords[$key]);
                    } else {

                        if (isset($request->travel)) {

                            $travel = new InquiryTravelHistory();
                            $travel->inquiry_id = $id;
                            $travel->country = $value['country'];
                            $travel->program = $value['program'];
                            $travel->status = isset($value['status']) ? $value['status'] : '';
                            $travel->year = $value['year'];
                            $travel->duration = $value['duration'];
                            $travel->remark = $value['remark'];
                            $travel->save();

                        } else {

                            $travel = new InquiryTravelHistory();
                            $travel->inquiry_id = $id;
                            $travel->country = $request->country;
                            $travel->program = $request->program;
                            $travel->status = $request->status;
                            $travel->year = $request->year;
                            $travel->duration = $request->yearDuration;
                            $travel->remark = $request->remarks;
                            $travel->save();

                        }
                    }
                }

            }
        }
        return "updated";

    }

    public function updateInquiryCounsellingSummary(Request $request, $id)
    {

        InquiryCounsellingSummary::where('inquiry_id', $id)->delete();

        $summary = new InquiryCounsellingSummary();
        $summary->inquiry_id = $id;
        $summary->country_id = $request->country_id;
        $summary->program_id = $request->program_id;
        $summary->suggestion = $request->suggestion;
        $summary->remark = $request->remark;
        $summary->save();

        return "updated";

    }

    public function updateInquiryExam($id, Request $request)
    {

        InquiryExam::where('inquiry_id', $id)->delete();

        $exams = $request->exam;
        $inquiry = $id;
        $remark = $request->examRemark;


        if ($inquiry == null) {
            return "error";
        } else {

            foreach ($exams as $key => $value) {

                if ($value['exam_type'] != null && isset($value['listening']) == null && isset($value['reading']) == null && isset($value['writing']) == null && isset($value['speaking']) == null && isset($value['overall']) == null && isset($value['year']) == null) {
                    unset($exams[$key]);
                } else {

                    $exam = new InquiryExam();

                    $exam->inquiry_id = $inquiry;
                    $exam->exam_type = $value['exam_type'];
                    if (isset($value['listening'])) {
                        $exam->listening = $value['listening'];
                    }
                    if (isset($value['reading'])) {
                        $exam->reading = $value['reading'];
                    }
                    if (isset($value['writing'])) {
                        $exam->writing = $value['writing'];
                    }
                    if (isset($value['speaking'])) {
                        $exam->speaking = $value['speaking'];
                    }
                    if (isset($value['overall'])) {
                        $exam->overall = $value['overall'];
                    }
                    if (isset($value['year'])) {
                        $exam->year = $value['year'];
                    }
                    if (isset($remark)) {
                        $exam->remark = $remark;
                    }
                    $exam->save();
                }

            }

            return "updated";

        }

    }

    public function updateDependentDetail($id, Request $request)
    {

        $dependentDetails = InquiryDependentDetail::all();

        if (count($dependentDetails) > 0) {

            foreach ($dependentDetails as $detail) {
                if ($detail->inquiry_id == $id) {
                    InquiryDependentDetail::where('inquiry_id', $id)->update(['name' => $request->dependentName, 'birth_date' => date('Y-m-d', strtotime($request->dependentBirthDate)), 'mobile' => $request->mobile, 'email' => $request->email, 'children' => $request->children]);

                    $userId = InquiryWiseUser::where('id', $detail->inquiry_id)->first();

                    $user = new InquiryDependentUser();
                    $user->dependent_id = $detail->id;
                    $user->inquiry_id = $detail->inquiry_id;
                    $user->user_id = $userId->user_id;
                    $user->save();


                    InquiryDependentChildren::where('dependent_id', $detail->id)->delete();


                    if (isset($request->child_name)) {

                        for ($i = 0; $i < count($request->child_name); $i++) {
                            $child = new InquiryDependentChildren();
                            $child->dependent_id = $detail->id;
                            $child->name = $request->child_name[$i];
                            $child->age_in_year = $request->age_yr[$i];
                            $child->age_in_month = $request->age_mnth[$i];
                            if ($request->birth_date != null) {
                                $child->birth_date = date('Y-m-d', strtotime($request->birth_date[$i]));
                            } else {
                                $child->birth_date = null;
                            }
                            $child->save();
                        }
                    }
                }
            }
        } else {
            $error = "";

            $validate = array(
                'mobile' => ['digits:10', 'nullable',
                    Rule::unique('inquiry_dependent_details')->ignore($id)
                ],
                'email' => 'email | nullable');

            $messsages = array(
                'mobile.digits' => 'The mobile number must be of 10 digits',
                'mobile.unique' => 'The mobile number is already taken',
            );

            $validateForDependentInfo = Validator::make($request->all(), $validate, $messsages);

            if ($validateForDependentInfo->fails()) {

                $error = $validateForDependentInfo->getMessageBag();

            } else {
                $dependent = new InquiryDependentDetail();
                $dependent->inquiry_id = $request->inquiryFormId;
                $dependent->name = $request->dependentName;
                if ($request->birth_date != null) {
                    $dependent->birth_date = date('Y-m-d', strtotime($request->dependentBirthDate));
                } else {
                    $dependent->birth_date = null;
                }
                $dependent->mobile = $request->mobile;
                $dependent->email = $request->email;
                $dependent->children = $request->children;
                $dependent->save();


                $userId = InquiryWiseUser::where('id', $dependent->inquiry_id)->first();

                $user = new InquiryDependentUser();
                $user->dependent_id = $dependent->id;
                $user->inquiry_id = $dependent->inquiry_id;
                $user->user_id = $userId->user_id;
                $user->save();

                if (isset($request->child)) {
                    for ($i = 0; $i < sizeof($request->child); $i++) {
                        $child = new InquiryDependentChildren();
                        $child->dependent_id = $dependent->id;
                        $child->name = $request->child[$i]['child_name'];
                        $child->age_in_year = $request->child[$i]['age_yr'];
                        $child->age_in_month = $request->child[$i]['age_mnth'];
                        if (isset($request->birth_date)) {
                            $child->birth_date = date('Y-m-d', strtotime($request->child[$i]['birth_date']));
                        } else {
                            $child->birth_date = null;
                        }
                        $child->save();
                    }
                }
            }
        }
        return "updated";
    }

    public function updateDependentAcademic($id, Request $request)
    {

        InquiryDependentAcademic::where('dependent_id', $id)->delete();


        $academics = $request->dependent_academic;
        $dependentId = $id;

        foreach ($academics as $key => $value) {

            if ($value['exam_passed'] == null && $value['institute'] == null && $value['year'] == null && $value['board'] == null && $value['result'] == null) {
                unset($academics[$key]);
            } else {
                $academic = new InquiryDependentAcademic();

                $academic->dependent_id = $dependentId;
                $academic->exam_passed = $value['exam_passed'];
                $academic->institute = $value['institute'];
                $academic->year = $value['year'];
                $academic->board = $value['board'];
                $academic->result = $value['result'];
                $academic->save();
            }

        }
        return "updated";

    }

    public
    function updateDependentExam($id, Request $request)
    {

        InquiryDependentExam::where('dependent_id', $id)->delete();

        $exams = $request->exam;
        $dependentId = $id;
        $remark = $request->examRemark;


        if ($dependentId == null) {
            return "error";
        } else {

            foreach ($exams as $key => $value) {

                if ($value['exam_type'] != null && isset($value['listening']) == null && isset($value['reading']) == null && isset($value['writing']) == null && isset($value['speaking']) == null && isset($value['overall']) == null && isset($value['year']) == null) {
                    unset($exams[$key]);
                } else {

                    $exam = new InquiryDependentExam();

                    $exam->dependent_id = $dependentId;
                    $exam->exam_type = $value['exam_type'];
                    if (isset($value['listening'])) {
                        $exam->listening = $value['listening'];
                    }
                    if (isset($value['reading'])) {
                        $exam->reading = $value['reading'];
                    }
                    if (isset($value['writing'])) {
                        $exam->writing = $value['writing'];
                    }
                    if (isset($value['speaking'])) {
                        $exam->speaking = $value['speaking'];
                    }
                    if (isset($value['overall'])) {
                        $exam->overall = $value['overall'];
                    }
                    if (isset($value['year'])) {
                        $exam->year = $value['year'];
                    }
                    if (isset($remark)) {
                        $exam->remark = $remark;
                    }
                    $exam->save();
                }

            }
            return "updated";
        }

    }

    public function updateDependentWorkExperience($id, Request $request)
    {

        InquiryDependentWorkExperience::where('dependent_id', $id)->delete();

        $workExperiences = $request->work;

        $dependentId = $id;

        if ($dependentId == null) {
            return "error";
        } else {

            foreach ($workExperiences as $key => $value) {

                if ($value['company'] == null && $value['designation'] == null && $value['duration'] == null && $value['location'] == null && $value['remark'] == null) {
                    unset($workExperiences[$key]);
                } else {
                    $workExperience = new InquiryDependentWorkExperience();

                    $workExperience->dependent_id = $dependentId;
                    $workExperience->company = $value['company'];
                    $workExperience->designation = $value['designation'];
                    $workExperience->duration = $value['duration'];
                    $workExperience->location = $value['location'];
                    $workExperience->remark = $value['remark'];
                    $workExperience->save();
                }

            }
            return "updated";

        }

    }

    public function updateDependentTravel($id, Request $request)
    {

        InquiryDependentTravelHistory::where('dependent_id', $id)->delete();


        $travelHistory = $request->travel;

        $travelHistory = new InquiryDependentTravelHistory();
        $travelHistory->dependent_id = $id;
        $travelHistory->country = isset($request->country) ? $request->country : "";
        $travelHistory->program = isset($request->program) ? $request->program : "";
        $travelHistory->status = isset($request->status) ? $request->status : "";
        $travelHistory->year = isset($request->year) ? $request->year : "";
        $travelHistory->duration = isset($request->dependentYearDuration) ? $request->dependentYearDuration : "";
        $travelHistory->remark = isset($request->dependent_remarks) ? $request->dependent_remarks : "";
        $travelHistory->save();


        foreach ($travelHistory as $key => $value) {
            if ($value['country'] == null && $value['program'] == null && $value['status'] == null && $value['year'] == null && $value['duration'] == null && $value['remark'] == null) {
                unset($travelHistory[$key]);
            } else {

                $travel = new InquiryDependentTravelHistory();
                $travel->dependent_id = $id;
                $travel->country = $value['country'];
                $travel->program = $value['program'];
                $travel->status = $value['status'];
                $travel->year = $value['year'];
                $travel->duration = $value['duration'];
                $travel->remark = $value['remark'];
                $travel->save();
            }
        }

        return "updated";
    }

    public function destroy(VisaInquiry $inquiry)
    {

        if (Session::has('user_id')) {

            $userId = Session::get('user_id');

        } else {

            $userId = Auth::id();
        }

        $inquiry_wise_user_id = InquiryWiseUser::where('inquiry_id', '=', $inquiry->id)->where('user_id', '=', $userId)->value('id');

        InquiryAcademic::where('inquiry_id', '=', $inquiry_wise_user_id)->delete();

        InquiryExam::where('inquiry_id', '=', $inquiry_wise_user_id)->delete();

        InquiryWorkExperience::where('inquiry_id', '=', $inquiry_wise_user_id)->delete();

        InquiryTravelHistory::where('inquiry_id', '=', $inquiry_wise_user_id)->delete();

        InquiryCounsellingSummary::where('inquiry_id', '=', $inquiry_wise_user_id)->delete();

        $dependent = InquiryDependentDetail::where('inquiry_id', '=', $inquiry_wise_user_id)->value('id');

        $dependent_inquiry_id = InquiryDependentUser::where('dependent_id', '=', $dependent)->where('user_id', $userId)->value('id');

        InquiryDependentChildren::where('dependent_id', '=', $dependent_inquiry_id)->delete();

        InquiryDependentExam::where('dependent_id', '=', $dependent_inquiry_id)->delete();

        InquiryDependentAcademic::where('dependent_id', '=', $dependent_inquiry_id)->delete();

        InquiryDependentWorkExperience::where('dependent_id', '=', $dependent_inquiry_id)->delete();

        InquiryDependentTravelHistory::where('dependent_id', '=', $dependent_inquiry_id)->delete();

        InquiryDependentUser::where('inquiry_id', '=', $inquiry->id)->where('user_id', '=', $userId)->delete();

        InquiryWiseUser::where('inquiry_id', '=', $inquiry->id)->where('user_id', '=', $userId)->delete();

        FollowUp::where('inquiry_wise_user_id', '=', $inquiry_wise_user_id)->delete();


        InquiryDependentDetail::where('inquiry_id', '=', $inquiry_wise_user_id)->delete();

        $inquiry->delete();

        return redirect('/inquiries');
    }

    public
    function completedInquiries()
    {
        if (Session::has('user_id')) {

            $userId = Session::get('user_id');

        } else {

            $userId = Auth::id();

        }

        if ($userId != 1) {

            $inquiries = VisaInquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->join("inquiry_wise_users", "visa_inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->where('visa_inquiries.status', '=', 1)->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();

        } else {

            $inquiries = Inquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->where('visa_inquiries.status', '=', 1)->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();

        }

        return view('/inquiries', compact("inquiries", "userId"));
    }

    public
    function cancelledInquiries()
    {
        if (Session::has('user_id')) {

            $userId = Session::get('user_id');

        } else {

            $userId = Auth::id();

        }

        if ($userId != 1) {

            $inquiries = Inquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");

            })->join("inquiry_wise_users", "visa_inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->where('visa_inquiries.status', '=', 2)->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();

        } else {

            $inquiries = Inquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->where('visa_inquiries.status', '=', 2)->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();
            /*$inquiries = Inquiry::where("status", "=", 2)->get();*/
        }

        return view('/inquiries', compact("inquiries", "userId"));
    }

    public
    function forwardedInquiries()
    {
        if (Session::has('user_id')) {
            $userId = Session::get('user_id');
        } else {
            $userId = Auth::id();
        }

        if ($userId != 1) {
            $inquiries = VisaInquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->join("inquiry_wise_users", "visa_inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->groupBy('reasons.inquiry_id')->orderBy('status', 'asc')->orderBy('visa_inquiries.updated_at', 'desc')->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();
        } else {
            $inquiries = VisaInquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->groupBy('reasons.inquiry_id')->orderBy('status', 'asc')->orderBy('visa_inquiries.updated_at', 'desc')->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();
            /*$inquiries = Inquiry::join('reasons', 'inquiries.id', '=', 'reasons.inquiry_id')->where('inquiries.status', '=', 0)->groupBy('reasons.inquiry_id')->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();*/
        }
        return view('/inquiries', compact("inquiries", "userId"));
    }


    /** *******************************************************
     * Methods for followup
     ******************************************************* */

    public
    function followUp(VisaInquiry $inquiry)
    {

        $inquiry_wise_user_id = request()->inquiry_wise_user_id;


        $counselling = InquiryCounsellingSummary::where('inquiry_id', $inquiry_wise_user_id)->orderBy('id', 'DESC')->first();


        if (Session::has('user_id')) {
            $userId = Session::get('user_id');
        } else {
            $userId = Auth::id();
        }

        $followUps = FollowUp::join('inquiry_wise_users', 'follow_ups.inquiry_wise_user_id', '=', 'inquiry_wise_users.id')->where('inquiry_wise_users.inquiry_id', '=', $inquiry->id)->orderBy('follow_ups.id', 'desc')->select('follow_ups.*', 'inquiry_wise_users.user_id')->get();

//        $inquiry->userName = Inquiry::getUserNames($inquiry->id, $userId);
        $inquiry->userName = VisaInquiry::getUserNames($inquiry->id, $userId);

        $reasons = Reason::join('inquiries', 'reasons.inquiry_id', '=', 'inquiries.id')->where('reasons.inquiry_id', '=', $inquiry->id)->select('reasons.*')->orderBy('reasons.id', 'desc')->get();

        return view('inquiry.followUp', compact('inquiry', 'followUps', 'reasons', 'inquiry_wise_user_id', 'counselling'));
    }

    public
    function createFollowUp(VisaInquiry $inquiry)
    {
        /*$inquiry_wise_user_id = InquiryWiseUser::where('inquiry_id', '=', $inquiry->id)->where('user_id', '=', Auth::id())->value('id');*/
        $inquiry_wise_user_id = request()->inquiry_wise_user_id;
        $followUps = FollowUp::join('inquiry_wise_users', 'follow_ups.inquiry_wise_user_id', '=', 'inquiry_wise_users.id')->where('inquiry_wise_users.inquiry_id', '=', $inquiry->id)->orderBy('follow_ups.id', 'desc')->select('follow_ups.*', 'inquiry_wise_users.user_id')->get();

        return view('inquiry.createFollowUp', compact('inquiry', 'inquiry_wise_user_id', 'followUps'));
    }

    public
    function storeFollowUp()
    {

        FollowUp::create([
            'inquiry_wise_user_id' => request()->inquiry_wise_user_id,
            'interest_level' => request()->interest_level,
            'status' => request()->status,
            'action' => request()->action,
            'next_follow_up' => date('Y-m-d H:i:s', strtotime(request()->next_follow_up)),
            'comments' => request()->comments
        ]);

        return redirect('/inquiries/' . request()->inquiry_id . '/followUp');
    }

    public
    function editFollowUp(FollowUp $followUp)
    {
        $inquiry = $followUp->inquiryWiseUser->inquiry;
        //$inquiry_wise_user_id = InquiryWiseUser::where('inquiry_id', '=', $inquiry->id)->where('user_id', '=', Auth::id())->value('id');
        return view('inquiry.editFollowUp', compact('followUp', 'inquiry'));
    }

    public
    function updateFollowUp(FollowUp $followUp)
    {
        $followUp->inquiry_wise_user_id = request()->inquiry_wise_user_id;
        $followUp->interest_level = request()->interest_level;
        $followUp->status = request()->status;
        $followUp->action = request()->action;
        $followUp->next_follow_up = date('Y-m-d H:i:s', strtotime(request()->next_follow_up));
        $followUp->comments = request()->comments;

        $followUp->save();

        return redirect('/inquiries/' . $followUp->inquiryWiseUser->inquiry_id . '/followUp');
    }

    public
    function getArea($id)
    {

        $area = Area::where('city_id', $id)->get();

        return Response::json($area);

    }

    public
    function autoCompleteFriend()
    {

        $friend = VisaInquiry::where('friend_name', '!=', null)->groupBy('friend_name')->pluck('friend_name')->toArray();

        $tag = VisaInquiry::whereIn('friend_name', $friend)->groupBy('tag')->pluck('tag')->toArray();

        return response()->json(array('friend' => $friend, 'tag' => $tag));
    }
}
