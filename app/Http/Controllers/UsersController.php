<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Designation;
use App\InquiryWiseUser;
use App\FollowUp;
use App\Http\Requests\RegistrationFormRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Role;
use App\User;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::get();
        return view('users', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branches = Branch::all();
        $roles = Role::all();
        $designations = Designation::all();

        return view('createUser', compact('branches','roles','designations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RegistrationFormRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegistrationFormRequest $request)
    {
        $data = request()->all();
        $data['password'] = bcrypt($data['password']);
        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'username' => $data['username'],
            'branch_id' => $data['branch_id'],
            'password' => $data['password'],
            'designation_id' => $data['designation_id'],
            'role_id' => $data['role_id'],
            'status' => isset($data['status'])?$data['status']:1,
        ]);
        return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $branches = Branch::all();
        $roles = Role::all();
        $designations = Designation::all();


        return view('editUser', compact('branches', 'user','designations','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RegistrationFormRequest $request
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $data = request()->all();
        //dd($data);
        //$data['password'] = bcrypt($user->password);
        $user->update($data);
        return redirect('/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $ids = InquiryWiseUser::where("user_id", "=", $user->id)->get()->toArray();
        InquiryWiseUser::where("user_id", "=", $user->id)->delete();
        FollowUp::whereIn("inquiry_wise_user_id", $ids)->delete();
        $user->delete();
        return back();
    }
}
