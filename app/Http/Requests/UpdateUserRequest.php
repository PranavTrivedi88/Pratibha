<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|max:255',
            'email' => [
                'required',
                'email',
                'max:255',
                Rule::unique('users')->ignore(Auth::user()->id)->where(function ($query) {
                    $query->where('is_admin', 1);
                })],
            'username' => [
                'required',
                'max:255',
                Rule::unique('users')->ignore(Auth::user()->id)->where(function ($query) {
                    $query->where('is_admin', 1);
                })],
            'branch_id' => 'integer|min:1',
            'designation_id' => 'integer|min:1',
            'role_id' => 'integer|min:1',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'branch_id.min' => 'Please select branch.',
            'designation_id.min' => 'Please select designation.',
            'role_id.min' => 'Please select role.',
        ];
    }
}
