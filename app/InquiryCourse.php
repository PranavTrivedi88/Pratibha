<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InquiryCourse extends Model
{
    protected $fillable = ["inquiry_id", "course_id"];

    public function inquiry(){
        return $this->belongsTo(Inquiry::class);
    }

    public function course(){
        return $this->belongsTo(Course::class);
    }

}
