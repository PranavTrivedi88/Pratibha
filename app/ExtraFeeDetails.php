<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtraFeeDetails extends Model
{
    protected $fillable = ['fee_detail_id','extra_fee_id','discount','paid_amount'];

    public function feeDetail()
    {
        return $this->belongsTo(FeeDetail::class);
    }

    public function extraFee()
    {
        return $this->belongsTo(ExtraFee::class);
    }
}
