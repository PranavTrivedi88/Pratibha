<?php

function isRole($role)
{
    if (Admin::user()) {
        return Admin::user()->isRole($role);
    } else {
        return false;
    }
}

function getCourseFees($userId, $courseId)
{
    if ($courseId != 1) {
         $feeType = \App\CourseStudent::where('student_id', $userId)->where('course_id', $courseId)->first()->fee_type;
        return \App\CourseFeeType::where('course_id', $courseId)->where('fee_type_id', $feeType)->first()->fees;
    } else {
        return \App\CourseFeeType::where('course_id', $courseId)->first()->fees;
    }


}

function getMonthDiff($month, $year)
{
    $date1 = new DateTime("01-" . $month . "-" . $year);
    $date2 = new DateTime("now", new DateTimeZone("Asia/Kolkata"));

    $diff = $date1->diff($date2)->format("%r%m");
    $year_diff = $date1->diff($date2)->format("%r%y");

    $months = 0;

    if ($year_diff > 0) {
        $months = $year_diff * 12;
    }
    $diff += $months;

    return ($diff <= 0) ? 0 : $diff + 1;
}

function ordinal($number)
{
    $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
    if ((($number % 100) >= 11) && (($number % 100) <= 13))
        return $number . 'th';
    else
        return $number . $ends[$number % 10];
}

function showActivityBranchName($courseObject)
{

        if(isRole('super-administrator') || isRole('administrator'))
        {
            if(isset($courseObject->branch->name))
            {
                return ' ('.$courseObject->branch->name.')';
            }
        }
}

function is_visible($menu_roles)
{
    if (empty($menu_roles)) {
        return true;
    }

    $roles = Admin::user()->roles->toArray();

    $roles = array_column($roles, 'slug');
    $user_roles = array_column($menu_roles, 'slug');

    if (array_intersect($roles, $user_roles)) {
        return true;
    }

    return false;
}