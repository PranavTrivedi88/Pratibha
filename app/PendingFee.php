<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;
use DateTimeZone;

class PendingFee extends Model
{
    protected $fillable = ["user_id", "course_id", "month", "year", "fees","fees_type"];

    public function user()
    {
        return $this->belongsTo(AppUser::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class)->withoutGlobalScope("removeMembership");
    }

    public static function getPermanentDiscount($user_id, $course_id)
    {
        $discount = StudentDiscount::where("user_id", $user_id)->where("course_id", $course_id)->value("discount_amount");
        return $discount ? $discount : 0;
    }

    public static function pendingAmountByCourse($user_id, $course_id)
    {
        $pendingData = PendingFee::where("user_id", $user_id)->where("course_id", $course_id)->first();
        $diff = getMonthDiff($pendingData->month, $pendingData->year);

        if ($pendingData->fees_type == 1)
            return ($diff == 0) ? $pendingData->fees : ($diff * $pendingData->fees);
        else
            return $pendingData->fees;
    }
}
