<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;

class ExampleTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */

    public function testBasicTest()
    {
        $response = $this->get('/');
        $response->assertStatus(302);
    }

    public function testLoginUri()
    {
        $response = $this->get('/login');
        $response->assertStatus(200);
    }

    public function testHomeUri()
    {
        $response = $this->get('/home');
        $response->assertStatus(302);

        $this->actingAs(User::first());
        $response = $this->get('/home');
        $response->assertStatus(200);
    }

    public function testBranchesUri()
    {
        $response = $this->get('/branches');
        $response->assertStatus(302);

        $this->actingAs(User::first());
        $response = $this->get('/branches');
        $response->assertStatus(200);
    }

    public function testCoursesUri()
    {
        $response = $this->get('/courses');
        $response->assertStatus(302);

        $this->actingAs(User::first());
        $response = $this->get('/courses');
        $response->assertStatus(200);
    }

    public function testInquiriesUri()
    {
        $response = $this->get('/inquiries');
        $response->assertStatus(302);

        $this->actingAs(User::first());
        $response = $this->get('/inquiries');
        $response->assertStatus(200);
    }

    public function testUsersUri()
    {
        $response = $this->get('/users');
        $response->assertStatus(302);

        $this->actingAs(User::first());
        $response = $this->get('/users');
        $response->assertStatus(200);
    }
}
